#pragma once

#include <unordered_map>
#include "Zone.h"
#include "Enemy.h"

class Server;
class GameManager;

class ZoneManager
{
public:
	ZoneManager(Server* server);
	~ZoneManager();

	void SetGameManager(GameManager* gameManager) { m_gameManager = gameManager; }

	void AddInstance(uint32_t zoneID);
	void RemoveInstance(uint32_t zoneID);

	void AddMember(uint32_t zoneID, Player* player);
	void RemoveMember(Player* player);

	void UpdateEnemies();

	void BroadcastZone(Player* player, uint8_t command, char* data, uint16_t length, bool guaranteed);
	void BroadcastZone(Enemy* enemy, uint8_t command, char* data, uint16_t length, bool guaranteed);
	void BroadcastZoneAll(uint32_t zoneId, uint8_t command, char* data, uint16_t length, bool guaranteed);

	void LogZone(uint32_t zoneId);
	void LogZones();

	Zone* GetZone(uint32_t zoneID);

	bool HasInstance(uint32_t id);

	int InstanceCount();

	std::unordered_map<uint32_t, Zone*> m_zones;

private:
	Server* m_server;
	GameManager* m_gameManager;

	Zone* m_hubTown;
};
