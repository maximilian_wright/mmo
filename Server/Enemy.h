#pragma once
#ifndef ENEMY_H
#define ENEMY_H

#include <set>
#include <time.h>
#include "Vector.h"

enum class EnemyType
{
	CAVEWORM = 1,
	SPIDER = 2,
	GOBLIN = 3
};

enum class EnemyData
{
	HEALTH = 1,
	BLOCKING = 6
};

class Enemy
{
public:
	Enemy(uint32_t id, uint32_t zoneId, EnemyType type, const Vector3& position, float rotationY, float velocityMag, uint32_t targetId, int32_t health);
	~Enemy();

	uint32_t GetId() const { return m_id; }

	uint32_t GetZoneId() const { return m_zoneId; }
	void SetZoneId(uint32_t zoneId) { m_zoneId = zoneId; }

	EnemyType GetType() const { return m_type; }
	void SetType(EnemyType type) { m_type = type; }

	Vector3 GetPosition() const { return m_position; }
	void SetPosition(const Vector3& position) { m_position = position; }

	float GetRotationY() const { return m_rotationY; }
	void SetRotationY(float rotationY) { m_rotationY = rotationY; }

	float GetVelocityMag() const { return m_velocityMag; }
	void SetVelocityMag(float velocityMag) { m_velocityMag = velocityMag; }

	uint32_t GetTargetId() const { return m_targetId; }
	void SetTargetId(uint32_t targetId) { m_targetId = targetId; }

	uint32_t GetPrevTargetId() const { return m_prevTargetId; }
	void SetPrevTargetId(uint32_t prevTargetId) { m_prevTargetId = prevTargetId; }

	int32_t GetHealth() const { return m_health; }
	void SetHealth(int32_t health);

	int32_t GetMaxHealth() const { return 100; }

	const std::set<EnemyData>* GetDirtyData() const { return &m_dirtyData; }
	void ClearDirtyData() { m_dirtyData.clear(); }

	bool GetBlocking() const { return m_blocking; }
	void SetBlocking(bool blocking);

	void Update();

private:
	std::set<EnemyData> m_dirtyData;
	Vector3 m_position;
	float m_rotationY;
	float m_velocityMag;
	uint32_t m_id;
	uint32_t m_zoneId;
	EnemyType m_type;
	uint32_t m_targetId;
	uint32_t m_prevTargetId;
	int32_t m_health;

	bool m_blocking;
	time_t m_blockTime;
};

#endif
