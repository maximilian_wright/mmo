#include "Server.h"

int main()
{
	Server* server = new Server();

	int status = server->Init();
	if (status != EXIT_SUCCESS)
	{
		delete server;
		return status;
	}

	status = server->Start();
	delete server;
	return status;
}
