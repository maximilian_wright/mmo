#pragma once


#include "ZoneManager.h"
#include "Player.h"
#include "Party.h"
#include <unordered_map>

class Server;
class PartyManager
{

public:
	PartyManager();
	PartyManager(Server* server, ZoneManager* zm);
	~PartyManager();

	uint32_t AddParty();
	void AddParty(uint32_t groupID);
	void RemoveParty(uint32_t groupID);

	void AddMember(uint32_t groupID, Player* player);
	void RemoveMember(Player* player);

	void BroadcastParty(Player* player, uint8_t command, char* data, uint16_t length, bool guaranteed);

	void LogParty(uint32_t partyId);
	void LogParties();

	std::unordered_map<uint32_t, Party*> m_parties;

	Server* m_server;
	ZoneManager* m_zoneManager;

	uint32_t m_counter;
};
