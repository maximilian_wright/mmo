#include "Party.h"
#include "IoManager.h"

Party::Party(uint16_t id)
{
	m_guid = id;
}
Party::~Party()
{
	while (m_members.size() != 0)
	{
		m_members[m_members.size() - 1]->SetPartyId(0);
		m_members.pop_back();
	}
}

void Party::AddMember(Player* player)
{
	if (std::find(m_members.begin(), m_members.end(), player) == m_members.end())
	{
		player->SetPartyId(m_guid);
		m_members.push_back(player);
	}
}

void Party::RemoveMember(Player* player)
{
	for (unsigned int i = 0; i < m_members.size(); i++)
	{
		if (m_members[i] == player)
		{
			player->SetPartyId(0);
			m_members.erase(m_members.begin() + i);
			return;
		}
	}
}

int Party::MemberCount()
{
	return m_members.size();
}