#include "GameManager.h"
#include "Server.h"

//ranges are squared
#define MELEE_RANGE 2.5 * 2.5
#define CLEAVE_RANGE 5 * 5
#define TAUNT_RANGE 12 * 12

GameManager::GameManager(Server* server, ZoneManager* zoneManager, PartyManager* partyManager)
	: m_server(server),
	m_zoneManager(zoneManager),
	m_partyManager(partyManager),
	m_enemyIdCounter(1)
{
}

GameManager::~GameManager()
{
	// Delete players.
	std::vector<Player*> playerList;
	for (auto playerPair : m_players)
	{
		playerList.push_back(playerPair.second);
	}
	for (unsigned int i = 0; i < playerList.size(); ++i)
	{
		delete playerList[i];
	}

	// Delete enemies.
	std::vector<Enemy*> enemyList;
	for (auto enemyPair : m_enemies)
	{
		enemyList.push_back(enemyPair.second);
	}
	for (unsigned int i = 0; i < enemyList.size(); ++i)
	{
		delete enemyList[i];
	}
}

uint32_t GameManager::GetNewEnemyId()
{
	uint32_t enemyId = m_enemyIdCounter++;
	return enemyId;
}

void GameManager::AddPlayer(uint32_t playerId, char* name, uint16_t nameLength)
{
	Player* player = new Player(playerId, 0, 0, name, nameLength, Vector3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f, 100, 10, 10, 10, 10);
	m_players[playerId] = player;
	m_zoneManager->AddMember(0, player);
}

void GameManager::RemovePlayer(uint32_t playerId)
{
	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(playerId);
	if (iter != m_players.end())
	{
		Player* player = iter->second;
		m_partyManager->RemoveMember(player);
		m_zoneManager->RemoveMember(player);
		m_players.erase(playerId);
		delete player;
	}
}

void GameManager::AddEnemy(Enemy* enemy)
{
	m_enemies[enemy->GetId()] = enemy;
}

void GameManager::RemoveEnemy(Enemy* enemy)
{
	m_enemies.erase(enemy->GetId());
}

void GameManager::HandlePlayerAction(uint32_t playerId, uint8_t actionId, char* data, uint16_t dataLength)
{
	uint8_t skillId = data[0];
	std::unordered_map<uint32_t, Player*>::iterator iterPlayer = m_players.find(playerId);
	if (iterPlayer != m_players.end() && iterPlayer->second->GetHealth() > 0)
	{
		if (skillId == (int)Actions::AUTOATTACK)
		{
			uint32_t targetId = *(uint32_t*)(data + 1);
			IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i used Auto-Attack", iterPlayer->second->GetId());
			std::unordered_map<uint32_t, Enemy*>::iterator iterTarget = m_enemies.find(targetId);
			if (iterTarget != m_enemies.end())
			{
				if (iterTarget->second->GetBlocking())
				{
					IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i blocked Auto-Attack!", iterTarget->second->GetId());
					iterTarget->second->SetBlocking(false);
				}
				else
				{
					int damage = iterPlayer->second->GetStrength();

					IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i was hit by Auto-Attack for %i damage", iterTarget->second->GetId(), damage);
					iterTarget->second->SetHealth(iterTarget->second->GetHealth() - damage);
					uint32_t buffer[2];
					buffer[0] = targetId;
					buffer[1] = damage;
					m_zoneManager->BroadcastZoneAll(iterPlayer->second->GetZoneId(), Commands::DAMAGE_ENEMY, (char*)&buffer, sizeof(buffer), false);
					if (iterTarget->second->GetHealth() <= 0)
					{
						KillEnemy(iterTarget->second->GetId());
					}
				}
				uint8_t buffer[10];
				*(uint32_t*)buffer = iterPlayer->second->GetId();
				*(buffer + 4) = (uint8_t)1;
				*(buffer + 5) = (uint8_t)Actions::AUTOATTACK;
				*(uint32_t*)(buffer + 6) = targetId;
				m_zoneManager->BroadcastZone(iterPlayer->second, (uint8_t)Commands::PLAYER_ACTION, (char*)&buffer, sizeof(buffer), true);
			}
			else
			{
				IoManager::Instance().Write(L"Tried to attack enemy %i, enemy not found", targetId);
			}
			
		}
		else if (skillId == (int)Actions::EMPOWERED_STRIKE)
		{
			uint32_t targetId = *(uint32_t*)(data + 1);
			IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i used Empowered Strike", iterPlayer->second->GetId());
			std::unordered_map<uint32_t, Enemy*>::iterator iterTarget = m_enemies.find(targetId);
			if (iterTarget != m_enemies.end())
			{
				if (iterTarget->second->GetBlocking())
				{
					IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i blocked Empowered Strike!", iterTarget->second->GetId());
					iterTarget->second->SetBlocking(false);
				}
				else
				{
					IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i was hit by Empowered Strike", iterTarget->second->GetId());
					int damage = iterPlayer->second->GetStrength() * 3;
					iterTarget->second->SetHealth(iterTarget->second->GetHealth() - damage);
					int32_t buffer[2];
					buffer[0] = targetId;
					buffer[1] = damage;
					m_zoneManager->BroadcastZoneAll(iterPlayer->second->GetZoneId(), Commands::DAMAGE_ENEMY, (char*)&buffer, sizeof(buffer), false);
					if (iterTarget->second->GetHealth() <= 0)
					{
						KillEnemy(iterTarget->second->GetId());
					}
				}
				uint8_t buffer[10];
				*(uint32_t*)buffer = iterPlayer->second->GetId();
				*(buffer + 4) = (uint8_t)1;
				*(buffer + 5) = (uint8_t)Actions::EMPOWERED_STRIKE;
				*(uint32_t*)(buffer + 6) = targetId;
				m_zoneManager->BroadcastZone(iterPlayer->second, (uint8_t)Commands::PLAYER_ACTION, (char*)&buffer, sizeof(buffer), true);
			}
			else
				IoManager::Instance().Write(L"Tried to attack enemy %i, enemy not found", targetId);
		}
		else if (skillId == (int)Actions::CLEAVE)
		{
			int damage = iterPlayer->second->GetStrength();
			IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i used Cleave", iterPlayer->second->GetId());
			Zone* zone = m_zoneManager->GetZone(iterPlayer->second->GetZoneId());
			std::vector<uint32_t> toKill;
			for (auto e : zone->m_enemyToSimPlayerMap)
			{
				Enemy* enemy = e.first;
				float rot = iterPlayer->second->GetRotationY() * 3.14f / 180.0f;
				Vector3 forward = normalize(Vector3(sin(rot), 0, cos(rot)));
				float dotProduct = dot(forward, normalize(iterPlayer->second->GetPosition() - enemy->GetPosition()));
				float distance = squaredLength(enemy->GetPosition() - iterPlayer->second->GetPosition());
				if (distance < CLEAVE_RANGE && dotProduct < 0)
				{
					if (enemy->GetBlocking())
					{
						IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i blocked Cleave!", enemy->GetId());
						enemy->SetBlocking(false);
					}
					else
					{
						IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i was hit by Cleave", enemy->GetId());
						enemy->SetHealth(enemy->GetHealth() - damage);
						uint32_t buffer[2];
						buffer[0] = enemy->GetId();
						buffer[1] = damage;
						m_zoneManager->BroadcastZoneAll(iterPlayer->second->GetZoneId(), Commands::DAMAGE_ENEMY, (char*)&buffer, sizeof(buffer), false);
						if (enemy->GetHealth() <= 0)
						{
							toKill.push_back(enemy->GetId());
						}
					}
				}
			}
			int i = toKill.size() - 1;
			while (toKill.size() > 0)
			{
				KillEnemy(toKill[i]);
				toKill.pop_back();
				--i;
			}
			uint8_t buffer[6];
			*(uint32_t*)buffer = iterPlayer->second->GetId();
			*(buffer + 4) = (uint8_t)1;
			*(buffer + 5) = (uint8_t)Actions::CLEAVE;
			m_zoneManager->BroadcastZone(iterPlayer->second, (uint8_t)Commands::PLAYER_ACTION, (char*)&buffer, sizeof(buffer), true);
		}
		else if (skillId == (int)Actions::BLOCK)
		{
			IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i used Block", iterPlayer->second->GetId());
			iterPlayer->second->SetBlocking(true);
			uint8_t buffer[6];
			*(uint32_t*)buffer = iterPlayer->second->GetId();
			buffer[4] = (uint8_t)1;
			buffer[5] = (uint8_t)Actions::BLOCK;
			m_zoneManager->BroadcastZone(iterPlayer->second, (uint8_t)Commands::PLAYER_ACTION, (char*)&buffer, sizeof(buffer), true);
		}
		else if (skillId == (int)Actions::TAUNT)
		{
			IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i used Taunt", iterPlayer->second->GetId());
			Zone* zone = m_zoneManager->GetZone(iterPlayer->second->GetZoneId());
			for (auto enemy : zone->m_enemyToSimPlayerMap)
			{
				float distance = squaredLength(enemy.first->GetPosition() - iterPlayer->second->GetPosition());
				if (distance < TAUNT_RANGE)
				{
					enemy.first->SetTargetId(iterPlayer->second->GetId());
					IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i used Taunt on Enemy %i.", iterPlayer->second->GetId(), enemy.first->GetId());
				}
			}
			uint8_t buffer[6];
			*(uint32_t*)buffer = iterPlayer->second->GetId();
			buffer[4] = (uint8_t)1;
			buffer[5] = (uint8_t)Actions::TAUNT;
			m_zoneManager->BroadcastZone(iterPlayer->second, (uint8_t)Commands::PLAYER_ACTION, (char*)&buffer, 5, true);
			
		}
	}
}

void GameManager::HandleEnemyAction(uint32_t enemyId, uint8_t actionId, char* data, uint16_t dataLength)
{
	uint8_t skillId = data[0];
	std::unordered_map<uint32_t, Enemy*>::iterator iterEnemy = m_enemies.find(enemyId);
	if (iterEnemy != m_enemies.end())
	{
		if (skillId == (int)Actions::AUTOATTACK)
		{
			uint32_t targetId = *(uint32_t*)(data+1);
			IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i used Auto-Attack", iterEnemy->second->GetId());
			std::unordered_map<uint32_t, Player*>::iterator iterTarget = m_players.find(targetId);
			if (iterTarget != m_players.end())
			{
				if (iterTarget->second->GetHealth() > 0)
				{
					if (iterTarget->second->GetBlocking())
					{
						IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i blocked Auto-Attack!", iterTarget->second->GetId());
						iterTarget->second->SetBlocking(false);
					}
					else
					{
						IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i was hit by Auto-Attack", iterTarget->second->GetId());
						int damage = iterEnemy->second->GetType() == EnemyType::GOBLIN ? 5 : 10;
						iterTarget->second->SetHealth(iterTarget->second->GetHealth() - damage);
						uint32_t buffer[2];
						buffer[0] = targetId;
						buffer[1] = damage;
						m_zoneManager->BroadcastZoneAll(iterTarget->second->GetZoneId(), Commands::DAMAGE_PLAYER, (char*)&buffer, sizeof(buffer), false);
						if (iterTarget->second->GetHealth() <= 0)
						{
							KillPlayer(iterTarget->second->GetId());
						}
					}
				}
				uint8_t buffer[10];
				*(uint32_t*)buffer = iterEnemy->second->GetId();
				buffer[4] = (uint8_t)1;
				buffer[5] = (uint8_t)Actions::AUTOATTACK;
				*(uint32_t*)(buffer + 6) = targetId;
				m_zoneManager->BroadcastZone(iterEnemy->second, (uint8_t)Commands::ENEMY_ACTION, (char*)&buffer, sizeof(buffer), true);
			}
		}
		else if (actionId == (int)Actions::EMPOWERED_STRIKE)
		{
			uint32_t targetId = *(uint32_t*)(data+1);
			IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i used Empowered Strike", iterEnemy->second->GetId());
			std::unordered_map<uint32_t, Player*>::iterator iterTarget = m_players.find(targetId);
			if (iterTarget != m_players.end() && iterTarget->second->GetHealth() > 0)
			{
				if (iterTarget->second->GetBlocking())
				{
					IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i blocked Empowered Strike!", iterTarget->second->GetId());
					iterTarget->second->SetBlocking(false);
				}
				else
				{
					IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i was hit by Empowered Strike", iterTarget->second->GetId());
					int damage = 10;
					iterTarget->second->SetHealth(iterTarget->second->GetHealth() - damage);
					uint32_t buffer[2];
					buffer[0] = targetId;
					buffer[1] = damage;
					m_zoneManager->BroadcastZoneAll(iterTarget->second->GetZoneId(), Commands::DAMAGE_PLAYER, (char*)&buffer, sizeof(buffer), false);

					if (iterTarget->second->GetHealth() <= 0)
					{
						KillPlayer(iterTarget->second->GetId());
					}
				}
				uint8_t buffer[10];
				*(uint32_t*)buffer = iterEnemy->second->GetId();
				buffer[4] = (uint8_t)1;
				buffer[5] = (uint8_t)Actions::EMPOWERED_STRIKE;
				*(uint32_t*)(buffer + 6) = targetId;
				m_zoneManager->BroadcastZone(iterEnemy->second, (uint8_t)Commands::ENEMY_ACTION, (char*)&buffer, sizeof(buffer), true);
			}
		}
		else if (actionId == (int)Actions::CLEAVE)
		{
			int damage = 5;
			IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i used Cleave", iterEnemy->second->GetId());
			Zone* zone = m_zoneManager->GetZone(iterEnemy->second->GetZoneId());
			std::vector<uint32_t> toKill;
			for (auto player : zone->m_members)
			{
				if (player->GetHealth() > 0)
				{
					float rot = iterEnemy->second->GetRotationY() * 3.14f / 180.0f;
					Vector3 forward = normalize(Vector3(sin(rot), 0, cos(rot)));
					float dotProduct = dot(forward, normalize(iterEnemy->second->GetPosition() - player->GetPosition()));
					float distance = squaredLength(player->GetPosition() - iterEnemy->second->GetPosition());
					if (distance < CLEAVE_RANGE && dotProduct < 0)
					{
						if (player->GetBlocking())
						{
							IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i blocked Cleave!", player->GetId());
							player->SetBlocking(false);
						}
						else
						{
							IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i was hit by Cleave", player->GetId());
							player->SetHealth(player->GetHealth() - damage);
							uint32_t buffer[2];
							buffer[0] = player->GetId();
							buffer[1] = damage;
							m_zoneManager->BroadcastZoneAll(iterEnemy->second->GetZoneId(), Commands::DAMAGE_PLAYER, (char*)&buffer, sizeof(buffer), false);
							if (player->GetHealth() <= 0)
							{
								toKill.push_back(player->GetId());
							}
						}
					}
				}
			}
			int i = toKill.size() - 1;
			while (toKill.size() > 0)
			{
				KillEnemy(toKill[i]);
				toKill.pop_back();
				--i;
			}
			uint8_t buffer[6];
			*(uint32_t*)buffer = iterEnemy->second->GetId();
			buffer[4] = (uint8_t)1;
			buffer[5] = (uint8_t)Actions::CLEAVE;
			m_zoneManager->BroadcastZone(iterEnemy->second, (uint8_t)Commands::ENEMY_ACTION, (char*)&buffer, sizeof(buffer), true);
		}
		else if (actionId == (int)Actions::BLOCK)
		{
			iterEnemy->second->SetBlocking(true);
			uint8_t buffer[6];
			*(uint32_t*)buffer = iterEnemy->second->GetId();
			buffer[4] = (uint8_t)1;
			buffer[5] = (uint8_t)Actions::BLOCK;
			m_zoneManager->BroadcastZone(iterEnemy->second, (uint8_t)Commands::ENEMY_ACTION, (char*)&buffer, sizeof(buffer), true);
		}
	}
}

void GameManager::EnterZone(uint32_t playerId, char* data)
{
	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(playerId);
	if (iter != m_players.end())
	{
		uint32_t zoneId = *(uint32_t*)data;
		if (zoneId != 0)
		{
			zoneId = iter->second->GetPartyId();
		}

		IoManager::Instance().Write(Verbosity::BASIC, L"Player %i entered zone %i", playerId, zoneId);
		m_server->PushGuaranteedPacket(playerId, (byte)Commands::ENTER_ZONE, -1, (char*)&zoneId, 4);

		m_zoneManager->AddMember(zoneId, iter->second);
	}
}

void GameManager::CreateParty(uint32_t playerId)
{
	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(playerId);
	if (iter != m_players.end())
	{
		uint32_t partyId = m_partyManager->AddParty();
		m_partyManager->AddMember(partyId, iter->second);
		IoManager::Instance().Write(Verbosity::BASIC, L"Player %i created party %i", playerId, partyId);
		m_server->PushGuaranteedPacket(playerId, Commands::CREATE_PARTY, -1, (char*)&partyId, 4);
	}
}

void GameManager::PartyInvite(uint32_t senderId, uint32_t recipientId)
{
	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(senderId);
	if (iter != m_players.end())
	{
		uint8_t buffer[72];
		*(uint32_t*)buffer = iter->second->GetPartyId();
		*(uint32_t*)(buffer + 4) = senderId;
		const char* name = iter->second->GetName();
		int nameLength = strlen(name) + 1;
		memcpy(buffer + 8, name, nameLength);
		m_server->PushGuaranteedPacket(recipientId, Commands::PARTY_INVITE, -1, (char*)&buffer, 8 + nameLength);
		IoManager::Instance().Write(Verbosity::BASIC, L"Player %i invited player %i to party %i", senderId, recipientId, iter->second->GetPartyId());
	}
}

void GameManager::JoinParty(uint32_t playerId, uint32_t partyId)
{
	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(playerId);
	if (iter != m_players.end() && partyId != 0)
	{
		m_partyManager->AddMember(partyId, iter->second);
		IoManager::Instance().Write(Verbosity::BASIC, L"Player %i joining group %i", playerId, partyId);
	}
}

void GameManager::LeaveParty(uint32_t playerId)
{
	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(playerId);
	if (iter != m_players.end())
	{
		IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i left party %i", playerId, iter->second->GetPartyId());
		m_partyManager->RemoveMember(iter->second);
		m_server->PushGuaranteedPacket(playerId, (uint8_t)Commands::LEAVE_PARTY, -1, (char*)&playerId, 4);
	}
}

void GameManager::MovePlayer(char* data, uint16_t length)
{
	char* dataBufferHead = data;

	uint32_t playerId = *(uint32_t*)dataBufferHead;
	dataBufferHead += 4;

	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(playerId);
	if (iter != m_players.end())
	{
		Player* player = iter->second;

		// Update all of the server player's movement data.
		Vector3 playerPosition;
		playerPosition.x = *(float*)dataBufferHead;
		dataBufferHead += 4;
		playerPosition.y = *(float*)dataBufferHead;
		dataBufferHead += 4;
		playerPosition.z = *(float*)dataBufferHead;
		dataBufferHead += 4;
		player->SetPosition(playerPosition);

		player->SetRotationY(*(float*)dataBufferHead);
		dataBufferHead += 4;

		player->SetVelocityMag(*(float*)dataBufferHead);

		m_zoneManager->BroadcastZone(player, Commands::MOVE_PLAYER, data, length, false);
	}
}

void GameManager::MoveEnemy(char* data, uint16_t length)
{
	char* dataBufferHead = data;

	uint32_t enemyId = *(uint32_t*)dataBufferHead;
	dataBufferHead += 4;

	std::unordered_map<uint32_t, Enemy*>::iterator iter = m_enemies.find(enemyId);
	if (iter != m_enemies.end())
	{
		Enemy* enemy = iter->second;

		// Update all of the server enemy's movement data.
		Vector3 enemyPosition;
		enemyPosition.x = *(float*)dataBufferHead;
		dataBufferHead += 4;
		enemyPosition.y = *(float*)dataBufferHead;
		dataBufferHead += 4;
		enemyPosition.z = *(float*)dataBufferHead;
		dataBufferHead += 4;
		enemy->SetPosition(enemyPosition);

		enemy->SetRotationY(*(float*)dataBufferHead);
		dataBufferHead += 4;

		enemy->SetVelocityMag(*(float*)dataBufferHead);

		m_zoneManager->BroadcastZone(enemy, Commands::MOVE_ENEMY, data, length, false);
	}
}

void GameManager::KillPlayer(uint32_t playerId)
{
	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(playerId);
	if (iter != m_players.end())
	{
		uint32_t id = iter->second->GetId();
		m_zoneManager->BroadcastZoneAll(iter->second->GetZoneId(), Commands::KILL_PLAYER, (char*)&id, 4, true);
		IoManager::Instance().Write(Verbosity::DEBUG, L"Player %i killed", playerId);
		Zone* zone = m_zoneManager->GetZone(iter->second->GetZoneId());

		for (auto enemyPlayerPair : zone->m_enemyToSimPlayerMap)
		{
			if (enemyPlayerPair.second == iter->second)
			{
				enemyPlayerPair.first->SetTargetId(0);
			}
		}

		if (zone != nullptr)
		{
			for (unsigned int i = 0; i < zone->m_members.size(); i++)
			{
				if (zone->m_members[i]->GetHealth() > 0)
					return;
			}
			m_zoneManager->BroadcastZoneAll(iter->second->GetZoneId(), Commands::DUNGEON_WIPE, nullptr, 0, true);
			int ran = rand() % 2;
			if (ran == 0)
				IoManager::Instance().Write(L"\nW-W-W-WASTED\n");
			else
				IoManager::Instance().Write(L"\nRIGGETY RIGGETY WRRRRRECKED SON\n");
		}
	}
}

void GameManager::KillEnemy(uint32_t enemyId)
{
	std::unordered_map<uint32_t, Enemy*>::iterator iter = m_enemies.find(enemyId);
	if (iter != m_enemies.end())
	{
		uint32_t id = iter->second->GetId();
		m_zoneManager->BroadcastZoneAll(iter->second->GetZoneId(), Commands::KILL_ENEMY, (char*)&id, 4, true);
		IoManager::Instance().Write(Verbosity::DEBUG, L"Enemy %i killed", enemyId);
		Zone* zone = m_zoneManager->GetZone(iter->second->GetZoneId());

		Enemy* enemy = iter->second;
		zone->RemoveEnemy(enemy);
		RemoveEnemy(enemy);
		delete enemy;

		if (zone != nullptr && zone->m_enemyToSimPlayerMap.empty())
		{
			m_zoneManager->BroadcastZoneAll(zone->m_id, Commands::DUNGEON_CLEAR, nullptr, 0, true);
			IoManager::Instance().Write(L"\nDUNGEON CLEARED AWWWWWW YEEEAAAAAAHHHHH\n");
		}
	}
}

void GameManager::Message(uint32_t playerId, char* data, uint16_t length)
{
	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(playerId);
	if (iter != m_players.end())
	{
		MESSAGE_STRUCT m;
		m.id = playerId;
		memset(m.buffer, '\0', 1300);
		const char* name = iter->second->GetName();
		int nameLength = strlen(name) + 1;
		memcpy(m.buffer, name, nameLength);
		memcpy(m.buffer + nameLength, data, length);

		m_server->BroadcastAll((byte)Commands::MESSAGE, (char*)&m, 4 + nameLength + length, true);
	}
}

void GameManager::Update()
{
	m_zoneManager->UpdateEnemies();

	char updateBuffer[64];
	char* updateBufferHead = updateBuffer;

	// Update players with all relevant player data that has changed on the server.
	for (auto playerPair : m_players)
	{
		Player* player = playerPair.second;
		const std::set<PlayerData>* dirtyData = player->GetDirtyData();
		player->Update();
		if (!dirtyData->empty())
		{
			// The player has dirty data, so pack and broadcast an UPDATE_PLAYER packet
			// with the player's dirty data to all players in the player's zone.
			uint32_t playerId = player->GetId();
			*(uint32_t*)updateBufferHead = playerId;
			updateBufferHead += 4;

			for (auto data : *dirtyData)
			{
				*(uint8_t*)updateBufferHead = (uint8_t)data;
				++updateBufferHead;

				switch (data)
				{
					case PlayerData::HEALTH:
						*(int32_t*)updateBufferHead = player->GetHealth();
						updateBufferHead += 4;
						break;

					case PlayerData::STRENGTH:
						*(int32_t*)updateBufferHead = player->GetStrength();
						updateBufferHead += 4;
						break;

					case PlayerData::STAMINA:
						*(int32_t*)updateBufferHead = player->GetStamina();
						updateBufferHead += 4;
						break;

					case PlayerData::INTELLIGENCE:
						*(int32_t*)updateBufferHead = player->GetIntelligence();
						updateBufferHead += 4;
						break;

					case PlayerData::DEXTERITY:
						*(int32_t*)updateBufferHead = player->GetDexterity();
						updateBufferHead += 4;
						break;

					case PlayerData::BLOCKING:
						*(bool*)updateBufferHead = player->GetBlocking();
						updateBufferHead += 1;
						break;
				}
			}

			player->ClearDirtyData();

			m_zoneManager->BroadcastZoneAll(player->GetZoneId(), Commands::UPDATE_PLAYER, updateBuffer, updateBufferHead - updateBuffer, true);
			updateBufferHead = updateBuffer;
		}
	}

	// Update players with all relevant enemy data that has changed on the server.
	for (auto enemyPair : m_enemies)
	{
		Enemy* enemy = enemyPair.second;
		const std::set<EnemyData>* dirtyData = enemy->GetDirtyData();
		enemy->Update();
		if (!dirtyData->empty())
		{
			// The enemy has dirty data, so pack and broadcast an UPDATE_ENEMY packet
			// with the enemy's dirty data to all players in the enemy's zone.
			uint32_t enemyId = enemy->GetId();
			*(uint32_t*)updateBufferHead = enemyId;
			updateBufferHead += 4;

			for (auto data : *dirtyData)
			{
				*(uint8_t*)updateBufferHead = (uint8_t)data;
				++updateBufferHead;

				switch (data)
				{
					case EnemyData::HEALTH:
						*(int32_t*)updateBufferHead = enemy->GetHealth();
						updateBufferHead += 4;
						break;

					case EnemyData::BLOCKING:
						*(bool*)updateBufferHead = enemy->GetBlocking();
						updateBufferHead += 1;
						break;
				}
			}

			enemy->ClearDirtyData();

			m_zoneManager->BroadcastZoneAll(enemy->GetZoneId(), Commands::UPDATE_ENEMY, updateBuffer, updateBufferHead - updateBuffer, true);
			updateBufferHead = updateBuffer;
		}
	}
}

void GameManager::LogPlayer(uint32_t playerId)
{
	std::unordered_map<uint32_t, Player*>::iterator iter = m_players.find(playerId);
	if (iter != m_players.end())
	{
		const char* name = iter->second->GetName();
		IoManager::Instance().Write(L"Player %i ", iter->second->GetId());
		IoManager::Instance().Write(L"\tName: %s ", name);
		IoManager::Instance().Write(L"\tZone %i ", iter->second->GetZoneId());
		IoManager::Instance().Write(L"\tParty %i ", iter->second->GetPartyId());
		IoManager::Instance().Write(L"\tHealth: %i", iter->second->GetHealth());
	}
}

void GameManager::LogEnemy(uint32_t enemyId)
{
	std::unordered_map<uint32_t, Enemy*>::iterator iter = m_enemies.find(enemyId);
	if (iter != m_enemies.end())
	{
		IoManager::Instance().Write(L"Enemy %i ", iter->second->GetId());
		IoManager::Instance().Write(L"\tZone %i ", iter->second->GetZoneId());
		IoManager::Instance().Write(L"\tHealth: %i", iter->second->GetHealth());
	}
	else
		IoManager::Instance().Write(L"Enemy %i not found", enemyId);
}

void GameManager::LogEnemies()
{
	if (m_enemies.size() == 0)
		IoManager::Instance().Write(L"No enemies.");
	else
	{
		for (auto enemy : m_enemies)
		{
			LogEnemy(enemy.first);
		}
	}
}