#include "Server.h"

Server::Server()
{
	m_isExiting = false;
    m_clientIDCounter = 1;
	m_zoneManager = new ZoneManager(this);
	m_partyManager = new PartyManager(this, m_zoneManager);
	m_gameManager = new GameManager(this, m_zoneManager, m_partyManager);
	m_zoneManager->SetGameManager(m_gameManager);
}

Server::~Server()
{
	delete m_gameManager;
	delete m_partyManager;
	delete m_zoneManager;
}

uint64_t Server::HashAddress(sockaddr_in data)
{
	uint64_t port64 = (uint64_t)data.sin_port;
	uint64_t IP64 = (uint64_t)data.sin_addr.S_un.S_addr;
	return (port64 << 32) + IP64;
}

int Server::Init()
{
	struct sockaddr_in server;

	IoManager::Instance().Write(L"Initialising Winsock.");
	if (WSAStartup(MAKEWORD(2, 2), &m_wsa) != 0)
	{
		IoManager::Instance().Write(L"Failed to initialize Winsock. Error code: %d.", WSAGetLastError());
		WSACleanup();
		return EXIT_FAILURE;
	}

	IoManager::Instance().Write(L"Creating a socket.");
	if ((m_socket = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
	{
		IoManager::Instance().Write(L"Failed to create socket. Error code: %d.", WSAGetLastError());
		WSACleanup();
		return EXIT_FAILURE;
	}

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);

	unsigned long l = 1;
	ioctlsocket(m_socket, FIONBIO, &l);

	IoManager::Instance().Write(L"Binding the socket.");
	if (bind(m_socket, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
	{
		IoManager::Instance().Write(L"Failed to bind the socket. Error code: %d.", WSAGetLastError());
		Shutdown();
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int Server::Start()
{
	m_socketAddressSize = sizeof(sockaddr_in);

	const UINT inputCommandBufferLength = 512;
	WCHAR inputCommandBuffer[inputCommandBufferLength];

	IoManager::Instance().Write(L"Starting the server loop.\nListening for network data.\nType commands to interact with the server.");
	while (1)
	{
		if (m_isExiting && m_serverDisconnectAckMap.empty())
		{
			// All of the clients have acked the server disconnect packet, so safely exit the server.
			Shutdown();
			return EXIT_SUCCESS;
		}

		// Process an input command if there is one.
		if (IoManager::Instance().ReadInputCommand(inputCommandBuffer, inputCommandBufferLength))
		{
			if (!wcscmp(inputCommandBuffer, L"e"))
			{
				// Let all of the clients know the server is exiting.
				BroadcastAll((byte)Commands::DISCONNECT, nullptr, 0, true);

				// Store the ID of each server disconnect ack packet that should be received by each client.
				for (auto const& cl : m_clientMap)
				{
					Client* client = cl.second;
					m_serverDisconnectAckMap[client] = client->m_packetCounter;
				}

				m_isExiting = true;
			}
			//Log all players
			else if (!wcscmp(inputCommandBuffer, L"pa"))
			{
				if (m_idToClientMap.size() == 0)
					IoManager::Instance().Write(L"No players.");
				for (auto player : m_idToClientMap)
				{
					m_gameManager->LogPlayer(player.second->m_id);
					player.second->LogWaitingMessages();
				}
			}
			//Log player data
			else if (!wcsncmp(inputCommandBuffer, L"p", 1))
			{
				WCHAR playerID[4]{ 0 };
				wcscpy_s(playerID, inputCommandBuffer + 1);
				uint32_t id = wcstoul(playerID, nullptr, 0);
				bool found = false;
				std::map<uint32_t, Client*>::iterator iter = m_idToClientMap.find(id);
				if (iter != m_idToClientMap.end())
				{
					m_gameManager->LogPlayer(iter->second->m_id);
					iter->second->LogWaitingMessages();
				}
				else
				{
					IoManager::Instance().Write(L"Player %i not found", id);
				}
			}
			
			//Log all enemies
			else if (!wcscmp(inputCommandBuffer, L"ea"))
			{
				m_gameManager->LogEnemies();
			}
			//Log enemy data
			else if (!wcsncmp(inputCommandBuffer, L"e", 1))
			{
				WCHAR enemyId[4]{ 0 };
				wcscpy_s(enemyId, inputCommandBuffer + 1);
				uint32_t id = wcstoul(enemyId, nullptr, 0);
				m_gameManager->LogEnemy(id);
			}

			//Log all zones
			else if (!wcscmp(inputCommandBuffer, L"za"))
			{
				m_zoneManager->LogZones();
			}
			//Log zone data
			else if (!wcsncmp(inputCommandBuffer, L"z", 1))
			{
				WCHAR zoneId[4]{ 0 };
				wcscpy_s(zoneId, inputCommandBuffer + 1);
				uint32_t id = wcstoul(zoneId, nullptr, 0);
				m_zoneManager->LogZone(id);
			}

			//Log all parties
			else if (!wcscmp(inputCommandBuffer, L"ga"))
			{
				m_partyManager->LogParties();
			}
			//Log party data
			else if (!wcsncmp(inputCommandBuffer, L"g", 1))
			{
				WCHAR partyId[4]{ 0 };
				wcscpy_s(partyId, inputCommandBuffer + 1);
				uint32_t id = wcstoul(partyId, nullptr, 0);
				m_partyManager->LogParty(id);
			}
			
			//Set verbosity level
			else if (!wcsncmp(inputCommandBuffer, L"v", 1))
			{
				WCHAR verbosityLevel[2]{ 0 };
				wcscpy_s(verbosityLevel, inputCommandBuffer + 1);
				uint32_t verbosity = wcstoul(verbosityLevel, nullptr, 0);
				if ((Verbosity)verbosity <= Verbosity::LITERALLY_EVERYTHING)
				{
					IoManager::Instance().SetVerbosity((Verbosity)verbosity);
					IoManager::Instance().Write(L"Verbosity set to %i", verbosity);
				}
			}
			else
			{
				IoManager::Instance().Write(L"Invalid input command: '%ls'.", inputCommandBuffer);
			}
		}

		//Check if there is waiting data
		int counter = 5;
		while (Receive() && counter > 0)
		{
			Client* client = m_clientMap[HashAddress(m_inSocketData)];
			//Check if client is currently being tracked
			if (client == NULL)
			{
				m_clientMap[HashAddress(m_inSocketData)] = new Client(m_inSocketData);
				client = m_clientMap[HashAddress(m_inSocketData)];
			}

			//Process buffer recursively
			ProcessPacket(client, (char*)&m_inBuffer);

			--counter;
		}

		m_gameManager->Update();

		for (auto& cl : m_clientMap)
		{
			Client* client = cl.second;
			int bufLength = client->GetBufferLength();
			//If client has data in buffer, send it
			if (bufLength > 0)
			{
				if (sendto(m_socket, client->m_buffer, bufLength, 0, (struct sockaddr*) &client->m_connectionInfo, m_socketAddressSize) == SOCKET_ERROR)
				{
					IoManager::Instance().Write(L"sendto() failed. Error code: %d.", WSAGetLastError());
					Shutdown();
					return EXIT_FAILURE;
				}
				//Empty the buffer
				client->ClearSendData();
			}
			//Check if any packets need to be resent
			client->CheckResendPackets();

			//If we haven't received data from a client in a while, disconnect them
			if (client->CheckTimeout())
			{
				m_disconnectedClients.push_back(client);
			}
		}

		//Remove all clients that have disconnected or timed out
		for (int i = m_disconnectedClients.size() - 1; i >= 0; i--)
		{
			DisconnectClient(m_disconnectedClients.at(i));
			m_disconnectedClients.pop_back();
		}
	}
}

int Server::Receive()
{
	memset(m_inBuffer, '\0', BUFLEN);
	int recv_len;
	recv_len = recvfrom(m_socket, m_inBuffer, BUFLEN, 0, (struct sockaddr *) &m_inSocketData, &m_socketAddressSize);
	if ((recv_len) == SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		if (err != WSAEWOULDBLOCK) {
			IoManager::Instance().Write(L"recvfrom() failed. Error code: %d.", err);
			Shutdown();
			return EXIT_FAILURE;
		}
		return 0;
	}
	return 1;
}

void Server::Shutdown()
{
	closesocket(m_socket);
	WSACleanup();
}

void Server::ProcessPacket(Client* client, char* buffer)
{
	int command = (int)(byte)*buffer;
	uint8_t packetId = *(buffer + 1);
	uint16_t length = *(uint16_t*)(buffer + 2);
	char* data = buffer + HEADER_SIZE;

	client->Ping();

	ProcessPacket(client, command, packetId, data, length);

	//Check if client has stored packet that is ready for processing
	while (client->HasNextPacketStored())
	{
		GuaranteedMessage& m = client->GetNextStoredPacket();
		IoManager::Instance().Write(Verbosity::DEBUG, L"Processing stored packet %i, command %i, from player %i", m.packetID, m.command, client->m_id);
		ProcessPacket(client, m.command, m.packetID, m.data, m.length);
	}

	//check if buffer has unprocessed data
	if (data + length < m_inBuffer + BUFLEN && *(data + length) != '\0')
	{
		ProcessPacket(client, (data + length));
	}
}

void Server::ProcessPacket(Client* client, uint8_t command, uint8_t packetId, char* data, int length)
{

	switch (command)
	{
		case Commands::ACKNOWLEDGE:
			client->ConfirmGuaranteedMessage(packetId);

			if (m_isExiting)
			{
				std::map<Client*, uint8_t>::iterator iter = m_serverDisconnectAckMap.find(client);
				if (iter != m_serverDisconnectAckMap.end() && packetId == iter->second)
				{
					// The client acked the server disconnect packet, so stop tracking the packet and disconnect the client.
					m_serverDisconnectAckMap.erase(iter);
					m_disconnectedClients.push_back(client);
				}
			}
			break;

		case Commands::DISCONNECT:
		{
			m_disconnectedClients.push_back(client);
			break;
		}
		case Commands::LOGIN:
		{
			if (client->m_id == 0)
			{
				// Set up this new client and related data.
				uint32_t clientID = m_clientIDCounter++;
				client->m_id = clientID;
				m_idToClientMap[clientID] = client;
				client->SetLastProcessedPacketId(packetId);

				PushGuaranteedPacket(client->m_id, (byte)Commands::LOGIN_SUCCESS, -1, (char*)&clientID, 4);
				IoManager::Instance().Write(Verbosity::BASIC, L"Client login packet player %i", client->m_id);

				// Add the player to be associated with this client in the game manager.
				m_gameManager->AddPlayer(clientID, data, length);
			}
			client->SendAcknowledge(packetId, command);
			break;
		}
		case Commands::MESSAGE:
		{
			if (ShouldProcessPacket(client, packetId, command, data, length))
			{
				m_gameManager->Message(client->m_id, data, length);
				IoManager::Instance().Write(Verbosity::ALL, L"Message from player %i: %s", client->m_id, data);
			}
			break;
		}
		case Commands::MOVE_PLAYER:
		{
			m_gameManager->MovePlayer(data, length);
			break;
		}
		case Commands::JOIN_PARTY:
		{

			if (ShouldProcessPacket(client, packetId, command, data, length))
			{
				uint32_t partyID = *(uint32_t*)data;
				m_gameManager->JoinParty(client->m_id, partyID);
			}
			break;
		}
		case Commands::CREATE_PARTY:
		{
			if (ShouldProcessPacket(client, packetId, command, data, length))
			{
				m_gameManager->CreateParty(client->m_id);
			}
			break;
		}
		case Commands::LEAVE_PARTY:
		{
			if (ShouldProcessPacket(client, packetId, command, data, length))
			{
				uint32_t playerId = *(uint32_t*)data;
				m_gameManager->LeaveParty(playerId);
			}
			break;
		}
		case Commands::PARTY_INVITE:
		{
			if (ShouldProcessPacket(client, packetId, command, data, length))
			{
				uint32_t playerID = *(uint32_t*)data;
				m_gameManager->PartyInvite(client->m_id, playerID);
			}
			break;
		}
		case Commands::ENTER_ZONE:
		{
			if (ShouldProcessPacket(client, packetId, command, data, length))
			{
				m_gameManager->EnterZone(client->m_id, data);
			}
			break;
		}
		case Commands::PLAYER_ACTION:
		{
			if (ShouldProcessPacket(client, packetId, command, data, length))
			{
				m_gameManager->HandlePlayerAction(client->m_id, *(uint8_t*)data, data + 1, length - 1);
			}
			break;
		}
		case Commands::ENEMY_ACTION:
		{
			if (ShouldProcessPacket(client, packetId, command, data, length))
			{
				m_gameManager->HandleEnemyAction(*(uint32_t*)data, *(uint8_t*)(data + 4), data + 5, length - 5);
			}
			break;
		}
		case Commands::MOVE_ENEMY:
		{
			m_gameManager->MoveEnemy(data, length);
			break;
		}
		case Commands::PING:
			break;
		default:
		{
			IoManager::Instance().Write(Verbosity::BASIC, L"Received unknown packet, command %i, packetId %i", command, packetId);
		}
	}
}

void Server::PushPacket(uint32_t playerID, byte command, char* data, uint16_t length)
{
	std::map<uint32_t, Client*>::iterator iter = m_idToClientMap.find(playerID);
	if (iter != m_idToClientMap.end())
	{
		iter->second->AddPacket(command, data, length);
	}
}

void Server::PushGuaranteedPacket(uint32_t playerID, byte command, int16_t packetID, char* data, uint16_t length)
{
	std::map<uint32_t, Client*>::iterator iter = m_idToClientMap.find(playerID);
	if (iter != m_idToClientMap.end())
	{
		uint8_t pid;
		if (packetID < 0)
			pid = iter->second->PacketID();
		else
			pid = (uint8_t)packetID;
		IoManager::Instance().Write(Verbosity::LITERALLY_EVERYTHING, L"Sending C:%i P:%i to player %i", command, pid, iter->second->m_id);
		iter->second->AddGuaranteedPacket(command, pid, data, length, false);
	}
}

void Server::BroadcastAll(byte command, char* data, uint16_t length, bool guaranteed)
{
	Client* client;
	for (auto const& cl : m_clientMap)
	{
		client = cl.second;
		if (guaranteed)
			PushGuaranteedPacket(client->m_id, command, client->PacketID(), data, length);
		else
			PushPacket(client->m_id, command, data, length);
	}
}

void Server::DisconnectClient(Client* client)
{
	uint32_t playerId = client->m_id;
	m_gameManager->RemovePlayer(playerId);

	m_clientMap.erase(HashAddress(client->m_connectionInfo));
	m_idToClientMap.erase(playerId);
	delete client;
	IoManager::Instance().Write(Verbosity::BASIC, L"Client %i disconnected.", playerId);
}

bool Server::ShouldProcessPacket(Client* client, uint8_t packetId, uint8_t command, char* data, int length)
{
	//Check if packet is in order
	if (client->IsNextPacketId(packetId))
	{
		//Check if packet has already been processed
		if (!client->AlreadyReceivedPacket(packetId))
		{
			IoManager::Instance().Write(Verbosity::ALL, L"Processing packet %i", packetId);
			client->SendAcknowledge(packetId, command);
			return true;
		}
		//Already processed, resend ack
		else
		{
			IoManager::Instance().Write(Verbosity::ALL, L"Already processed packet %i", packetId);
			client->SendAcknowledge(packetId, command);
			return false;
		}
	}
	//Packet is out of order
	else
	{
		//May be a packet that we've already processed, resend ack
		if (!client->AlreadyReceivedPacket(packetId))
		{
			IoManager::Instance().Write(Verbosity::DEBUG, L"Storing packet %i, command %i, player %i", packetId, command, client->m_id);
			client->StoreIncomingPacket(command, packetId, data, length);
		}
		return false;
	}
}