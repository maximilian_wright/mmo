#include "ZoneManager.h"
#include "Server.h"
#include "GameManager.h"

ZoneManager::ZoneManager(Server* server)
{
	m_server = server;
	m_hubTown = new Zone(0);
}

ZoneManager::~ZoneManager()
{
}

void ZoneManager::AddInstance(uint32_t zoneID)
{
	if (zoneID != 0 && m_zones.count(zoneID) == 0)
	{
		Zone* zone = new Zone(zoneID);
		m_zones.insert(std::pair<uint32_t, Zone*>(zoneID, zone));
	}
}

void ZoneManager::RemoveInstance(uint32_t zoneID)
{
	if (zoneID != 0 && m_zones.count(zoneID) > 0)
	{
		Zone* zone = m_zones[zoneID];
		if (zone == nullptr)
		{
			return;
		}

		for (int i = zone->MemberCount(); i > 0; i--)
		{
			AddMember(0, zone->m_members[i-1]);
		}

		if (m_zones.count(zoneID) > 0)
		{
			// Remove and destroy all enemies in the zone.
			for (auto enemyPlayerPair : zone->m_enemyToSimPlayerMap)
			{
				Enemy* enemy = enemyPlayerPair.first;
				m_gameManager->RemoveEnemy(enemy);
				delete enemy;
			}

			m_zones.erase(zoneID);
			delete zone;
		}
	}
}

void ZoneManager::AddMember(uint32_t zoneID, Player* player)
{
	if (zoneID != player->GetZoneId())
	{
		RemoveMember(player);
	}

	bool creatingInstance = false;
	if (zoneID != 0 && !HasInstance(zoneID))
	{
		// The dungeon instance does not exist, so make it.
		AddInstance(zoneID);
		creatingInstance = true;
	}

	Zone* zone = GetZone(zoneID);
	zone->AddMember(player);

	// Set the player's data to default/zone specific values.
	player->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
	player->SetRotationY(0.0f);
	player->SetVelocityMag(0.0f);
	player->SetHealth(100);

	uint32_t playerId = player->GetId();
	int nameLength = strlen(player->GetName()) + 1;
	TRACK_PLAYER_STRUCT s;
	s.id = playerId;
	s.partyId = player->GetPartyId();
	memset(s.name, '\0', 64);
	strcpy_s(s.name, 64, player->GetName());

	Vector3 playerPosition = player->GetPosition();
	s.moveStruct.positionX = playerPosition.x;
	s.moveStruct.positionY = playerPosition.y;
	s.moveStruct.positionZ = playerPosition.z;
	s.moveStruct.rotationY = player->GetRotationY();
	s.moveStruct.velocityMag = player->GetVelocityMag();

	uint8_t* buffer = s.playerDataBuffer;

	*(uint8_t*)buffer = (uint8_t)PlayerData::HEALTH;
	++buffer;
	*(int32_t*)buffer = player->GetHealth();
	buffer += 4;

	*(uint8_t*)buffer = (uint8_t)PlayerData::STRENGTH;
	++buffer;
	*(int32_t*)buffer = player->GetStrength();
	buffer += 4;

	*(uint8_t*)buffer = (uint8_t)PlayerData::STAMINA;
	++buffer;
	*(int32_t*)buffer = player->GetStamina();
	buffer += 4;

	*(uint8_t*)buffer = (uint8_t)PlayerData::INTELLIGENCE;
	++buffer;
	*(int32_t*)buffer = player->GetIntelligence();
	buffer += 4;

	*(uint8_t*)buffer = (uint8_t)PlayerData::DEXTERITY;
	++buffer;
	*(int32_t*)buffer = player->GetDexterity();
	buffer += 4;

	*(uint8_t*)buffer = (uint8_t)PlayerData::BLOCKING;
	++buffer;
	*(bool*)buffer = player->GetBlocking();
	buffer += 1;

	memcpy(s.name + nameLength, &s.moveStruct, sizeof(MOVE_STRUCT) + sizeof(s.playerDataBuffer));

	BroadcastZone(player, (uint8_t)Commands::TRACK_PLAYER, (char*)&s, 8 + nameLength + sizeof(MOVE_STRUCT) + sizeof(s.playerDataBuffer), true);

	for (auto pl : zone->m_members)
	{
		if (pl != player)
		{
			nameLength = strlen(pl->GetName()) + 1;
			TRACK_PLAYER_STRUCT s;
			s.id = pl->GetId();
			s.partyId = pl->GetPartyId();
			memset(s.name, '\0', 64);
			strcpy_s(s.name, 64, pl->GetName());

			Vector3 plPosition = pl->GetPosition();
			s.moveStruct.positionX = plPosition.x;
			s.moveStruct.positionY = plPosition.y;
			s.moveStruct.positionZ = plPosition.z;
			s.moveStruct.rotationY = pl->GetRotationY();
			s.moveStruct.velocityMag = pl->GetVelocityMag();

			uint8_t* buffer = s.playerDataBuffer;

			*(uint8_t*)buffer = (uint8_t)PlayerData::HEALTH;
			++buffer;
			*(int32_t*)buffer = pl->GetHealth();
			buffer += 4;

			*(uint8_t*)buffer = (uint8_t)PlayerData::STRENGTH;
			++buffer;
			*(int32_t*)buffer = pl->GetStrength();
			buffer += 4;

			*(uint8_t*)buffer = (uint8_t)PlayerData::STAMINA;
			++buffer;
			*(int32_t*)buffer = pl->GetStamina();
			buffer += 4;

			*(uint8_t*)buffer = (uint8_t)PlayerData::INTELLIGENCE;
			++buffer;
			*(int32_t*)buffer = pl->GetIntelligence();
			buffer += 4;

			*(uint8_t*)buffer = (uint8_t)PlayerData::DEXTERITY;
			++buffer;
			*(int32_t*)buffer = pl->GetDexterity();
			buffer += 4;

			*(uint8_t*)buffer = (uint8_t)PlayerData::BLOCKING;
			++buffer;
			*(bool*)buffer = pl->GetBlocking();
			buffer += 1;

			memcpy(s.name + nameLength, &s.moveStruct, sizeof(MOVE_STRUCT) + sizeof(buffer));

			m_server->PushGuaranteedPacket(playerId, (uint8_t)Commands::TRACK_PLAYER, -1, (char*)&s, 4 + nameLength + sizeof(MOVE_STRUCT));
		}
	}

	if (creatingInstance)
	{
		// Create all of the enemies for the new zone instance.
		Enemy* enemies[] =
		{
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::GOBLIN, Vector3(-34.24302f, 5.5f, 34.19709f), 0.0f, 0.0f, 0, 100),
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::GOBLIN, Vector3(-37.74f, 5.5f, 37.73f), 0.0f, 0.0f, 0, 100),
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::GOBLIN, Vector3(-28.92f, 5.5f, 35.85f), 0.0f, 0.0f, 0, 100),
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::GOBLIN, Vector3(25.67f, 5.5f, 47.5f), 0.0f, 0.0f, 0, 100),
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::GOBLIN, Vector3(20.35f, 5.5f, 45.85f), 0.0f, 0.0f, 0, 100),
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::CAVEWORM, Vector3(-50.29331f, 8.503113f, 64.46258f), 0.0f, 0.0f, 0, 100),
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::CAVEWORM, Vector3(-48.9f, 8.503113f, 74.08f), 0.0f, 0.0f, 0, 100),
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::SPIDER, Vector3(-23.06f, 5.5f, 61.45f), 0.0f, 0.0f, 0, 100),
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::SPIDER, Vector3(23.91f, 5.5f, 69.84f), 0.0f, 0.0f, 0, 100),
			new Enemy(m_gameManager->GetNewEnemyId(), zoneID, EnemyType::SPIDER, Vector3(-29.96f, 5.5f, 68.09f), 0.0f, 0.0f, 0, 100)
		};

		for (auto enemy : enemies)
		{
			m_gameManager->AddEnemy(enemy);
			zone->AddEnemy(enemy);
		}
	}

	// Send a TRACK_ENEMY packet for each enemy in the zone, containing each enemy's data, to the player.
	for (auto enemyPlayerPair : zone->m_enemyToSimPlayerMap)
	{
		Enemy* enemy = enemyPlayerPair.first;
		char trackEnemyBuffer[32];
		char* trackEnemyBufferHead = trackEnemyBuffer;

		*(uint32_t*)trackEnemyBufferHead = enemy->GetId();
		trackEnemyBufferHead += 4;

		*(uint8_t*)trackEnemyBufferHead = (uint8_t)enemy->GetType();
		++trackEnemyBufferHead;

		Vector3 enemyPosition = enemy->GetPosition();
		*(float*)trackEnemyBufferHead = enemyPosition.x;
		trackEnemyBufferHead += 4;
		*(float*)trackEnemyBufferHead = enemyPosition.y;
		trackEnemyBufferHead += 4;
		*(float*)trackEnemyBufferHead = enemyPosition.z;
		trackEnemyBufferHead += 4;

		*(float*)trackEnemyBufferHead = enemy->GetRotationY();
		trackEnemyBufferHead += 4;

		*(float*)trackEnemyBufferHead = enemy->GetVelocityMag();
		trackEnemyBufferHead += 4;

		*(uint8_t*)trackEnemyBufferHead = (uint8_t)EnemyData::HEALTH;
		++trackEnemyBufferHead;
		*(uint32_t*)trackEnemyBufferHead = enemy->GetHealth();
		trackEnemyBufferHead += 4;

		*(uint8_t*)trackEnemyBufferHead = (uint8_t)EnemyData::BLOCKING;
		++trackEnemyBufferHead;
		*(uint8_t*)trackEnemyBufferHead = enemy->GetBlocking();

		m_server->PushGuaranteedPacket(playerId, Commands::TRACK_ENEMY, -1, trackEnemyBuffer, sizeof(trackEnemyBuffer));
	}

	if (creatingInstance)
	{
		// Delegate the simulation of each enemy to the player (start enemy idling).
		// Send a START_SIMULATING_ENEMY packet for each enemy in the zone to the player.
		for (auto enemyPlayerPair : zone->m_enemyToSimPlayerMap)
		{
			Enemy* enemy = enemyPlayerPair.first;
			zone->m_enemyToSimPlayerMap[enemy] = player;

			char startSimulatingEnemyBuffer[28];
			char* startSimulatingEnemyBufferHead = startSimulatingEnemyBuffer;

			*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetId();
			startSimulatingEnemyBufferHead += 4;

			Vector3 enemyPosition = enemy->GetPosition();
			*(float*)startSimulatingEnemyBufferHead = enemyPosition.x;
			startSimulatingEnemyBufferHead += 4;
			*(float*)startSimulatingEnemyBufferHead = enemyPosition.y;
			startSimulatingEnemyBufferHead += 4;
			*(float*)startSimulatingEnemyBufferHead = enemyPosition.z;
			startSimulatingEnemyBufferHead += 4;

			*(float*)startSimulatingEnemyBufferHead = enemy->GetRotationY();
			startSimulatingEnemyBufferHead += 4;

			*(float*)startSimulatingEnemyBufferHead = enemy->GetVelocityMag();
			startSimulatingEnemyBufferHead += 4;

			*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetTargetId();

			m_server->PushGuaranteedPacket(playerId, Commands::START_SIMULATING_ENEMY, -1, startSimulatingEnemyBuffer, 28);
		}
	}
}

void ZoneManager::RemoveMember(Player* player)
{
	uint32_t zoneID = player->GetZoneId();
	uint32_t playerID = player->GetId();
	if (zoneID == 0)
	{
		BroadcastZone(player, Commands::UNTRACK_PLAYER, (char*)&playerID, 4, true);
		m_hubTown->RemoveMember(player);
	}
	else if (m_zones.count(zoneID) == 1)
	{
		BroadcastZone(player, Commands::UNTRACK_PLAYER, (char*)&playerID, 4, true);
		m_zones[zoneID]->RemoveMember(player);
		//delete instance if it isn't the HubTown and it's empty
		if (zoneID != 0 && m_zones[zoneID]->MemberCount() == 0)
		{
			RemoveInstance(zoneID);
		}
	}
	
}

void ZoneManager::UpdateEnemies()
{
	for (auto idZonePair : m_zones)
	{
		Zone* zone = idZonePair.second;

		for (auto enemyPlayerPair : zone->m_enemyToSimPlayerMap)
		{
			// AI Guide:
			// If enemy targetId has changed and is different from the player that's simulating them (ex. taunt), then make player with targetid simulate enemy and stop simulate current player.
			// When a player disconnects, player will be nullptr, targetid will be 0, unless it changed to another player after disconnect then it should make player with targetId simulate enemy.
			// When a player dies, they won't stop simulating but targetid will be 0.
			// When enemies die, they are totally destroyed/removed and no stop simulating packet is sent.

			Enemy* enemy = enemyPlayerPair.first;

			if (enemy->GetTargetId() == 0)
			{
				float enemyTargetRangeSquared = 64.0f;
				Player* nearestPlayer = nullptr;
				float minDistanceSquared = FLT_MAX;

				// Loop through the players, searching for the nearest living player to the enemy.
				for (auto player : zone->m_members)
				{
					if (player->GetHealth() > 0)
					{
						float distanceSquared = squaredLength(player->GetPosition() - enemy->GetPosition());
						if (distanceSquared < minDistanceSquared)
						{
							nearestPlayer = player;
							minDistanceSquared = distanceSquared;
						}
					}
				}

				if (enemyPlayerPair.second == nullptr)
				{
					if (nearestPlayer != nullptr)
					{
						if (minDistanceSquared <= enemyTargetRangeSquared)
						{
							// The nearest player is within the enemy's target range, so make the enemy target the player.
							enemy->SetTargetId(nearestPlayer->GetId());
						}
						else
						{
							bool foundPlayerTarget = false;
							minDistanceSquared = FLT_MAX;

							for (auto enemyPlayerPair2 : zone->m_enemyToSimPlayerMap)
							{
								Enemy* enemy2 = enemyPlayerPair2.first;
								float distanceSquared = squaredLength(enemy2->GetPosition() - enemy->GetPosition());

								if (enemy2 != enemy &&
									distanceSquared <= enemyTargetRangeSquared &&
									enemy2->GetTargetId() != 0 &&
									distanceSquared < minDistanceSquared)
								{
									// Loop through the players, searching for the target enemy's target player.
									for (auto player : zone->m_members)
									{
										if (player->GetId() == enemy2->GetTargetId())
										{
											nearestPlayer = player;
											foundPlayerTarget = true;
											break;
										}
									}

									minDistanceSquared = distanceSquared;
								}
							}

							if (foundPlayerTarget)
							{
								// The nearest enemy is within the enemy's target range and has a target, so make the enemy target the nearest enemy's player target.
								enemy->SetTargetId(nearestPlayer->GetId());
							}
						}
					}
					else
					{
						// All of the players are dead.
						// Loop through the players, searching for the nearest dead player to the enemy.
						for (auto player : zone->m_members)
						{
							float distanceSquared = squaredLength(player->GetPosition() - enemy->GetPosition());
							if (distanceSquared < minDistanceSquared)
							{
								nearestPlayer = player;
								minDistanceSquared = distanceSquared;
							}
						}

						// Make the enemy target the nearest dead player to add insult to injury...
						enemy->SetTargetId(nearestPlayer->GetId());
					}

					zone->m_enemyToSimPlayerMap[enemy] = nearestPlayer;

					// Send a START_SIMULATING_ENEMY packet for the enemy to the player.
					char startSimulatingEnemyBuffer[28];
					char* startSimulatingEnemyBufferHead = startSimulatingEnemyBuffer;

					*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetId();
					startSimulatingEnemyBufferHead += 4;

					Vector3 enemyPosition = enemy->GetPosition();
					*(float*)startSimulatingEnemyBufferHead = enemyPosition.x;
					startSimulatingEnemyBufferHead += 4;
					*(float*)startSimulatingEnemyBufferHead = enemyPosition.y;
					startSimulatingEnemyBufferHead += 4;
					*(float*)startSimulatingEnemyBufferHead = enemyPosition.z;
					startSimulatingEnemyBufferHead += 4;

					*(float*)startSimulatingEnemyBufferHead = enemy->GetRotationY();
					startSimulatingEnemyBufferHead += 4;

					*(float*)startSimulatingEnemyBufferHead = enemy->GetVelocityMag();
					startSimulatingEnemyBufferHead += 4;

					*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetTargetId();

					m_server->PushGuaranteedPacket(nearestPlayer->GetId(), Commands::START_SIMULATING_ENEMY, -1, startSimulatingEnemyBuffer, 28);
				}
				else if (nearestPlayer != nullptr)
				{
					if (minDistanceSquared <= enemyTargetRangeSquared)
					{
						// The nearest player is within the enemy's target range, so make the enemy target the player.
						enemy->SetTargetId(nearestPlayer->GetId());

						if (nearestPlayer != enemyPlayerPair.second)
						{
							// Send a STOP_SIMULATING_ENEMY packet for the enemy to the current simulating player.
							uint32_t enemyId = enemy->GetId();
							m_server->PushGuaranteedPacket(enemyPlayerPair.second->GetId(), Commands::STOP_SIMULATING_ENEMY, -1, (char*)&enemyId, 4);

							zone->m_enemyToSimPlayerMap[enemy] = nearestPlayer;
						}

						// Send a START_SIMULATING_ENEMY packet for the enemy to the player.
						char startSimulatingEnemyBuffer[28];
						char* startSimulatingEnemyBufferHead = startSimulatingEnemyBuffer;

						*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetId();
						startSimulatingEnemyBufferHead += 4;

						Vector3 enemyPosition = enemy->GetPosition();
						*(float*)startSimulatingEnemyBufferHead = enemyPosition.x;
						startSimulatingEnemyBufferHead += 4;
						*(float*)startSimulatingEnemyBufferHead = enemyPosition.y;
						startSimulatingEnemyBufferHead += 4;
						*(float*)startSimulatingEnemyBufferHead = enemyPosition.z;
						startSimulatingEnemyBufferHead += 4;

						*(float*)startSimulatingEnemyBufferHead = enemy->GetRotationY();
						startSimulatingEnemyBufferHead += 4;

						*(float*)startSimulatingEnemyBufferHead = enemy->GetVelocityMag();
						startSimulatingEnemyBufferHead += 4;

						*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetTargetId();

						m_server->PushGuaranteedPacket(nearestPlayer->GetId(), Commands::START_SIMULATING_ENEMY, -1, startSimulatingEnemyBuffer, 28);
					}
					else
					{
						bool foundPlayerTarget = false;
						minDistanceSquared = FLT_MAX;

						for (auto enemyPlayerPair2 : zone->m_enemyToSimPlayerMap)
						{
							Enemy* enemy2 = enemyPlayerPair2.first;
							float distanceSquared = squaredLength(enemy2->GetPosition() - enemy->GetPosition());

							if (enemy2 != enemy &&
								distanceSquared <= enemyTargetRangeSquared &&
								enemy2->GetTargetId() != 0 &&
								distanceSquared < minDistanceSquared)
							{
								// Loop through the players, searching for the target enemy's target player.
								for (auto player : zone->m_members)
								{
									if (player->GetId() == enemy2->GetTargetId())
									{
										nearestPlayer = player;
										foundPlayerTarget = true;
										break;
									}
								}

								minDistanceSquared = distanceSquared;
							}
						}

						if (foundPlayerTarget)
						{
							// The nearest enemy is within the enemy's target range and has a target, so make the enemy target the nearest enemy's player target.
							enemy->SetTargetId(nearestPlayer->GetId());

							if (nearestPlayer != enemyPlayerPair.second)
							{
								// Send a STOP_SIMULATING_ENEMY packet for the enemy to the current simulating player.
								uint32_t enemyId = enemy->GetId();
								m_server->PushGuaranteedPacket(enemyPlayerPair.second->GetId(), Commands::STOP_SIMULATING_ENEMY, -1, (char*)&enemyId, 4);

								zone->m_enemyToSimPlayerMap[enemy] = nearestPlayer;
							}

							// Send a START_SIMULATING_ENEMY packet for the enemy to the player.
							char startSimulatingEnemyBuffer[28];
							char* startSimulatingEnemyBufferHead = startSimulatingEnemyBuffer;

							*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetId();
							startSimulatingEnemyBufferHead += 4;

							Vector3 enemyPosition = enemy->GetPosition();
							*(float*)startSimulatingEnemyBufferHead = enemyPosition.x;
							startSimulatingEnemyBufferHead += 4;
							*(float*)startSimulatingEnemyBufferHead = enemyPosition.y;
							startSimulatingEnemyBufferHead += 4;
							*(float*)startSimulatingEnemyBufferHead = enemyPosition.z;
							startSimulatingEnemyBufferHead += 4;

							*(float*)startSimulatingEnemyBufferHead = enemy->GetRotationY();
							startSimulatingEnemyBufferHead += 4;

							*(float*)startSimulatingEnemyBufferHead = enemy->GetVelocityMag();
							startSimulatingEnemyBufferHead += 4;

							*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetTargetId();

							m_server->PushGuaranteedPacket(nearestPlayer->GetId(), Commands::START_SIMULATING_ENEMY, -1, startSimulatingEnemyBuffer, 28);
						}
					}
				}
				else
				{
					// All of the players are dead.
					// Loop through the players, searching for the nearest dead player to the enemy.
					for (auto player : zone->m_members)
					{
						float distanceSquared = squaredLength(player->GetPosition() - enemy->GetPosition());
						if (distanceSquared < minDistanceSquared)
						{
							nearestPlayer = player;
							minDistanceSquared = distanceSquared;
						}
					}

					// Make the enemy target the nearest dead player to add insult to injury...
					enemy->SetTargetId(nearestPlayer->GetId());

					if (nearestPlayer != enemyPlayerPair.second)
					{
						// Send a STOP_SIMULATING_ENEMY packet for the enemy to the current simulating player.
						uint32_t enemyId = enemy->GetId();
						m_server->PushGuaranteedPacket(enemyPlayerPair.second->GetId(), Commands::STOP_SIMULATING_ENEMY, -1, (char*)&enemyId, 4);

						zone->m_enemyToSimPlayerMap[enemy] = nearestPlayer;
					}

					// Send a START_SIMULATING_ENEMY packet for the enemy to the player.
					char startSimulatingEnemyBuffer[28];
					char* startSimulatingEnemyBufferHead = startSimulatingEnemyBuffer;

					*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetId();
					startSimulatingEnemyBufferHead += 4;

					Vector3 enemyPosition = enemy->GetPosition();
					*(float*)startSimulatingEnemyBufferHead = enemyPosition.x;
					startSimulatingEnemyBufferHead += 4;
					*(float*)startSimulatingEnemyBufferHead = enemyPosition.y;
					startSimulatingEnemyBufferHead += 4;
					*(float*)startSimulatingEnemyBufferHead = enemyPosition.z;
					startSimulatingEnemyBufferHead += 4;

					*(float*)startSimulatingEnemyBufferHead = enemy->GetRotationY();
					startSimulatingEnemyBufferHead += 4;

					*(float*)startSimulatingEnemyBufferHead = enemy->GetVelocityMag();
					startSimulatingEnemyBufferHead += 4;

					*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetTargetId();

					m_server->PushGuaranteedPacket(nearestPlayer->GetId(), Commands::START_SIMULATING_ENEMY, -1, startSimulatingEnemyBuffer, 28);
				}
			}
			else if (enemyPlayerPair.second == nullptr)
			{
				Player* targetPlayer = nullptr;

				// Loop through the players, searching for the enemy's target player.
				for (auto player : zone->m_members)
				{
					if (player->GetId() == enemy->GetTargetId())
					{
						targetPlayer = player;
						break;
					}
				}

				zone->m_enemyToSimPlayerMap[enemy] = targetPlayer;

				// Send a START_SIMULATING_ENEMY packet for the enemy to the player.
				char startSimulatingEnemyBuffer[28];
				char* startSimulatingEnemyBufferHead = startSimulatingEnemyBuffer;

				*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetId();
				startSimulatingEnemyBufferHead += 4;

				Vector3 enemyPosition = enemy->GetPosition();
				*(float*)startSimulatingEnemyBufferHead = enemyPosition.x;
				startSimulatingEnemyBufferHead += 4;
				*(float*)startSimulatingEnemyBufferHead = enemyPosition.y;
				startSimulatingEnemyBufferHead += 4;
				*(float*)startSimulatingEnemyBufferHead = enemyPosition.z;
				startSimulatingEnemyBufferHead += 4;

				*(float*)startSimulatingEnemyBufferHead = enemy->GetRotationY();
				startSimulatingEnemyBufferHead += 4;

				*(float*)startSimulatingEnemyBufferHead = enemy->GetVelocityMag();
				startSimulatingEnemyBufferHead += 4;

				*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetTargetId();

				m_server->PushGuaranteedPacket(targetPlayer->GetId(), Commands::START_SIMULATING_ENEMY, -1, startSimulatingEnemyBuffer, 28);
			}
			else if (enemy->GetTargetId() == enemyPlayerPair.second->GetId() && enemy->GetTargetId() != enemy->GetPrevTargetId())
			{
				// The enemy's target is the player that is already simulating it but the enemy's target has changed since last update.
				// A START_SIMULATING_ENEMY packet still needs to be sent in order to tell the player that the enemy needs to target them since it wasn't previously targeting them.
				Player* targetPlayer = enemyPlayerPair.second;

				// Send a START_SIMULATING_ENEMY packet for the enemy to the player.
				char startSimulatingEnemyBuffer[28];
				char* startSimulatingEnemyBufferHead = startSimulatingEnemyBuffer;

				*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetId();
				startSimulatingEnemyBufferHead += 4;

				Vector3 enemyPosition = enemy->GetPosition();
				*(float*)startSimulatingEnemyBufferHead = enemyPosition.x;
				startSimulatingEnemyBufferHead += 4;
				*(float*)startSimulatingEnemyBufferHead = enemyPosition.y;
				startSimulatingEnemyBufferHead += 4;
				*(float*)startSimulatingEnemyBufferHead = enemyPosition.z;
				startSimulatingEnemyBufferHead += 4;

				*(float*)startSimulatingEnemyBufferHead = enemy->GetRotationY();
				startSimulatingEnemyBufferHead += 4;

				*(float*)startSimulatingEnemyBufferHead = enemy->GetVelocityMag();
				startSimulatingEnemyBufferHead += 4;

				*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetTargetId();

				m_server->PushGuaranteedPacket(targetPlayer->GetId(), Commands::START_SIMULATING_ENEMY, -1, startSimulatingEnemyBuffer, 28);
			}
			else if (enemy->GetTargetId() != enemyPlayerPair.second->GetId())
			{
				Player* targetPlayer = nullptr;

				// Send a STOP_SIMULATING_ENEMY packet for the enemy to the current simulating player.
				uint32_t enemyId = enemy->GetId();
				m_server->PushGuaranteedPacket(enemyPlayerPair.second->GetId(), Commands::STOP_SIMULATING_ENEMY, -1, (char*)&enemyId, 4);

				// Loop through the players, searching for the enemy's target player.
				for (auto player : zone->m_members)
				{
					if (player->GetId() == enemy->GetTargetId())
					{
						targetPlayer = player;
						break;
					}
				}

				zone->m_enemyToSimPlayerMap[enemy] = targetPlayer;

				// Send a START_SIMULATING_ENEMY packet for the enemy to the player.
				char startSimulatingEnemyBuffer[28];
				char* startSimulatingEnemyBufferHead = startSimulatingEnemyBuffer;

				*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetId();
				startSimulatingEnemyBufferHead += 4;

				Vector3 enemyPosition = enemy->GetPosition();
				*(float*)startSimulatingEnemyBufferHead = enemyPosition.x;
				startSimulatingEnemyBufferHead += 4;
				*(float*)startSimulatingEnemyBufferHead = enemyPosition.y;
				startSimulatingEnemyBufferHead += 4;
				*(float*)startSimulatingEnemyBufferHead = enemyPosition.z;
				startSimulatingEnemyBufferHead += 4;

				*(float*)startSimulatingEnemyBufferHead = enemy->GetRotationY();
				startSimulatingEnemyBufferHead += 4;

				*(float*)startSimulatingEnemyBufferHead = enemy->GetVelocityMag();
				startSimulatingEnemyBufferHead += 4;

				*(uint32_t*)startSimulatingEnemyBufferHead = enemy->GetTargetId();

				m_server->PushGuaranteedPacket(targetPlayer->GetId(), Commands::START_SIMULATING_ENEMY, -1, startSimulatingEnemyBuffer, 28);
			}

			enemy->SetPrevTargetId(enemy->GetTargetId());
		}
	}
}

void ZoneManager::BroadcastZone(Player* player, uint8_t command, char* data, uint16_t length, bool guaranteed)
{
	Zone* zone = GetZone(player->GetZoneId());
	if (zone != nullptr)
	{
		for (auto other : zone->m_members)
		{
			if (player != other)
			{
				if (guaranteed)
				{
					m_server->PushGuaranteedPacket(other->GetId(), command, -1, data, length);
				}
				else
				{
					m_server->PushPacket(other->GetId(), command, data, length);
				}
			}
		}
	}
}

void ZoneManager::BroadcastZone(Enemy* enemy, uint8_t command, char* data, uint16_t length, bool guaranteed)
{
	Zone* zone = GetZone(enemy->GetZoneId());
	if (zone != nullptr)
	{
		BroadcastZone(zone->m_enemyToSimPlayerMap[enemy], command, data, length, guaranteed);
	}
}

void ZoneManager::BroadcastZoneAll(uint32_t zoneId, uint8_t command, char* data, uint16_t length, bool guaranteed)
{
	Zone* zone = GetZone(zoneId);
	if (zone != nullptr)
	{
		for (auto player : zone->m_members)
		{
			if (guaranteed)
			{
				m_server->PushGuaranteedPacket(player->GetId(), command, -1, data, length);
			}
			else
			{
				if (command == (uint8_t)Commands::DAMAGE_PLAYER || command == (uint8_t)Commands::DAMAGE_ENEMY)
					IoManager::Instance().Write(Verbosity::ALL, L"Sending damage packet");
				m_server->PushPacket(player->GetId(), command, data, length);
			}
		}
	}
}

void ZoneManager::LogZone(uint32_t id)
{
	Zone* zone = GetZone(id);
	if (zone == nullptr)
		IoManager::Instance().Write(L"Zone %i not found", id);
	else
	{
		IoManager::Instance().Write(L"Zone %i", zone->m_id);
		for (auto player : zone->m_members)
		{
			IoManager::Instance().Write(L"\tPlayer %i", player->GetId());
		}
		for (auto enemy : zone->m_enemyToSimPlayerMap)
		{
			IoManager::Instance().Write(L"\tEnemy %i, simulated on player %i", enemy.first->GetId(), enemy.second->GetId());
		}

	}
}

void ZoneManager::LogZones()
{
	LogZone(0);
	for (auto zone : m_zones)
	{
		LogZone(zone.second->m_id);
	}
}

Zone* ZoneManager::GetZone(uint32_t zoneID)
{
	if (zoneID == 0)
		return m_hubTown;
	if (m_zones.count(zoneID) > 0)
		return m_zones[zoneID];
	return nullptr;
}

bool ZoneManager::HasInstance(uint32_t id)
{
	return m_zones.count(id) != 0;
}

int ZoneManager::InstanceCount()
{
	return m_zones.size();
}