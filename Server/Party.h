#pragma once

#include "Player.h"
#include <vector>

class Party
{

public:
	Party(uint16_t id);
	~Party();

	void AddMember(Player* player);
	void RemoveMember(Player* player);

	int MemberCount();

	std::vector<Player*> m_members;
	uint16_t m_guid;



};