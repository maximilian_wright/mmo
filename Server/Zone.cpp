#include "Zone.h"

Zone::Zone(uint16_t id)
{
	m_id = id;
}

Zone::~Zone()
{

}

void Zone::AddMember(Player* player)
{
	if (std::find(m_members.begin(), m_members.end(), player) == m_members.end())
	{
		player->SetZoneId(m_id);
		m_members.push_back(player);
	}
}

void Zone::RemoveMember(Player* player)
{
	// Remove the player from the enemy simulation map for each enemy the player is simulating.
	for (auto enemyPlayerPair : m_enemyToSimPlayerMap)
	{
		if (enemyPlayerPair.second == player)
		{
			enemyPlayerPair.first->SetTargetId(0);
			m_enemyToSimPlayerMap[enemyPlayerPair.first] = nullptr;
		}
	}

	for (unsigned int i = 0; i < m_members.size(); i++)
	{
		if (m_members[i] == player)
		{
			m_members.erase(m_members.begin() + i);
			return;
		}
	}
}

int Zone::MemberCount()
{
	return m_members.size();
}

void Zone::AddEnemy(Enemy* enemy)
{
	m_enemyToSimPlayerMap[enemy] = nullptr;
}

void Zone::RemoveEnemy(Enemy* enemy)
{
	m_enemyToSimPlayerMap.erase(enemy);
}
