#include "Client.h"
#include <vector>

Client::Client() 
{
	memset(&m_buffer, '\0', BUFLEN);
	m_bufferHead = (char*)&m_buffer;
	m_id = 0;
	m_packetCounter = 0;
	m_timeoutCounter = 0;
	m_lastProcessedPacketId = 1;
	memset(&m_clientPacketStatus, 0, 256);
}

Client::Client(sockaddr_in connInfo) : Client()
{
	m_connectionInfo = connInfo;
}

Client::~Client()
{
	//clean up guaranteed messages that haven't gone through
	std::vector<uint8_t> outKeys;
	for (std::map<uint8_t, GuaranteedMessage>::iterator it = m_outPackets.begin(); it != m_outPackets.end(); it++)
	{
		outKeys.push_back(it->first);
	}
	for (unsigned int i = 0; i < outKeys.size(); i++)
	{
		ConfirmGuaranteedMessage(outKeys[i]);
	}
	std::vector<uint8_t> inKeys;
	//clean up stored packets that haven't been processed
	for (std::map<uint8_t, GuaranteedMessage>::iterator it = m_inPackets.begin(); it != m_inPackets.end(); it++)
	{
		inKeys.push_back(it->first);
	}
	for (unsigned int i = 0; i < inKeys.size(); i++)
	{
		FreeStoredPacket(inKeys[i]);
	}
	
}

int Client::GetBufferLength()
{
	return m_bufferHead - (char*)&m_buffer;
}

void Client::ClearSendData()
{
	memset(m_buffer, '\0', BUFLEN);
	m_bufferHead = (char*)&m_buffer;
}

//Generates new packet id
uint8_t Client::PacketID()
{
	m_packetCounter = m_packetCounter + 1;
	return m_packetCounter;
}

void Client::AddPacket(byte command, char* data, uint16_t length)
{
	if(m_bufferHead + length + HEADER_SIZE - m_buffer <= BUFLEN) {
		//add command byte and data length
		AddPacketHeader(command, 0, length);

		//add packet data
		AddPacketBody(data, length);
		if (m_bufferHead - m_buffer >= 1300)
			IoManager::Instance().Write(L"Buffer is full.");
	}
	else
		IoManager::Instance().Write(L"Not enough space for packet!");
}

void Client::AddGuaranteedPacket(byte command, uint8_t packetID, char* data, uint16_t length, bool isResend)
{
	if (m_bufferHead + length + HEADER_SIZE - m_buffer <= BUFLEN) 
	{

		//add command byte, packet id, and data length
		AddPacketHeader(command, packetID, length);

		//add packet data
		AddPacketBody(data, length);
		if (m_bufferHead - m_buffer >= 1300)
			IoManager::Instance().Write(L"Buffer is full.");
	}
	else
		IoManager::Instance().Write(L"Not enough space for packet!");

	if (!isResend) {
		//add packet to reliability tracking
		TrackGuaranteedMessage(command, packetID, data, length);
	}
}

void Client::StoreIncomingPacket(uint8_t command, uint8_t packetId, char* data, short length)
{
	if (m_inPackets.count(packetId) == 0)
	{
		GuaranteedMessage m;
		m.command = command;
		m.packetID = packetId;
		m.data = (char*)malloc(length);
		memcpy(m.data, data, length);
		m.length = length;
		m_inPackets[packetId] = m;
	}
}

GuaranteedMessage& Client::GetNextStoredPacket()
{
	//Should have called HasNextPacketId, so no error checking
	return m_inPackets[m_lastProcessedPacketId + 1];
}

void Client::FreeStoredPacket(uint8_t packetId)
{
	if (m_inPackets.count(packetId) > 0)
	{
		IoManager::Instance().Write(L"Freeing stored packet %i", packetId);
		free(m_inPackets[packetId].data);
		m_inPackets.erase(packetId);
		int next = ((int)m_lastProcessedPacketId + 1) % 256;
		m_lastProcessedPacketId = (uint8_t)next;
	}
}

bool Client::IsNextPacketId(uint8_t packetId)
{
	int next = ((int)m_lastProcessedPacketId + 1) % 256;
	if (packetId == next)
	{
		m_lastProcessedPacketId = (uint8_t)next;
		return true;
	}
	return false;
}

bool Client::HasNextPacketStored()
{
	int next = ((int)m_lastProcessedPacketId + 1) % 256;
	if (m_inPackets.count(next) > 0)
		return true;
	return false;
}

void Client::AddPacketHeader(byte command, uint8_t packetID, uint16_t length)
{
	*(uint8_t*)m_bufferHead = command;
	m_bufferHead++;
	*(uint8_t*)m_bufferHead = packetID;
	m_bufferHead++;
	*(uint16_t*)m_bufferHead = length;
	m_bufferHead += 2;
}

void Client::AddPacketBody(char* data, uint16_t length)
{
	if (length > 0 && data != nullptr)
	{
		memcpy(m_bufferHead, data, length);
		m_bufferHead += length;
	}
}

void Client::SendAcknowledge(uint8_t id, int command)
{
	*(uint8_t*)m_bufferHead = (uint8_t)4;
	m_bufferHead++;
	*(uint8_t*)m_bufferHead = id;
	m_bufferHead++;
	*(uint8_t*)m_bufferHead = (uint8_t)command;
	m_bufferHead += 2;

	Acknowledge(id);
}

void Client::Acknowledge(uint8_t id)
{
	if (id > m_clientPacketCounter)
	{
		for (int i = 0; i < (id + 256 - m_clientPacketCounter + 20); i++)
		{
			m_clientPacketStatus[i % 256] = 0;
		}
		m_clientPacketCounter = id;
	}
	else if (abs(id - m_clientPacketCounter) > 200)
	{

		for (int i = 0; i < (id - m_clientPacketCounter + 20); i++)
		{
			m_clientPacketStatus[i % 256] = 0;
		}
		m_clientPacketCounter = id;
	}
}

void Client::TrackGuaranteedMessage(byte command, uint8_t id, char* data, int length)
{
	//IoManager::Instance().Write(L"Started tracking packet %u, %u", m_id, id);
	//check if key already exists - if so, free it so it can be overwritten
	ConfirmGuaranteedMessage(id);

	//Set up data for tracking packet state
	GuaranteedMessage msg;
	if (length > 0 && data != nullptr)
	{
		msg.data = (char*)malloc(length);
		memcpy(msg.data, data, length);
	}
	else
	{
		msg.data = nullptr;
	}
	msg.command = command;
	msg.length = length;
	msg.packetID = id;
	time(&msg.timestamp);
	m_outPackets[id] = msg;
}

void Client::ConfirmGuaranteedMessage(uint8_t id)
{
	//check if message exists
	std::map<uint8_t, GuaranteedMessage>::iterator iter = m_outPackets.find(id);
	if (iter != m_outPackets.end())
	{
		// Remove message from tracking.
		//IoManager::Instance().Write(L"Confirmed packet %u %u", m_id, id);
		if (m_outPackets[id].data != nullptr)
		{
			// Free data buffer.
			free(m_outPackets[id].data);
		}
		m_outPackets.erase(iter);
	}
}

bool Client::AlreadyReceivedPacket(uint8_t id)
{
	return m_clientPacketStatus[id];
}

void Client::LogWaitingMessages()
{
	for (auto packet : m_outPackets)
		IoManager::Instance().Write(L"\tUnacknowledged: Command %i, PID %i", packet.second.command, packet.second.packetID);
}

void Client::SetLastProcessedPacketId(uint8_t id)
{
	m_lastProcessedPacketId = id;
}

void Client::CheckResendPackets()
{
	time_t currentTime;
	time(&currentTime);
	for (auto& m : m_outPackets)
	{
		auto packet = m.second;
		if (currentTime > packet.timestamp + PACKET_RESEND_DELAY)
		{
			AddGuaranteedPacket(packet.command, packet.packetID, packet.data, packet.length, true);
			m.second.timestamp = currentTime;
		}
	}
}

void Client::Ping()
{
	time(&m_timeoutCounter);
}

bool Client::CheckTimeout()
{
	time_t t;
	time(&t);
	if (t > m_timeoutCounter + TIMEOUT)
		return true;
	return false;
}
