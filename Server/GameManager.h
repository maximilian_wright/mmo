#pragma once
#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <unordered_map>
#include "Player.h"
#include "Enemy.h"
#include "ZoneManager.h"
#include "PartyManager.h"

class Server;

class GameManager
{
public:
	GameManager(Server* server, ZoneManager* zoneManager, PartyManager* partyManager);
	~GameManager();

	uint32_t GetNewEnemyId();

	void AddPlayer(uint32_t playerId, char* name, uint16_t nameLength);
	void RemovePlayer(uint32_t playerId);

	void AddEnemy(Enemy* enemy);
	void RemoveEnemy(Enemy* enemy);

	void HandlePlayerAction(uint32_t playerId, uint8_t actionId, char* data, uint16_t dataLength);
	void HandleEnemyAction(uint32_t enemyId, uint8_t actionId, char* data, uint16_t dataLength);
	void EnterZone(uint32_t playerId, char* data);
	void CreateParty(uint32_t playerId);
	void PartyInvite(uint32_t senderId, uint32_t recipientId);
	void JoinParty(uint32_t playerId, uint32_t partyId);
	void LeaveParty(uint32_t playerId);
	void MovePlayer(char* data, uint16_t length);
	void MoveEnemy(char* data, uint16_t length);
	void KillPlayer(uint32_t playerId);
	void KillEnemy(uint32_t enemyId);
	void Message(uint32_t playerId, char* data, uint16_t length);
	void Update();
	void LogPlayer(uint32_t playerId);
	void LogEnemy(uint32_t enemyId);
	void LogEnemies();

private:
	std::unordered_map<uint32_t, Player*> m_players;
	std::unordered_map<uint32_t, Enemy*> m_enemies;

	Server* m_server;
	ZoneManager* m_zoneManager;
	PartyManager* m_partyManager;

	uint32_t m_enemyIdCounter;
};

#endif
