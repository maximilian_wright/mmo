#include "PartyManager.h"
#include "Server.h"
#include "Commands.h"

PartyManager::PartyManager()
{
	m_counter = 1;
}

PartyManager::PartyManager(Server* server, ZoneManager* zm) : PartyManager()
{
	m_server = server;
	m_zoneManager = zm;
}

PartyManager::~PartyManager()
{

}

uint32_t PartyManager::AddParty()
{
	uint32_t id = m_counter++;
	AddParty(id);
	return id;
}

void PartyManager::AddParty(uint32_t id)
{
	if (m_parties.count(id) == 0)
	{
		Party* party = new Party(id);
		m_parties.insert(std::pair<uint32_t, Party*>(id, party));
	}
}

void PartyManager::RemoveParty(uint32_t id)
{
	if (m_parties.count(id) != 0)
	{
		for (int i = m_parties[id]->MemberCount(); i > 0; i--)
		{
			RemoveMember(m_parties[id]->m_members[i-1]);
		}
		m_parties.erase(id);
		if (m_zoneManager->HasInstance(id))
		{
			m_zoneManager->RemoveInstance(id);
		}
	}
}

void PartyManager::AddMember(uint32_t id, Player* player)
{
	if (m_parties.count(id) == 1)
	{
		//remove from current group
		RemoveMember(player);
		m_parties[id]->AddMember(player);

		uint8_t buffer[68];
		*(uint32_t*)buffer = player->GetId();
		const char* name = player->GetName();
		int nameLength = strlen(name) + 1;
		memcpy(buffer + 4, name, nameLength);

		BroadcastParty(player, Commands::JOIN_PARTY, (char*)&buffer, 4 + nameLength, true);
		for (auto member : m_parties[id]->m_members)
		{
			uint8_t buffer[68];
			*(uint32_t*)buffer = member->GetId();
			const char* name = member->GetName();
			int nameLength = strlen(name) + 1;
			memcpy(buffer + 4, name, nameLength);
			m_server->PushGuaranteedPacket(player->GetId(), Commands::JOIN_PARTY, -1, (char*)&buffer, 4 + nameLength);
		}
	}
}

void PartyManager::RemoveMember(Player* player)
{
	uint32_t partyID = player->GetPartyId();
	uint32_t playerId = player->GetId();
	if (partyID != 0)
	{
		if (m_parties.count(partyID) != 0)
		{
			BroadcastParty(player, Commands::LEAVE_PARTY, (char*)&playerId, 4, true);
			m_parties[partyID]->RemoveMember(player);
			if (m_parties[partyID]->MemberCount() == 0)
				RemoveParty(partyID);

		}
	}

}

void PartyManager::BroadcastParty(Player* player, uint8_t command, char* data, uint16_t length, bool guaranteed)
{
	if (m_parties.count(player->GetPartyId()) > 0)
	{
		Party* party = m_parties[player->GetPartyId()];
		for (auto& other : party->m_members)
		{
			if (player != other) {
				if (guaranteed)
					m_server->PushGuaranteedPacket(other->GetId(), command, -1, data, length);
				else
					m_server->PushPacket(other->GetId(), command, data, length);
			}
		}
	}
}

void PartyManager::LogParty(uint32_t id)
{
	if (m_parties.count(id) > 0)
	{
		Party* party = m_parties[id];
		IoManager::Instance().Write(L"Party %i: %i players", id, party->m_members.size());
		for (auto player : party->m_members)
		{
			IoManager::Instance().Write(L"\tPlayer %i", player->GetId());
		}
	}
	else
		IoManager::Instance().Write(L"Party %i not found", id);
}

void PartyManager::LogParties()
{
	if (m_parties.size() == 0)
		IoManager::Instance().Write(L"No parties.");
	else
	{
		for (auto party : m_parties)
		{
			LogParty(party.second->m_guid);
		}
	}
}