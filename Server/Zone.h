#pragma once

#include <vector>
#include <unordered_map>
#include "Player.h"
#include "Enemy.h"

class Zone
{
public:
	Zone(uint16_t id);
	~Zone();

	void AddMember(Player* player);
	void RemoveMember(Player* player);
	int MemberCount();

	void AddEnemy(Enemy* enemy);
	void RemoveEnemy(Enemy* enemy);

	std::vector<Player*> m_members;
	std::unordered_map<Enemy*, Player*> m_enemyToSimPlayerMap;
	uint16_t m_id;
};

