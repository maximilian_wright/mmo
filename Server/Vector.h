#pragma once
#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

//  Defined constant for when numbers are too small to be used in the
//    denominator of a division operation.  This is only used if the
//    _DEBUG macro is defined.
const float DIVIDE_BY_ZERO_TOLERANCE = float(1.0e-07);

// 2D vector
struct Vector2
{
	float  x;
	float  y;

	//
	//  --- Constructors and Destructors ---
	//

	Vector2(float s = float(0.0)) :
		x(s), y(s) {}

	Vector2(float x, float y) :
		x(x), y(y) {}

	Vector2(const Vector2& v)
	{
		x = v.x;  y = v.y;
	}

	//
	//  --- Indexing Operator ---
	//

	float& operator [] (int i) { return *(&x + i); }
	const float operator [] (int i) const { return *(&x + i); }

	//
	//  --- (non-modifying) Arithematic Operators ---
	//

	Vector2 operator - () const // unary minus operator
	{
		return Vector2(-x, -y);
	}

	Vector2 operator + (const Vector2& v) const
	{
		return Vector2(x + v.x, y + v.y);
	}

	Vector2 operator - (const Vector2& v) const
	{
		return Vector2(x - v.x, y - v.y);
	}

	Vector2 operator * (const float s) const
	{
		return Vector2(s*x, s*y);
	}

	Vector2 operator * (const Vector2& v) const
	{
		return Vector2(x*v.x, y*v.y);
	}

	friend Vector2 operator * (const float s, const Vector2& v)
	{
		return v * s;
	}

	Vector2 operator / (const float s) const {
#ifdef _DEBUG
		if (std::fabs(s) < DIVIDE_BY_ZERO_TOLERANCE) {
			std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
				<< "Division by zero" << std::endl;
			return Vector2();
		}
#endif // _DEBUG

		float r = float(1.0) / s;
		return *this * r;
	}

	//
	//  --- (modifying) Arithematic Operators ---
	//

	Vector2& operator += (const Vector2& v)
	{
		x += v.x;  y += v.y;   return *this;
	}

	Vector2& operator -= (const Vector2& v)
	{
		x -= v.x;  y -= v.y;  return *this;
	}

	Vector2& operator *= (const float s)
	{
		x *= s;  y *= s;   return *this;
	}

	Vector2& operator *= (const Vector2& v)
	{
		x *= v.x;  y *= v.y; return *this;
	}

	Vector2& operator /= (const float s) {
#ifdef _DEBUG
		if (std::fabs(s) < DIVIDE_BY_ZERO_TOLERANCE) {
			std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
				<< "Division by zero" << std::endl;
		}
#endif // _DEBUG

		float r = float(1.0) / s;
		*this *= r;

		return *this;
	}

	//
	//  --- Insertion and Extraction Operators ---
	//

	friend std::ostream& operator << (std::ostream& os, const Vector2& v) {
		return os << "( " << v.x << ", " << v.y << " )";
	}

	friend std::istream& operator >> (std::istream& is, Vector2& v)
	{
		return is >> v.x >> v.y;
	}

	//
	//  --- Conversion Operators ---
	//

	operator const float* () const
	{
		return static_cast<const float*>(&x);
	}

	operator float* ()
	{
		return static_cast<float*>(&x);
	}
};

// Non-class Vector2 Methods

inline
float dot(const Vector2& u, const Vector2& v) {
	return u.x * v.x + u.y * v.y;
}

inline
float length(const Vector2& v) {
	return std::sqrt(dot(v, v));
}

inline
Vector2 normalize(const Vector2& v) {
	float len = length(v);
	if (std::fabs(len) < DIVIDE_BY_ZERO_TOLERANCE)
		return Vector2();
	return v / len;
}


// 3D vector
struct Vector3
{
	float  x;
	float  y;
	float  z;

	//
	//  --- Constructors and Destructors ---
	//

	Vector3(float s = float(0.0)) :
		x(s), y(s), z(s) {}

	Vector3(float x, float y, float z) :
		x(x), y(y), z(z) {}

	Vector3(const Vector3& v) { x = v.x;  y = v.y;  z = v.z; }

	Vector3(const Vector2& v, const float f) { x = v.x;  y = v.y;  z = f; }

	//
	//  --- Indexing Operator ---
	//

	float& operator [] (int i) { return *(&x + i); }
	const float operator [] (int i) const { return *(&x + i); }

	//
	//  --- (non-modifying) Arithematic Operators ---
	//

	Vector3 operator - () const  // unary minus operator
	{
		return Vector3(-x, -y, -z);
	}

	Vector3 operator + (const Vector3& v) const
	{
		return Vector3(x + v.x, y + v.y, z + v.z);
	}

	Vector3 operator - (const Vector3& v) const
	{
		return Vector3(x - v.x, y - v.y, z - v.z);
	}

	Vector3 operator * (const float s) const
	{
		return Vector3(s*x, s*y, s*z);
	}

	Vector3 operator * (const Vector3& v) const
	{
		return Vector3(x*v.x, y*v.y, z*v.z);
	}

	friend Vector3 operator * (const float s, const Vector3& v)
	{
		return v * s;
	}

	Vector3 operator / (const float s) const {
#ifdef _DEBUG
		if (std::fabs(s) < DIVIDE_BY_ZERO_TOLERANCE) {
			std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
				<< "Division by zero" << std::endl;
			return Vector3();
		}
#endif // _DEBUG

		float r = float(1.0) / s;
		return *this * r;
	}

	//
	//  --- (modifying) Arithematic Operators ---
	//

	Vector3& operator += (const Vector3& v)
	{
		x += v.x;  y += v.y;  z += v.z;  return *this;
	}

	Vector3& operator -= (const Vector3& v)
	{
		x -= v.x;  y -= v.y;  z -= v.z;  return *this;
	}

	Vector3& operator *= (const float s)
	{
		x *= s;  y *= s;  z *= s;  return *this;
	}

	Vector3& operator *= (const Vector3& v)
	{
		x *= v.x;  y *= v.y;  z *= v.z;  return *this;
	}

	Vector3& operator /= (const float s) {
#ifdef _DEBUG
		if (std::fabs(s) < DIVIDE_BY_ZERO_TOLERANCE) {
			std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
				<< "Division by zero" << std::endl;
		}
#endif // _DEBUG

		float r = float(1.0) / s;
		*this *= r;

		return *this;
	}

	//
	//  --- Insertion and Extraction Operators ---
	//

	friend std::ostream& operator << (std::ostream& os, const Vector3& v) {
		return os << "( " << v.x << ", " << v.y << ", " << v.z << " )";
	}

	friend std::istream& operator >> (std::istream& is, Vector3& v)
	{
		return is >> v.x >> v.y >> v.z;
	}

	//
	//  --- Conversion Operators ---
	//

	operator const float* () const
	{
		return static_cast<const float*>(&x);
	}

	operator float* ()
	{
		return static_cast<float*>(&x);
	}
};

// Non-class Vector3 Methods

inline
float dot(const Vector3& u, const Vector3& v) {
	return u.x*v.x + u.y*v.y + u.z*v.z;
}

inline
float length(const Vector3& v) {
	return std::sqrt(dot(v, v));
}

inline
float squaredLength(const Vector3& v) {
	return dot(v, v);
}

inline
Vector3 normalize(const Vector3& v) {
	float len = length(v);
	if (std::fabs(len) < DIVIDE_BY_ZERO_TOLERANCE)
		return Vector3();
	return v / len;
}

inline
Vector3 cross(const Vector3& a, const Vector3& b)
{
	return Vector3(a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}


// 4D vector
struct Vector4
{
	float  x;
	float  y;
	float  z;
	float  w;

	//
	//  --- Constructors and Destructors ---
	//

	Vector4(float s = float(0.0)) :
		x(s), y(s), z(s), w(s) {}

	Vector4(float x, float y, float z, float w) :
		x(x), y(y), z(z), w(w) {}

	Vector4(const Vector4& v) { x = v.x;  y = v.y;  z = v.z;  w = v.w; }

	Vector4(const Vector3& v, const float w = 1.0) : w(w)
	{
		x = v.x;  y = v.y;  z = v.z;
	}

	Vector4(const Vector2& v, const float z, const float w) : z(z), w(w)
	{
		x = v.x;  y = v.y;
	}

	//
	//  --- Indexing Operator ---
	//

	float& operator [] (int i) { return *(&x + i); }
	const float operator [] (int i) const { return *(&x + i); }

	//
	//  --- (non-modifying) Arithematic Operators ---
	//

	Vector4 operator - () const  // unary minus operator
	{
		return Vector4(-x, -y, -z, -w);
	}

	Vector4 operator + (const Vector4& v) const
	{
		return Vector4(x + v.x, y + v.y, z + v.z, w + v.w);
	}

	Vector4 operator - (const Vector4& v) const
	{
		return Vector4(x - v.x, y - v.y, z - v.z, w - v.w);
	}

	Vector4 operator * (const float s) const
	{
		return Vector4(s*x, s*y, s*z, s*w);
	}

	Vector4 operator * (const Vector4& v) const
	{
		return Vector4(x*v.x, y*v.y, z*v.z, w*v.z);
	}

	friend Vector4 operator * (const float s, const Vector4& v)
	{
		return v * s;
	}

	Vector4 operator / (const float s) const {
#ifdef _DEBUG
		if (std::fabs(s) < DIVIDE_BY_ZERO_TOLERANCE) {
			std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
				<< "Division by zero" << std::endl;
			return Vector4();
		}
#endif // _DEBUG

		float r = float(1.0) / s;
		return *this * r;
	}

	//
	//  --- (modifying) Arithematic Operators ---
	//

	Vector4& operator += (const Vector4& v)
	{
		x += v.x;  y += v.y;  z += v.z;  w += v.w;  return *this;
	}

	Vector4& operator -= (const Vector4& v)
	{
		x -= v.x;  y -= v.y;  z -= v.z;  w -= v.w;  return *this;
	}

	Vector4& operator *= (const float s)
	{
		x *= s;  y *= s;  z *= s;  w *= s;  return *this;
	}

	Vector4& operator *= (const Vector4& v)
	{
		x *= v.x, y *= v.y, z *= v.z, w *= v.w;  return *this;
	}

	Vector4& operator /= (const float s) {
#ifdef _DEBUG
		if (std::fabs(s) < DIVIDE_BY_ZERO_TOLERANCE) {
			std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
				<< "Division by zero" << std::endl;
		}
#endif // _DEBUG

		float r = float(1.0) / s;
		*this *= r;

		return *this;
	}

	//
	//  --- Insertion and Extraction Operators ---
	//

	friend std::ostream& operator << (std::ostream& os, const Vector4& v) {
		return os << "( " << v.x << ", " << v.y
			<< ", " << v.z << ", " << v.w << " )";
	}

	friend std::istream& operator >> (std::istream& is, Vector4& v)
	{
		return is >> v.x >> v.y >> v.z >> v.w;
	}

	//
	//  --- Conversion Operators ---
	//

	operator const float* () const
	{
		return static_cast<const float*>(&x);
	}

	operator float* ()
	{
		return static_cast<float*>(&x);
	}
};

// Non-class Vector4 Methods

inline
float dot(const Vector4& u, const Vector4& v) {
	return u.x*v.x + u.y*v.y + u.z*v.z + u.w + v.w;
}

inline
float length(const Vector4& v) {
	return std::sqrt(dot(v, v));
}

inline
Vector4 normalize(const Vector4& v) {
	float len = length(v);
	if (std::fabs(len) < DIVIDE_BY_ZERO_TOLERANCE)
		return Vector4();
	return v / len;
}

inline
Vector3 cross(const Vector4& a, const Vector4& b)
{
	return Vector3(a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

#endif
