#include "IoManager.h"

IoManager::IoManager()
	: m_inputCommandBufferLength(0),
	m_hStdin(GetStdHandle(STD_INPUT_HANDLE)),
	m_hStdout(GetStdHandle(STD_OUTPUT_HANDLE)),
	m_verbosity((Verbosity)2)
{
}

IoManager::~IoManager()
{
}

bool IoManager::ReadInputCommand(WCHAR* inputCommandBuffer, UINT inputCommandBufferLength)
{
	DWORD numUnreadInputEvents;
	if (!GetNumberOfConsoleInputEvents(m_hStdin, &numUnreadInputEvents))
	{
		return false;
	}

	// Get input.
	for (unsigned int i = 0; i < numUnreadInputEvents; ++i)
	{
		INPUT_RECORD inputEventBuffer;
		DWORD numInputEventsRead;
		if (!ReadConsoleInput(m_hStdin, &inputEventBuffer, 1, &numInputEventsRead))
		{
			return false;
		}

		if (inputEventBuffer.EventType == KEY_EVENT)
		{
			KEY_EVENT_RECORD keyEvent = inputEventBuffer.Event.KeyEvent;

			if (keyEvent.bKeyDown)
			{
				WCHAR key = keyEvent.wVirtualKeyCode;

				if (key >= L'A' && key <= L'Z')
				{
					if (m_inputCommandBufferLength < min(512, inputCommandBufferLength))
					{
						DWORD controlKeyState = keyEvent.dwControlKeyState;
						DWORD letterCaseMask = CAPSLOCK_ON | SHIFT_PRESSED;
						if ((controlKeyState & letterCaseMask) == 0 ||
							(controlKeyState & letterCaseMask) == letterCaseMask)
						{
							// Make the letter lowercase.
							key += 32;
						}

						DWORD numCharsWritten;
						if (!WriteConsole(m_hStdout, &key, 1, &numCharsWritten, NULL))
						{
							return false;
						}

						m_inputCommandBuffer[m_inputCommandBufferLength++] = key;
					}
				}
				else if (key >= L'0' && key <= L'9' ||
					key == VK_SPACE)
				{
					if (m_inputCommandBufferLength < min(512, inputCommandBufferLength))
					{
						DWORD numCharsWritten;
						if (!WriteConsole(m_hStdout, &key, 1, &numCharsWritten, NULL))
						{
							return false;
						}

						m_inputCommandBuffer[m_inputCommandBufferLength++] = key;
					}
				}
				else if (key == VK_RETURN)
				{
					// Write the Next Line (\n) character and input prefix (>) instead
					// of the key's Carriage Return (\r) character to the output buffer.
					WCHAR newInputLine[] = L"\n>";
					DWORD numCharsWritten;
					if (!WriteConsole(m_hStdout, newInputLine, 2, &numCharsWritten, NULL))
					{
						return false;
					}

					if (m_inputCommandBufferLength < min(512, inputCommandBufferLength))
					{
						m_inputCommandBuffer[m_inputCommandBufferLength++] = L'\0';
						wcscpy_s(inputCommandBuffer, m_inputCommandBufferLength, m_inputCommandBuffer);
						m_inputCommandBufferLength = 0;
						return true;
					}
					else
					{
						m_inputCommandBufferLength = 0;
						return false;
					}
				}
			}
		}
	}

	return false;
}

void IoManager::Write(const WCHAR* format, ...) const
{
	// Combines passed in output (max 512), new input line (2), and current input command (max 512).
	WCHAR outputBuffer[1026];

	va_list args;
	va_start(args, format);
	int numOutputCharsWritten = vswprintf(outputBuffer, 512, format, args);
	va_end(args);
	if (numOutputCharsWritten <= 0)
	{
		return;
	}

	wcsncat_s(outputBuffer, 1026, L"\n>", 2);
	wcsncat_s(outputBuffer, 1026, m_inputCommandBuffer, m_inputCommandBufferLength);

	DWORD outputBufferLength = numOutputCharsWritten + 2 + m_inputCommandBufferLength;

	CONSOLE_SCREEN_BUFFER_INFO consoleScreenBufferInfo;
	if (!GetConsoleScreenBufferInfo(m_hStdout, &consoleScreenBufferInfo))
	{
		return;
	}

	COORD inputStartPosition =
	{
		0,
		consoleScreenBufferInfo.dwCursorPosition.Y - ((SHORT)m_inputCommandBufferLength + 1) / consoleScreenBufferInfo.dwSize.X
	};

	if (!SetConsoleCursorPosition(m_hStdout, inputStartPosition))
	{
		return;
	}

	WCHAR clearInputBuffer[513];
	DWORD clearInputBufferLength = m_inputCommandBufferLength + 1;
	wmemset(clearInputBuffer, L'\0', clearInputBufferLength);
	DWORD numCharsWritten;
	if (!WriteConsole(m_hStdout, clearInputBuffer, clearInputBufferLength, &numCharsWritten, NULL))
	{
		return;
	}

	if (!SetConsoleCursorPosition(m_hStdout, inputStartPosition))
	{
		return;
	}

	if (!WriteConsole(m_hStdout, outputBuffer, outputBufferLength, &numCharsWritten, NULL))
	{
		return;
	}
}

void IoManager::Write(Verbosity verbosity, const WCHAR* format, ...) const
{
	if (verbosity > m_verbosity)
		return;
	// Combines passed in output (max 512), new input line (2), and current input command (max 512).
	WCHAR outputBuffer[1026];

	va_list args;
	va_start(args, format);
	int numOutputCharsWritten = vswprintf(outputBuffer, 512, format, args);
	va_end(args);
	if (numOutputCharsWritten <= 0)
	{
		return;
	}

	wcsncat_s(outputBuffer, 1026, L"\n>", 2);
	wcsncat_s(outputBuffer, 1026, m_inputCommandBuffer, m_inputCommandBufferLength);

	DWORD outputBufferLength = numOutputCharsWritten + 2 + m_inputCommandBufferLength;

	CONSOLE_SCREEN_BUFFER_INFO consoleScreenBufferInfo;
	if (!GetConsoleScreenBufferInfo(m_hStdout, &consoleScreenBufferInfo))
	{
		return;
	}

	COORD inputStartPosition =
	{
		0,
		consoleScreenBufferInfo.dwCursorPosition.Y - ((SHORT)m_inputCommandBufferLength + 1) / consoleScreenBufferInfo.dwSize.X
	};

	if (!SetConsoleCursorPosition(m_hStdout, inputStartPosition))
	{
		return;
	}

	WCHAR clearInputBuffer[513];
	DWORD clearInputBufferLength = m_inputCommandBufferLength + 1;
	wmemset(clearInputBuffer, L'\0', clearInputBufferLength);
	DWORD numCharsWritten;
	if (!WriteConsole(m_hStdout, clearInputBuffer, clearInputBufferLength, &numCharsWritten, NULL))
	{
		return;
	}

	if (!SetConsoleCursorPosition(m_hStdout, inputStartPosition))
	{
		return;
	}

	if (!WriteConsole(m_hStdout, outputBuffer, outputBufferLength, &numCharsWritten, NULL))
	{
		return;
	}
}

void IoManager::SetVerbosity(Verbosity verbosity)
{
	m_verbosity = verbosity;
}