#pragma once
#include <string>
#include <vector>
#include <map>
#include <chrono>
#include <winsock2.h>
#include "IoManager.h"
#include "commands.h"

struct GuaranteedMessage 
{
	char command;
	uint8_t packetID;
	char* data;
	int length;
	time_t timestamp;
};

class Client
{
public:
	Client();
	Client(sockaddr_in connInfo);
	~Client();
	int GetBufferLength();
	void ClearSendData();
	uint8_t PacketID();
	void AddPacket(byte command, char* data, uint16_t length);
	void AddGuaranteedPacket(byte command, uint8_t packetId, char* data, uint16_t length, bool isResend);
    void StoreIncomingPacket(uint8_t command, uint8_t packetId, char* data, short length);
    GuaranteedMessage& GetNextStoredPacket();
	void FreeStoredPacket(uint8_t packetId);
	bool IsNextPacketId(uint8_t packetId);
	bool HasNextPacketStored();
	void CheckResendPackets();
	void Ping();
	bool CheckTimeout();
	void ConfirmGuaranteedMessage(uint8_t id);
	void SendAcknowledge(uint8_t id, int command);
	void Acknowledge(uint8_t id);
	bool AlreadyReceivedPacket(uint8_t id);
	void LogWaitingMessages();
	void SetLastProcessedPacketId(uint8_t id);

	sockaddr_in m_connectionInfo;
	char m_buffer[1300];
	char* m_bufferHead;
	
	uint32_t m_id;
	uint8_t m_packetCounter;
	uint8_t m_clientPacketCounter;
	bool m_clientPacketStatus[256];

private:
	std::map<uint8_t, GuaranteedMessage> m_inPackets;
	std::map<uint8_t, GuaranteedMessage> m_outPackets;
	void AddPacketHeader(byte command, uint8_t packetID, uint16_t length);
	void AddPacketBody(char* data, uint16_t length);
	void TrackGuaranteedMessage(byte command, uint8_t id, char* data, int length);
	

	uint8_t m_lastProcessedPacketId;
	time_t m_timeoutCounter;

};