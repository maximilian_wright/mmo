#pragma once

#include <stdio.h>
#include <winsock2.h>
#include <WS2tcpip.h>
#include <thread>
#include <map>
#include <unordered_map>
#include <vector>
#include "IoManager.h"
#include "Client.h"
#include "Party.h"
#include "ZoneManager.h"
#include "PartyManager.h"
#include "GameManager.h"
#include "commands.h"

#pragma comment(lib,"ws2_32.lib") //Winsock Library

#define PORT 8888   //The port on which to listen for incoming data

struct MOVE_STRUCT
{
	float positionX;
	float positionY;
	float positionZ;
	float rotationY;
	float velocityMag;
};

struct TRACK_PLAYER_STRUCT 
{
	uint32_t id;
	uint32_t partyId;
	char name[64]; // max 64 characters, but only name length will be added to buffer
	MOVE_STRUCT moveStruct;
	uint8_t playerDataBuffer[35];
};

struct MESSAGE_STRUCT
{
	uint32_t id;
	char buffer[1300];
};

class Server
{
public:
	Server();
	~Server();

	int Init();
	int Start();
	void Shutdown();

	void PushPacket(uint32_t playerId, byte command, char* data, uint16_t length);
	void PushGuaranteedPacket(uint32_t playerId, byte command, int16_t packetID, char* data, uint16_t length);
	void BroadcastAll(byte command, char* data, uint16_t length, bool guaranteed);

private:
	uint64_t HashAddress(sockaddr_in data);
	int Receive();
	void ProcessPacket(Client* client, char* buffer);
	void ProcessPacket(Client* client, uint8_t command, uint8_t packetId, char* data, int length);
	void DisconnectClient(Client* client);
	bool ShouldProcessPacket(Client* client, uint8_t packetId, uint8_t command, char* data, int length);

	bool m_isExiting;

	SOCKET m_socket;
	WSAData m_wsa;
	std::map<uint64_t, Client*> m_clientMap;
	std::map<uint32_t, Client*> m_idToClientMap;
	std::vector<Client*> m_disconnectedClients;
	std::map<Client*, uint8_t> m_serverDisconnectAckMap;

	ZoneManager* m_zoneManager;
	PartyManager* m_partyManager;
	GameManager* m_gameManager;

	int m_socketAddressSize;
	sockaddr_in m_inSocketData;
	char m_inBuffer[BUFLEN];
	uint32_t m_clientIDCounter;
};