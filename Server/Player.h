#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <set>
#include <time.h>
#include "Vector.h"

enum class PlayerData
{
	HEALTH = 1,
	STRENGTH = 2,
	STAMINA = 3,
	INTELLIGENCE = 4,
	DEXTERITY = 5, 
	BLOCKING = 6
};

class Player
{
public:
	Player(uint32_t id, uint32_t partyId, uint32_t zoneId, char* name, uint16_t nameLength, const Vector3& position, float rotationY, float velocityMag,
		int32_t health, int32_t strength, int32_t stamina, int32_t intelligence, int32_t dexterity);
	~Player();

	uint32_t GetId() const { return m_id; }

	uint32_t GetPartyId() const { return m_partyId; }
	void SetPartyId(uint32_t partyId) { m_partyId = partyId; }

	uint32_t GetZoneId() const { return m_zoneId; }
	void SetZoneId(uint32_t zoneId) { m_zoneId = zoneId; }

	const char* GetName() const { return m_name; }
	void SetName(char* name, uint16_t length);

	Vector3 GetPosition() const { return m_position; }
	void SetPosition(const Vector3& position) { m_position = position; }

	float GetRotationY() const { return m_rotationY; }
	void SetRotationY(float rotationY) { m_rotationY = rotationY; }

	float GetVelocityMag() const { return m_velocityMag; }
	void SetVelocityMag(float velocityMag) { m_velocityMag = velocityMag; }

	int32_t GetHealth() const { return m_health; }
	void SetHealth(int32_t health);

	int32_t GetMaxHealth() const { return m_stamina * 10; }

	int32_t GetStrength() const { return m_strength; }
	void SetStrength(int32_t strength);

	int32_t GetStamina() const { return m_stamina; }
	void SetStamina(int32_t stamina);

	int32_t GetIntelligence() const { return m_intelligence; }
	void SetIntelligence(int32_t intelligence);

	int32_t GetDexterity() const { return m_dexterity; }
	void SetDexterity(int32_t dexterity);

	const std::set<PlayerData>* GetDirtyData() const { return &m_dirtyData; }
	void ClearDirtyData() { m_dirtyData.clear(); }

	bool GetBlocking() const { return m_blocking; }
	void SetBlocking(bool blocking);

	void Update();

private:
	char m_name[64];
	std::set<PlayerData> m_dirtyData;
	Vector3 m_position;
	float m_rotationY;
	float m_velocityMag;
	uint32_t m_id;
	uint32_t m_partyId;
	uint32_t m_zoneId;
	int32_t m_health;
	int32_t m_strength;
	int32_t m_stamina;
	int32_t m_intelligence;
	int32_t m_dexterity;
	bool m_blocking;
	time_t m_blockTime;
};

#endif
