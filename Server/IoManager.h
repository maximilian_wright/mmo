#pragma once
#ifndef IO_MANAGER_H
#define IO_MANAGER_H

#include <Windows.h>
#include <wchar.h>

enum Verbosity {
	NONE = 0,
	BASIC = 1,
	DEBUG = 2,
	ALL = 3,
	LITERALLY_EVERYTHING = 4
};

// Singleton manager for handling all of the input and output on the server.
// Instance and resource destruction is automatic and guaranteed.
class IoManager
{
public:
	// Create/get an instance of the IoManager singleton.
	// Instance is instantiated on first use and guaranteed to be destroyed later.
	// This function is thread-safe.
	static IoManager& Instance()
	{
		static IoManager instance;
		return instance;
	}

	~IoManager();
	IoManager(const IoManager& other) = delete;
	void operator=(const IoManager& other) = delete;

	bool ReadInputCommand(WCHAR* inputCommandBuffer, UINT inputCommandBufferLength);
	void Write(const WCHAR* format, ...) const;
	void Write(Verbosity verbosity, const WCHAR* format, ...) const;
	void SetVerbosity(Verbosity verbosity);

private:
	WCHAR m_inputCommandBuffer[512];
	UINT m_inputCommandBufferLength;

	HANDLE m_hStdin;
	HANDLE m_hStdout;

	Verbosity m_verbosity;

	IoManager();
};

#endif
