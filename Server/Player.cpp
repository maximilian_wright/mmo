#include "Player.h"
#include <algorithm>

Player::Player(uint32_t id, uint32_t partyId, uint32_t zoneId, char* name, uint16_t nameLength, const Vector3& position, float rotationY, float velocityMag,
	int32_t health, int32_t strength, int32_t stamina, int32_t intelligence, int32_t dexterity)
	: m_id(id),
	m_partyId(partyId),
	m_zoneId(zoneId),
	m_position(position),
	m_rotationY(rotationY),
	m_velocityMag(velocityMag),
	m_health(health),
	m_strength(strength),
	m_stamina(stamina),
	m_intelligence(intelligence),
	m_dexterity(dexterity),
	m_blocking(false)
{
	strcpy_s(m_name, name);
}

Player::~Player()
{
}

void Player::SetName(char* name, uint16_t length)
{
	strncpy_s(m_name, name, length);
}

void Player::SetHealth(int32_t health)
{
	int newHealth = std::max(0, std::min(health, 100));
	if (newHealth != m_health)
	{
		m_health = newHealth;
		m_dirtyData.insert(PlayerData::HEALTH);
	}
}

void Player::SetStrength(int32_t strength)
{
	m_strength = strength;
	m_dirtyData.insert(PlayerData::STRENGTH);
}

void Player::SetStamina(int32_t stamina)
{
	m_stamina = stamina;
	m_dirtyData.insert(PlayerData::STAMINA);
}

void Player::SetIntelligence(int32_t intelligence)
{
	m_intelligence = intelligence;
	m_dirtyData.insert(PlayerData::INTELLIGENCE);
}

void Player::SetDexterity(int32_t dexterity)
{
	m_dexterity = dexterity;
	m_dirtyData.insert(PlayerData::DEXTERITY);
}

void Player::SetBlocking(bool blocking)
{
	m_blocking = blocking;
	if (blocking)
		time(&m_blockTime);
	else
		m_blockTime = 0;
	m_dirtyData.insert(PlayerData::BLOCKING);
}

void Player::Update()
{
	time_t t;
	time(&t);
	if (m_blocking && t - 5 > m_blockTime)
		SetBlocking(false);
}
