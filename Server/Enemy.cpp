#include "Enemy.h"
#include <algorithm>

Enemy::Enemy(uint32_t id, uint32_t zoneId, EnemyType type, const Vector3& position, float rotationY, float velocityMag, uint32_t targetId, int32_t health)
	: m_id(id),
	m_zoneId(zoneId),
	m_position(position),
	m_rotationY(rotationY),
	m_velocityMag(velocityMag),
	m_type(type),
	m_targetId(targetId),
	m_prevTargetId(targetId),
	m_health(health),
	m_blocking(false)
{
}

Enemy::~Enemy()
{
}

void Enemy::SetHealth(int32_t health)
{
	int newHealth = std::max(0, std::min(health, 100));
	if (newHealth != m_health)
	{
		m_health = newHealth;
		m_dirtyData.insert(EnemyData::HEALTH);
	}
}

void Enemy::SetBlocking(bool blocking)
{
	m_blocking = blocking;
	if (blocking)
		time(&m_blockTime);
	else
		m_blockTime = 0;
	m_dirtyData.insert(EnemyData::BLOCKING);
}

void Enemy::Update()
{
	time_t t;
	time(&t);
	if (m_blocking && t - 5 > m_blockTime)
		SetBlocking(false);
}
