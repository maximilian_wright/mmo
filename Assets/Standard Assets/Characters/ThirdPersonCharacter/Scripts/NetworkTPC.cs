﻿using UnityEngine;
using System.Collections;


namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(ThirdPersonCharacter))]

    public class NetworkTPC : MonoBehaviour
    {

        private ThirdPersonCharacter m_Character;


        // Use this for initialization
        void Start()
        {
            m_Character = GetComponent<ThirdPersonCharacter>();

            Collider[] playerColliders = GameObject.FindGameObjectWithTag("Player").GetComponents<Collider>();
            foreach(Collider col in playerColliders)
            {
                Physics.IgnoreCollision(col, GetComponent<Collider>());
            }
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        void UpdateCharacterPosition(float x, float y, float z)
        {
            m_Character.transform.position = new Vector3(x, y, z);
        }
    }
}
