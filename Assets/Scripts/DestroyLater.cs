﻿using UnityEngine;
using System.Collections;

public class DestroyLater : MonoBehaviour
{
    public float later = 2f;
    private float timer = 0;

	void Update()
    {
        timer += Time.deltaTime;

        if (timer > later)
            Destroy(gameObject);
	}
}
