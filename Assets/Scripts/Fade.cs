﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    public const float FADE_SPEED = 0.33f;

    private RawImage image;
    private bool running = false;

    private bool fadeIn = false;
    private bool fadeOut = false;
    private bool done = false;

    private Color color;
    private float alpha = 0;

    public bool Done
    {
        get { return done; }
    }

    public bool Running
    {
        get { return running; }
    }

    void Awake()
    {
        color = new Color(1f, 1f, 1f, alpha);

        image = GetComponent<RawImage>();
        image.color = color;
    }
    
    void Update()
    {
        if (fadeIn)
        {
            alpha += FADE_SPEED * Time.deltaTime;
            alpha = Mathf.Clamp(alpha, 0f, 1f);

            color.a = alpha;
            image.color = color;

            if (alpha == 1f)
            {
                fadeIn = false;
                done = true;
                running = false;
            }
        }
        else if (fadeOut)
        {
            alpha -= FADE_SPEED * Time.deltaTime;
            alpha = Mathf.Clamp(alpha, 0f, 1f);

            color.a = alpha;
            image.color = color;

            if (alpha == 0f)
            {
                fadeOut = false;
                done = true;
                running = false;
            }
        }
    }

    public void FadeIn()
    {
        fadeIn = true;
        done = false;
        running = true;
    }

    public void FadeOut()
    {
        fadeOut = true;
        done = false;
        running = true;
    }
}
