﻿using UnityEngine;
using Client;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public string ip = "129.21.119.28";
    private string username;
    private string[] names =
    {
        "Max",
        "Tim",
        "Trevor",
        "Justin",
        "Bob",
        "Evan",
        "Steve",
        "Craig",
        "Mike",
        "Ludo"
    };

    public GameObject mainMenuPanel;
    public GameObject loadingPanel;
    public GameObject optionsPanel;
    public InputField nameField;
    public InputField ipField;

    void Awake()
    {
        ipField.text = ip;
    }

    public void Login()
    {
        username = nameField.text;

        if (username.Length == 0)
            username = names[(int)(Random.value * names.Length)];

        ClientManager.Instance.Connect(ipField.text, 8888);
        ClientManager.Instance.Send(new PacketLogin(username + '\0'));
        FindObjectOfType<InstanceManager>().myName = username;
        loadingPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    public void Options()
    {
        optionsPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
