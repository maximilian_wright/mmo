﻿using UnityEngine;

public class SpawnStruct
{
    public ESpawnStructType Type;
    public int ID;
    public Vector3 Position;
    public Quaternion Rotation;

    public SpawnStruct(ESpawnStructType type, int id, Vector3 pos) : this(type, id, pos, Quaternion.identity) { }
    public SpawnStruct(ESpawnStructType type, int id, Vector3 pos, Quaternion rot)
    {
        Type = type;
        ID = id;
        Position = pos;
        Rotation = rot;
    }
}

public class PlayerStruct : SpawnStruct
{
    public string UserName;

    public PlayerStruct(ESpawnStructType type, int id, Vector3 pos, string name) : this(type, id, pos, Quaternion.identity, name) { }
    public PlayerStruct(ESpawnStructType type, int id, Vector3 pos, Quaternion rot, string name) : base(type, id, pos, rot)
    {
        UserName = name;
    }
}

public class EnemyStruct : SpawnStruct
{
    public int EnemyType;

    public EnemyStruct(ESpawnStructType type, int id, Vector3 pos, int etype) : this(type, id, pos, Quaternion.identity, etype) { }
    public EnemyStruct(ESpawnStructType type, int id, Vector3 pos, Quaternion rot, int etype) : base(type, id, pos, rot)
    {
        EnemyType = etype;
    }
}