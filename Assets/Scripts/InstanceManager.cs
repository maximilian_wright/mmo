﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Client;
using UnityEngine.UI;

public class InstanceManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject playerOtherPrefab;
    public GameObject cavewormPrefab;
    public GameObject spiderPrefab;
    public GameObject goblinPrefab;
    public GameObject dmgPrefab;
    public GameObject exitPrefab;
    public Texture2D imgDungeon;
    public Texture2D imgHub;

    private ClientManager cliManager;

    private int myID;
    public string myName;
    private PlayerScript player;
    private Dictionary<int, Transform> players;
    private HashSet<int> playerIDs;

    private Dictionary<int, Transform> enemies;
    private HashSet<int> enemyIDs;
    private List<int> controlledEnemies;

    public RawImage image;
    private Fade fade;
    public float fadeSpeed = 2f;

    public float clearTime = 4;
    private float clearTimer = 0;
    private bool clearing = false;
    public Texture2D[] clearTextures;

    public float wipeTime = 10;
    private float wipeTimer = 0;
    private bool wiping = false;
    public Texture2D[] wipeTextures;

    private TransitionMenu tm;
    private bool levelLoaded = false;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        cliManager = ClientManager.Instance;
        players = new Dictionary<int, Transform>();
        playerIDs = new HashSet<int>();
        enemies = new Dictionary<int, Transform>();
        enemyIDs = new HashSet<int>();
        controlledEnemies = new List<int>();

        InvokeRepeating("SendPing", 15, 15);
    }

    void OnApplicationQuit()
    {
        ClientManager.DestroyInstance();
    }

    void OnLevelWasLoaded(int level)
    {
        if (SceneManager.GetActiveScene().name == "menu")
        {
            Debug.Log("loaded level: " + level);
            return;
        }

        levelLoaded = true;

        while (cliManager.SpawnList.Count > 0)
        {
            SpawnStruct ss = cliManager.SpawnList[0];

            switch (ss.Type)
            {
                case ESpawnStructType.PLAYER:
                    if (!players.ContainsKey(ss.ID))
                    {
                        Debug.Log("Spawning myself at: " + ss.Position);

                        GameObject me = (GameObject)Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
                        me = me.transform.GetChild(0).gameObject;

                        if (GameObject.FindGameObjectWithTag("PlayerSpawn") != null)
                        {
                            Transform spawn = GameObject.FindGameObjectWithTag("PlayerSpawn").transform;
                            me.transform.position = spawn.position;
                            me.transform.rotation = spawn.rotation;
                        }
                        else
                        {
                            me = (GameObject)Instantiate(playerPrefab, ss.Position, ss.Rotation);
                        }

                        player = me.GetComponent<PlayerScript>();
                        player.Init(true, ss.ID, ss.Position, myName);
                        me.transform.Find("Name").GetComponent<TextMesh>().text = myName;

                        myID = ss.ID;
                        players.Add(ss.ID, me.transform);
                        playerIDs.Remove(ss.ID);

                        //remove struct
                        cliManager.SpawnList.RemoveAt(0);
                    }
                    break;
                case ESpawnStructType.PLAYER_OTHER:
                    if (!players.ContainsKey(ss.ID))
                    {
                        Debug.Log("spawning other player id: " + ss.ID + " | at: " + ss.Position);
                        PlayerStruct ps = (PlayerStruct)ss;
                        GameObject oplayer = (GameObject)Instantiate(playerOtherPrefab, ss.Position, ss.Rotation);

                        oplayer.GetComponentInChildren<PlayerScript>().Init(false, ss.ID, ss.Position, ps.UserName);
                        oplayer.transform.Find("Name").GetComponent<TextMesh>().text = ps.UserName;

                        players.Add(ss.ID, oplayer.transform);
                        playerIDs.Remove(ss.ID);

                        //remove struct
                        cliManager.SpawnList.RemoveAt(0);
                    }
                    break;
                case ESpawnStructType.MONSTER:
                    if (SceneManager.GetActiveScene().name != "RockDungeon") break;

                    if (!enemies.ContainsKey(ss.ID))
                    {
                        EnemyStruct es = (EnemyStruct)ss;
                        GameObject enemy = null;

                        switch (es.EnemyType)
                        {
                            case 1:
                                enemy = (GameObject)Instantiate(cavewormPrefab, ss.Position, ss.Rotation);
                                break;
                            case 2:
                                enemy = (GameObject)Instantiate(spiderPrefab, ss.Position, ss.Rotation);
                                break;
                            case 3:
                                enemy = (GameObject)Instantiate(goblinPrefab, ss.Position, ss.Rotation);
                                break;
                        }

                        enemy.GetComponent<EnemyScript>().Init(es.EnemyType, es.ID);
                        enemies.Add(ss.ID, enemy.transform);
                        enemyIDs.Add(ss.ID);

                        //remove struct
                        cliManager.SpawnList.RemoveAt(0);
                    }
                    break;
                default:
                    Debug.Log("Invalid ESpawnStructType");
                    //remove struct
                    cliManager.SpawnList.RemoveAt(0);
                    break;
            }
        }
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().name == "menu") return;

        if (cliManager.DisconnectedFromServer)
        {
            Logout();
            SceneManager.LoadScene("menu");
        }

        #region Zone Changing

        if (cliManager.ChangeZone != -1)
        {
            player.ZoneEnter(cliManager.ChangeZone);

            //remove all players in this zone
            List<int> ids = new List<int>();
            foreach (KeyValuePair<int, Transform> pair in players)
                if (pair.Key != myID)
                {
                    ids.Add(pair.Key);
                    playerIDs.Add(pair.Key);
                }

            foreach (int i in ids)
            {
                Debug.Log("zone change: destroying player " + i);
                Destroy(players[i].gameObject);
                players.Remove(i);
            }

            //remove all enemies in this zone
            foreach (KeyValuePair<int, Transform> pair in enemies)
                Destroy(pair.Value.gameObject);
            enemies.Clear();
            enemyIDs.Clear();
            controlledEnemies.Clear();

            if (cliManager.ChangeZone == 0)
            {
                player.GetComponent<Animator>().SetBool("Dead", false);
                tm = player.gameObject.AddComponent<TransitionMenu>();
                tm.LoadScene("LobbyTown", imgHub);
                levelLoaded = false;
            }
            else
            {
                tm = player.gameObject.AddComponent<TransitionMenu>();
                tm.LoadScene("RockDungeon", imgDungeon);
                levelLoaded = false;
            }

            cliManager.ChangeZone = -1;
        }

        #endregion

        #region Party Handling

        //handle creating a party
        if (cliManager.CreateParty != -1)
        {
            player.CreateParty(cliManager.CreateParty);
            cliManager.CreateParty = -1;
        }

        //handle getting kicked from party
        while (cliManager.RemovePartyMembers.Count > 0)
        {
            int playerid = cliManager.RemovePartyMembers[0];
            cliManager.RemovePartyMembers.RemoveAt(0);
            player.RemovePartyMember(playerid);

            if (playerid == myID && player.Zone != 0)
            {
                cliManager.Send(new PacketZoneEnter(0));
            }
        }

        //handle any party invites
        if (cliManager.InviteList.Count > 0)
        {
            InviteStruct inv = cliManager.InviteList[0];
            cliManager.InviteList.RemoveAt(0);

            Dialog d = player.gameObject.AddComponent<Dialog>();
            d.title = "Party Invite";
            d.message = inv.SenderName + " has invited you to join their party";
            d.buttons = new string[] { "Yes", "No" };
            d.OnClick += (option) =>
            {
                switch (option)
                {
                    case 0:
                        cliManager.Send(new PacketJoinParty(inv.PartyID));
                        player.JoinParty(inv.PartyID);
                        d.Close();
                        break;
                    default:
                        d.Close();
                        break;
                }
            };
        }

        //handle new party members joining
        while (cliManager.AddPartyMembers.Count > 0)
        {
            MemberStruct ms = cliManager.AddPartyMembers[0];
            cliManager.AddPartyMembers.RemoveAt(0);
            
            player.AddPartyMember(ms);
        }

        #endregion
        
        /*
         * Handles spawning game objects
         */
        if (cliManager.SpawnList.Count > 0 && (tm == null || tm.LoadFinished) && levelLoaded)
        {
            SpawnStruct ss = cliManager.SpawnList[0];

            switch (ss.Type)
            {
                case ESpawnStructType.PLAYER_OTHER:
                    if (!players.ContainsKey(ss.ID))
                    {
                        Debug.Log("spawning other player id: " + ss.ID + " | at: " + ss.Position);
                        PlayerStruct ps = (PlayerStruct)ss;
                        GameObject oplayer = (GameObject)Instantiate(playerOtherPrefab, ss.Position, ss.Rotation);
						
                        oplayer.GetComponentInChildren<PlayerScript>().Init(false, ss.ID, ss.Position, ps.UserName);
                        oplayer.transform.Find("Name").GetComponent<TextMesh>().text = ps.UserName;
						
                        players.Add(ss.ID, oplayer.transform);
                        playerIDs.Remove(ss.ID);

                        //remove struct
                        cliManager.SpawnList.RemoveAt(0);
                    }
                    break;
                case ESpawnStructType.MONSTER:
                    if (SceneManager.GetActiveScene().name != "RockDungeon") break;

                    if (!enemies.ContainsKey(ss.ID))
                    {
                        EnemyStruct es = (EnemyStruct)ss;
                        GameObject enemy = null;

                        switch (es.EnemyType)
                        {
                            case 1:
                                enemy = (GameObject)Instantiate(cavewormPrefab, ss.Position, ss.Rotation);
                                break;
                            case 2:
                                enemy = (GameObject)Instantiate(spiderPrefab, ss.Position, ss.Rotation);
                                break;
                            case 3:
                                enemy = (GameObject)Instantiate(goblinPrefab, ss.Position, ss.Rotation);
                                break;
                        }

                        enemy.GetComponent<EnemyScript>().Init(es.EnemyType, es.ID);
                        enemies.Add(ss.ID, enemy.transform);
                        enemyIDs.Add(ss.ID);

                        //remove struct
                        cliManager.SpawnList.RemoveAt(0);
                    }
                    break;
                default:
                    Debug.Log("Invalid ESpawnStructType");
                    //remove struct
                    cliManager.SpawnList.RemoveAt(0);
                    break;
            }
        }

        /*
         * Handles incoming damage value displaying
         */
        while (cliManager.DamageList.Count > 0)
        {
            DamageStruct ds = cliManager.DamageList[0];
            cliManager.DamageList.RemoveAt(0);
            GameObject g = null;

            switch (ds.Type)
            {
                case EIDType.PLAYER:
                    if (players.ContainsKey(ds.ID))
                        g = (GameObject)Instantiate(dmgPrefab, players[ds.ID].position + Vector3.up, Quaternion.identity);
                    break;
                case EIDType.MONSTER:
                    if (enemies.ContainsKey(ds.ID))
                        g = (GameObject)Instantiate(dmgPrefab, enemies[ds.ID].position + Vector3.up, Quaternion.identity);
                    break;
                case EIDType.ITEM:
                    Debug.Log("Add handling of item damage");
                    break;
                default:
                    Debug.Log("Unknown type of damagestruct: " + ds.Type.ToString());
                    break;
            }

            if (g != null)
                g.GetComponent<DamageText>().Create(ds);
        }

        /*
         * Handles destroying game objects
         */
        while (cliManager.DestroyList.Count > 0)
        {
            DestroyStruct ds = cliManager.DestroyList[0];
            cliManager.DestroyList.RemoveAt(0);

            switch (ds.Type)
            {
                case EDestroyStructType.PLAYER:
                    if (players.ContainsKey(ds.ID))
                    {
                        Destroy(players[ds.ID].gameObject);
                        players.Remove(ds.ID);
                        playerIDs.Add(ds.ID);
                    }
                    break;
                case EDestroyStructType.MONSTER:
                    if (enemies.ContainsKey(ds.ID))
                    {
                        EnemyScript es = enemies[ds.ID].GetComponent<EnemyScript>();

                        if (es.IsControlled)
                            controlledEnemies.Remove(ds.ID);

                        StartCoroutine(es.Die());
                        enemies.Remove(ds.ID);
                        enemyIDs.Add(ds.ID);
                    }
                    break;
                default:
                    Debug.Log("Cannot handle this type of destroystruct yet!");
                    break;
            }
        }

        /*
         * Handles dungeon clear/wipe
         */

        if (cliManager.DungeonClear)
        {
            if (!clearing)
            {
                clearing = true;
                image = FindObjectOfType<RawImage>();
                image.texture = clearTextures[Random.Range(0, clearTextures.Length)];
                fade = image.GetComponent<Fade>();

                Transform spawnPos = GameObject.FindGameObjectWithTag("DungeonExit").GetComponent<Transform>();
                Instantiate(exitPrefab, spawnPos.position, Quaternion.identity);
                GuiChat gc = player.gameObject.GetComponent<GuiChat>();
                gc.AddMessage("Success!");
                gc.AddMessage("The dungeon exit has spawned.");
            }

            clearTimer += Time.deltaTime;

            if (clearTimer >= clearTime)
            {
                if (!fade.Running && fade.Done)
                    fade.FadeOut();
            }
            else if (clearTimer >= clearTime / 3 * 2)
            {
                if (!fade.Running)
                    fade.FadeIn();
            }

            if (clearTimer > clearTime)
            {
                clearing = false;
                clearTimer = 0;
                cliManager.DungeonClear = false;
                cliManager.Send(new PacketZoneEnter(0));
            }
        }
        else if (cliManager.DungeonWipe)
        {
            if (!wiping)
            {
                wiping = true;
                image = FindObjectOfType<RawImage>();
                image.texture = wipeTextures[Random.Range(0, wipeTextures.Length)];
                fade = image.GetComponent<Fade>();

                GuiChat gc = player.gameObject.GetComponent<GuiChat>();
                gc.AddMessage("Failure!");
                gc.AddMessage("The enemies overwhelm you...");
            }

            wipeTimer += Time.deltaTime;

            if (wipeTimer >= wipeTime)
            {
                if (!fade.Running && fade.Done)
                    fade.FadeOut();
            }
            else if (wipeTimer >= wipeTime / 3 * 2)
            {
                if (!fade.Running)
                    fade.FadeIn();
            }

            if (wipeTimer >= wipeTime)
            {
                wiping = false;
                wipeTimer = 0;
                cliManager.DungeonWipe = false;
                cliManager.Send(new PacketZoneEnter(0));
            }
        }

        /*
         * Handles movement
         */
        for (int i = 0; i < cliManager.MoveList.Count; i++)
        {
            MoveStruct ms = cliManager.MoveList[0];

            switch (ms.Type)
            {
                case EMoveStructType.PLAYER:
                    if (players.ContainsKey(ms.ID))
                    {
                        Transform t = players[ms.ID];
                        t.rotation = ms.Rotation;

                        GameObject playerObj = t.gameObject;
                        playerObj.GetComponentInChildren<PlayerScript>().SetTargetPosition(ms.Position);

                        cliManager.MoveList.RemoveAt(i);
                        i--;
                    }
                    else if (playerIDs.Contains(ms.ID))
                    {
                        cliManager.MoveList.RemoveAt(i);
                        i--;
                    }
                    break;
                case EMoveStructType.MONSTER:
                    if (enemies.ContainsKey(ms.ID))
                    {
                        Transform t = enemies[ms.ID];
                        t.GetComponent<EnemyScript>().SetTargetMovePosition(ms.Position);
                        t.rotation = ms.Rotation;

                        cliManager.MoveList.RemoveAt(i);
                        i--;
                    }
                    else if (enemyIDs.Contains(ms.ID))
                    {
                        cliManager.MoveList.RemoveAt(i);
                        i--;
                    }
                    break;
                default:
                    Debug.Log("Unknown type of movestruct");
                    break;
            }
        }

        #region Enemy Simulation Tracking

        /*
         * Handles changing enemy contols
         */

        for (int i = 0; i < cliManager.StartSimList.Count; i++)
        {
            if (SceneManager.GetActiveScene().name != "RockDungeon") break;

            SimulateStruct ss = cliManager.StartSimList[i];

            //if enemy isn't spawned yet, skip
            if (!enemies.ContainsKey(ss.EnemyID)) continue;

            controlledEnemies.Add(ss.EnemyID);
            EnemyScript es = enemies[ss.EnemyID].GetComponent<EnemyScript>();
            es.SetControlled(true, ss.Position, ss.Rotation);

            if (ss.TargetID != 0)
                es.SetPlayerTarget(players[ss.TargetID]);

            cliManager.StartSimList.RemoveAt(i);
            i--;
        }

        for (int i = 0; i < cliManager.StopSimList.Count; i++)
        {
            if (SceneManager.GetActiveScene().name != "RockDungeon") break;

            int eid = cliManager.StopSimList[i];

            //if enemy isn't spawned yet, skip
            if (!enemies.ContainsKey(eid)) continue;

            controlledEnemies.Remove(eid);
            enemies[eid].GetComponent<EnemyScript>().SetControlled(false);
            cliManager.StopSimList.RemoveAt(i);
            i--;
        }

        #endregion

        for (int i = 0; i < cliManager.DeathList.Count; i++)
        {
            int id = cliManager.DeathList[i];

            if (players.ContainsKey(id))
            {
                players[id].GetComponent<PlayerScript>().Die();

                cliManager.DeathList.RemoveAt(i);
                i--;
            }
            else if (playerIDs.Contains(id))
            {
                cliManager.DeathList.RemoveAt(i);
                i--;
            }
        }

        /*
         * Handles updating
         */
        for (int i = 0; i < cliManager.UpdateList.Count; i++)
        {
            UpdateStruct us = cliManager.UpdateList[0];

            switch (us.Type)
            {
                case EUpdateStructType.PLAYER:
                    if (players.ContainsKey(us.ID))
                    {
                        PlayerScript p = players[us.ID].GetComponentInChildren<PlayerScript>();

                        foreach (KeyValuePair<EStat, int> pair in us.Stats)
                        {
                            //Debug.Log("Updating " + pair.Key.ToString());

                            switch (pair.Key)
                            {
                                case EStat.HEALTH:
                                    p.Player.Health = pair.Value;
                                    break;
                                case EStat.STRENGTH:
                                    p.Player.Strength = pair.Value;
                                    break;
                                case EStat.STAMINA:
                                    p.Player.Stamina = pair.Value;
                                    break;
                                case EStat.INTELLIGENCE:
                                    p.Player.Intelligence = pair.Value;
                                    break;
                                case EStat.DEXTERITY:
                                    p.Player.Dexterity = pair.Value;
                                    break;
                                case EStat.BLOCKING:
                                    p.SetBlocking(pair.Value);
                                    break;
                                default:
                                    Debug.Log("Unknown stat value: " + pair.Key);
                                    break;
                            }
                        }

                        cliManager.UpdateList.RemoveAt(i);
                        i--;
                    }
                    //if this player used to exist, but doesn't anymore (left game/zone)
                    else if (playerIDs.Contains(us.ID))
                    {
                        //Debug.Log("player id doesn't exist anymore");
                        cliManager.UpdateList.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        Debug.Log("player id doesn't exist");
                    }
                    break;
                case EUpdateStructType.MONSTER:
                    if (enemies.ContainsKey(us.ID))
                    {
                        EnemyScript e = enemies[us.ID].GetComponent<EnemyScript>();

                        foreach (KeyValuePair<EStat, int> pair in us.Stats)
                        {
                            switch (pair.Key)
                            {
                                case EStat.HEALTH:
                                    e.health = pair.Value;
                                    break;
                                case EStat.BLOCKING:
                                    e.SetBlocking(pair.Value);
                                    break;
                                default:
                                    Debug.Log("Enemies don't have " + pair.Key.ToString());
                                    break;
                            }
                        }
                        
                        cliManager.UpdateList.RemoveAt(i);
                        i--;
                    }
                    else if (enemyIDs.Contains(us.ID))
                    {
                        //Debug.Log("enemy id doesn't exist anymore");
                        cliManager.UpdateList.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        
                    }
                    break;
                default:
                    Debug.Log("Unknown type of updatestruct");
                    break;
            }
        }

        /*
         * Handles incoming action packets
         */
        for (int i = 0; i < cliManager.ActionList.Count; i++)
        {
            ActionStruct actStruct = cliManager.ActionList[0];

            if (players.ContainsKey(actStruct.SenderID))
            {
                //players[actStruct.SenderID].GetComponent<PlayerScript>().PlayAnimation((int)actStruct.ActionType);

                cliManager.ActionList.RemoveAt(i);
                i--;
            }
            else if (playerIDs.Contains(actStruct.SenderID))
            {
                //Debug.Log("player id doesn't exist anymore [action]");
                cliManager.ActionList.RemoveAt(i);
                i--;
            }
            else
            {

            }
        }
    }

    public Transform GetPlayerByID(int id)
    {
        if (players.ContainsKey(id))
            return players[id];

        return null;
    }

    /// <summary>
    /// Called when the player logs out
    /// </summary>
    public void Logout()
    {
        player.GetComponent<TargetHandler>().Logout();

        foreach (KeyValuePair<int, Transform> pair in players)
        {
            if (pair.Value != null)
                Destroy(pair.Value.gameObject);
        }

        foreach (KeyValuePair<int, Transform> pair in enemies)
        {
            if (pair.Value != null)
                Destroy(pair.Value.gameObject);
        }

        Destroy(Camera.main.gameObject);
        Destroy(gameObject);
    }

    /// <summary>
    /// Sends a keep alive packet to the server
    /// </summary>
    private void SendPing()
    {
        cliManager.Send(new PacketPing());
    }
}
