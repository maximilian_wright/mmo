﻿using UnityEngine;
using Client.Game;

public class GuiCharacter : Gui
{
    public GUIStyle style;

    private PlayerScript playerScript;

    void Awake()
    {
        ID = 6;

        playerScript = GetComponent<PlayerScript>();
    }

    void OnGUI()
    {
        area = GUILayout.Window(ID, area, OnWindow, "Character");
        area.x = Mathf.Clamp(area.x, 0, Screen.width - area.width);
        area.y = Mathf.Clamp(area.y, 0, Screen.height - area.height);
    }

    void OnWindow(int id)
    {
        GUI.DragWindow(new Rect(0, 0, 10000, 20));

        Player p = playerScript.Player;

        GUILayout.Label(p.Name);
        GUILayout.Label("HP: " + p.Health + " / " + p.MaxHealth);
    }
}
