﻿using System.Collections.Generic;
using UnityEngine;

public class Options
{
    public static Options instance;

    public const string KEY_SHOW_MY_NAME = "key_show-my-name";
    public const string KEY_SHOW_OTHER_NAMES = "key_show-other-names";

    private Dictionary<string, object> values;

    static Options()
    {
        if (instance == null)
            instance = new Options();
    }

    public void Save()
    {

    }

    public void Load()
    {

    }
}