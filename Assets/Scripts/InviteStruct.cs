﻿public class InviteStruct
{
    public int PartyID;
    public int SenderID;
    public string SenderName;
    
    public InviteStruct(int pid, int sid, string name)
    {
        PartyID = pid;
        SenderID = sid;
        SenderName = name;
    }
}