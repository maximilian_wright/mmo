﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingMenu : MonoBehaviour
{
    public GameObject mainMenuPanel;
    public GameObject loadingPanel;
    public Text message;
    public Slider slider;
    public GameObject okButton;
    public GameObject cancelButton;

    private enum LoadState { MENU, LOGIN, LOADING, DONE, FAILED }
    private LoadState state = LoadState.MENU;

    private const float CONNECT_TIMEOUT = 10f;
    private float connectTimer = 0;

    private AsyncOperation aop;

    public void Load()
    {
        state = LoadState.LOGIN;
        connectTimer = 0;
        message.text = "Logging in...";
        cancelButton.SetActive(true);
        okButton.SetActive(false);
    }

    void Update()
    {
        if (state == LoadState.LOGIN)
        {
            connectTimer += Time.deltaTime;

            if (connectTimer >= CONNECT_TIMEOUT)
            {
                ClientManager.Instance.CancelSending();
                state = LoadState.FAILED;
                message.text = "Error connecting to server!";
                cancelButton.SetActive(false);
                okButton.SetActive(true);
            }

            if (ClientManager.Instance.IsLoggedIn)
                StartCoroutine("StartLoad");
        }

        if (aop == null) return;

        slider.value = aop.progress;

        if (aop.progress >= 0.9f)
        {
            state = LoadState.DONE;
            message.text = "Load complete!";
            slider.value = 1f;
            aop.allowSceneActivation = true;
            loadingPanel.SetActive(false);
        }
    }

    public void Cancel()
    {
        ClientManager.Instance.CancelSending();

        Return();
    }

    public void Return()
    {
        state = LoadState.MENU;
        mainMenuPanel.SetActive(true);
        loadingPanel.SetActive(false);
    }
    
    private IEnumerator StartLoad()
    {
        state = LoadState.LOADING;
        message.text = "Loading world...";
        Destroy(cancelButton);
        Destroy(okButton);

        aop = SceneManager.LoadSceneAsync("LobbyTown");
        aop.allowSceneActivation = false;
        yield return aop;
    }
}
