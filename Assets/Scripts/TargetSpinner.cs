﻿using UnityEngine;
using System.Collections;

public class TargetSpinner : MonoBehaviour
{
    [SerializeField]
    private float spinSpeed = 10;

    void Update()
    {
        transform.Rotate(new Vector3(0.0f, spinSpeed, 0.0f));
        transform.localPosition = new Vector3(transform.localPosition.x, Mathf.PingPong(Time.time, 0.5f), transform.localPosition.z);
    }
}
