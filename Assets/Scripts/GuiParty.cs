﻿using UnityEngine;
using System.Collections.Generic;
using Client;

public class GuiParty : Gui
{
    private PlayerScript playerScript;
    private TargetHandler targetHandler;
    private ClientManager cliManager;

    private bool partyOwner = false;
    private int myid;
    
    void Awake()
    {
        ID = 7;

        playerScript = GetComponent<PlayerScript>();
        targetHandler = GetComponent<TargetHandler>();
        cliManager = ClientManager.Instance;
    }
    
    public override void Start()
    {
        area = new Rect(Screen.width - WIDTH / 2, Screen.height / 2 - HEIGHT / 2, WIDTH, HEIGHT);
    }

    void OnGUI()
    {
        area = GUILayout.Window(ID, area, OnWindow, "Party");
        area.x = Mathf.Clamp(area.x, 0, Screen.width - area.width);
        area.y = Mathf.Clamp(area.y, 0, Screen.height - area.height);
    }

    public override void Update()
    {
        IsMouseOverWindow = IsMouseOver || IsMouseOverWindow;
    }

    void OnWindow(int id)
    {
        GUI.DragWindow(new Rect(0, 0, 10000, 20));

        if (playerScript.Party == -1)
        {
            if (GUILayout.Button("Create Party", GUILayout.ExpandHeight(true)))
            {
                cliManager.Send(new PacketCreateParty());
            }
        }
        else
        {
            GUILayout.BeginHorizontal();

            if (targetHandler.GetTargetID() == -1)
                GUI.enabled = false;

            if (GUILayout.Button("Invite"))
            {
                InviteParty(targetHandler.GetTargetPlayerID());
            }

            if (targetHandler.GetTargetID() == -1)
                GUI.enabled = true;

            GUILayout.EndHorizontal();

            Dictionary<int, string> party = playerScript.PartyMembers;
            foreach (KeyValuePair<int, string> pair in party)
            {
                GUILayout.BeginHorizontal();
                if (partyOwner)
                {
                    if (pair.Key == myid)
                        GUILayout.Label(pair.Value);
                    else if (GUILayout.Button(pair.Value))
                        cliManager.Send(new PacketLeaveParty(pair.Key));
                }
                else
                {
                    GUILayout.Label(pair.Value);
                }
                GUILayout.EndHorizontal();
            }

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Leave Party"))
            {
                cliManager.Send(new PacketLeaveParty(myid));
            }
        }
    }

    public void InviteParty(int id)
    {
        cliManager.Send(new PacketPartyInvite(id));
        Debug.Log("Invited player " + id + " to party!");
    }

    public void SetOwned()
    {
        partyOwner = true;
    }

    public void SetID(int id)
    {
        myid = id;
    }
}
