﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(GuiCharacter))]
[RequireComponent(typeof(GuiChat))]
[RequireComponent(typeof(GuiParty))]
public class Hotbar : Gui
{
    public static KeyCode KEY_CHARACTER = KeyCode.C;
    public static KeyCode KEY_CHAT = KeyCode.T;
    public static KeyCode KEY_PARTY = KeyCode.P;
    public static KeyCode KEY_SKILLS = KeyCode.K;

    private static KeyCode KEY_DIALOG = KeyCode.Joystick1Button19;

    private Dictionary<KeyCode, Gui> guis;
    private HashSet<KeyCode> destroy;
    private int numShowing = 0;
    private HashSet<KeyCode> showing;

    private Rect menuButtonArea;
    private bool open = false;
    private bool hidden = false;

    public override void Start()
    {
        guis = new Dictionary<KeyCode, Gui>();
        guis.Add(KEY_CHARACTER, GetComponent<GuiCharacter>());
        guis.Add(KEY_CHAT, GetComponent<GuiChat>());
        guis.Add(KEY_PARTY, GetComponent<GuiParty>());
        guis.Add(KEY_SKILLS, GetComponent<GuiSkills>());
        destroy = new HashSet<KeyCode>();
        showing = new HashSet<KeyCode>();

        Show(KEY_CHARACTER);
        Show(KEY_CHAT);
        Show(KEY_SKILLS);

        area = new Rect(0, Screen.height - HEIGHT, WIDTH, HEIGHT);
        menuButtonArea = new Rect(0, Screen.height - 40, WIDTH, 40);
    }

    public override void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (numShowing > 0)
            {
                foreach (KeyValuePair<KeyCode, Gui> pair in guis)
                    pair.Value.enabled = false;
            }
            else
                open = !open;
        }

        //reset and recalculate if mouse over window
        IsMouseOverWindow = false;

        foreach (KeyValuePair<KeyCode, Gui> pair in guis)
        {
            try
            {
                if (Input.GetKeyUp(pair.Key))
                    Show(pair.Key);

                IsMouseOverWindow = pair.Value.IsMouseOver || IsMouseOverWindow;
            }
            catch (MissingReferenceException)
            {
                destroy.Add(pair.Key);
            }
        }

        foreach (KeyCode code in destroy)
            guis.Remove(code);
        destroy.Clear();

        if (numShowing == 0)
            IsMouseOverWindow = false;

        IsMouseOverWindow = IsMouseOver || IsMouseOverWindow;
    }

    public override bool IsMouseOver
    {
        get
        {
            if (open)
                return area.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y));

            return menuButtonArea.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y));
        }
    }

    public void AddDialog(Dialog d)
    {
        if (guis.ContainsKey(KEY_DIALOG))
        {
            Destroy(guis[KEY_DIALOG]);
            guis.Remove(KEY_DIALOG);
        }

        guis[KEY_DIALOG] = d;
    }

    private void Show(KeyCode key)
    {
        guis[key].enabled = !guis[key].enabled;

        if (guis[key].enabled)
            numShowing++;
        else
            numShowing--;
    }

    void OnGUI()
    {
        if (hidden) return;

        GUILayout.BeginArea(area);

        GUILayout.BeginVertical();

        if (open)
        {
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Character (C)", GUILayout.Height(40)))
            {
                Show(KEY_CHARACTER);
            }
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Party (P)", GUILayout.Height(40)))
            {
                Show(KEY_PARTY);
            }
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Settings", GUILayout.Height(40)))
            {

            }
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Logout", GUILayout.Height(40)))
            {
                FindObjectOfType<InstanceManager>().Logout();
                ClientManager.DestroyInstance();
                SceneManager.LoadScene("menu");
            }
            GUILayout.FlexibleSpace();
        }
        else
            GUILayout.FlexibleSpace();

        if (GUILayout.Button("Menu", GUILayout.Height(40)))
            open = !open;

        GUILayout.EndVertical();

        GUILayout.EndArea();
    }

    public void Hide()
    {
        if (hidden) return;

        showing.Clear();

        foreach (KeyValuePair<KeyCode, Gui> pair in guis)
        {
            try
            {
                if (pair.Value == null) continue;

                if (pair.Value.enabled)
                {
                    showing.Add(pair.Key);
                    Show(pair.Key);
                }
            }
            catch (MissingReferenceException)
            {
                destroy.Add(pair.Key);
            }
        }

        foreach (KeyCode code in destroy)
            guis.Remove(code);
        destroy.Clear();

        hidden = true;
    }

    public void Show()
    {
        if (!hidden) return;

        foreach (KeyCode code in showing)
            Show(code);

        hidden = false;
    }
}
