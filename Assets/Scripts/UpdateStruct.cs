﻿using System.Collections.Generic;

public struct UpdateStruct
{
    public EUpdateStructType Type;
    public int ID;
    public SortedList<EStat, int> Stats;

    public UpdateStruct(EUpdateStructType type, int id, SortedList<EStat, int> stats)
    {
        Type = type;
        ID = id;
        Stats = stats;
    }
}