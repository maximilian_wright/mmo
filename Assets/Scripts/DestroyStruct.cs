﻿public class DestroyStruct
{
    public EDestroyStructType Type;
    public int ID;

    public DestroyStruct(EDestroyStructType type, int id)
    {
        Type = type;
        ID = id;
    }
}