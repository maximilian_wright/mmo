﻿using UnityEngine;
public struct SimulateStruct
{
    public int EnemyID;
    public Vector3 Position;
    public Quaternion Rotation;
    public float Velocity;
    public int TargetID;

    public SimulateStruct(int eid, Vector3 pos, Quaternion rot, float vel, int tar)
    {
        EnemyID = eid;
        Position = pos;
        Rotation = rot;
        Velocity = vel;
        TargetID = tar;
    }
}
