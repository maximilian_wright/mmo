﻿using UnityEngine;
using System.Collections.Generic;
using Client;

public class GuiChat : Gui
{
    public int MAX_MESSAGES = 200;
    public GameObject chatPrefab;

    private ClientManager cliManager;
    private InstanceManager iManager;

    private bool autoscroll = true;

    private Vector2 scrollPos = Vector2.zero;
    private List<ChatMessage> messages;

    private string text = "";

    void Awake()
    {
        ID = 8;
        
        cliManager = ClientManager.Instance;
        iManager = FindObjectOfType<InstanceManager>();
        messages = new List<ChatMessage>();
    }

    public override void Start()
    {
        area = new Rect(150, Screen.height - HEIGHT, WIDTH, HEIGHT);
    }

    void OnGUI()
    {
        area = GUILayout.Window(ID, area, OnWindow, "Chat");
        area.x = Mathf.Clamp(area.x, 0, Screen.width - area.width);
        area.y = Mathf.Clamp(area.y, 0, Screen.height - area.height);
    }

    public override void Update()
    {
        while (cliManager.ChatList.Count > 0)
        {
            ChatMessage cm = cliManager.ChatList[0];
            cliManager.ChatList.RemoveAt(0);
            AddMessage(cm);

            //spawn chat bubble
            if (cm.Type == ChatMessage.EChatType.PLAYER)
            {
                Transform p = iManager.GetPlayerByID(cm.PlayerID);

                if (chatPrefab != null)
                {
                    GameObject msg = (GameObject)Instantiate(chatPrefab, p.position, Quaternion.identity);
                    msg.GetComponent<SpeechBubble>().Init(p, cm.Message);
                }
            }
        }

        while (messages.Count > MAX_MESSAGES)
            messages.RemoveAt(messages.Count - 1);

        IsMouseOverWindow = IsMouseOver || IsMouseOverWindow;
    }

    void OnWindow(int id)
    {
        GUI.DragWindow(new Rect(0, 0, 10000, 20));

        scrollPos = GUILayout.BeginScrollView(scrollPos);

        for (int i = 0; i < messages.Count; i++)
        {
            GUILayout.BeginHorizontal();
            if (messages[i].Type == ChatMessage.EChatType.PLAYER)
                GUILayout.Label("[" + messages[i].Name + "] " + messages[i].Message);
            else
                GUILayout.Label(messages[i].Message);
            GUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();
        text = GUILayout.TextField(text, GUILayout.ExpandWidth(true));
        autoscroll = GUILayout.Toggle(autoscroll, "", GUILayout.Width(20));
        if (GUILayout.Button("Send", GUILayout.Width(50)))
        {
            if (text.Trim().Length != 0)
            {
                cliManager.Send(new PacketChatMessage(text.Trim() + '\0'));
                text = "";
            }
        }
        GUILayout.EndHorizontal();
    }

    public void AddMessage(ChatMessage msg)
    {
        messages.Add(msg);

        if (autoscroll)
            scrollPos.y = Mathf.Infinity;
    }

    public void AddMessage(string s)
    {
        AddMessage(new ChatMessage(ChatMessage.EChatType.INFO, 0, "", s));
    }
}
