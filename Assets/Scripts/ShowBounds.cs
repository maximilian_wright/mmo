﻿using UnityEngine;
using System.Collections;

public class ShowBounds : MonoBehaviour
{
    void OnDrawGizmos()
    {
        Renderer r = GetComponent<Renderer>();

        if (r)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(r.bounds.center, r.bounds.size);
        }
    }
}
