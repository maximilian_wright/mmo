﻿using UnityEngine;
using System.Collections.Generic;
using Client;
using Client.Game;

public class PlayerScript : MonoBehaviour
{
    private ClientManager cliManager;

    private bool loaded = false;
    private bool sending = false;

    private bool localPlayer;
    private int uid;

    [SerializeField]
    private string displayName; //the name that will be shown in the client aka username

    private float movementSendWaitTime = 0.03f;

    private Vector3 lastPos;
    private float lastRot;

    private Vector3 desiredPos = Vector3.zero;
    private Vector3 prevPos = Vector3.zero;
    private float movementTimer = 0.0f;

    private bool blocking = false;

    private Animator m_Animator;
    //[SerializeField]
    //float m_RunCycleLegOffset = 0.2f;
    //[SerializeField]
    //float m_AnimSpeedMultiplier = 1f;
    //private bool m_IsGrounded;

    private int party = -1;
    private Dictionary<int, string> partyMembers;
    private Player player;
    private GuiChat chat;

    private int zone = 0;

    public Player Player
    {
        get { return player; }
    }

    public int ID
    {
        get { return uid; }
    }

    public int Party
    {
        get { return party; }
    }

    public int Zone
    {
        get { return zone; }
    }

    public Dictionary<int, string> PartyMembers
    {
        get { return partyMembers; }
    }

    void Awake()
    {
        if (transform.parent != null)
            DontDestroyOnLoad(transform.parent.gameObject);
    }

    void Start()
    {
        cliManager = ClientManager.Instance;
        partyMembers = new Dictionary<int, string>();
        chat = GetComponent<GuiChat>();
        //m_IsGrounded = false;

        desiredPos = transform.position;
        prevPos = transform.position;
    }

    void Update()
    {
        if (!loaded) return;

        if (player.IsDead) return;

        if (localPlayer)
            LocalUpdate();
        else
            SyncedUpdate();
    }

    /// <summary>
    /// Called if this player is a local player
    /// </summary>
    private void LocalUpdate()
    {
        if (!sending)
            StartSendMovePacket();
    }

    /// <summary>
    /// Called if this player is NOT a local player.
    /// Updates and moves the player
    /// </summary>
    private void SyncedUpdate()
    {
        movementTimer += Time.deltaTime;
        Vector3 curPos = Vector3.Lerp(prevPos, desiredPos, (movementTimer / 0.1f));
        transform.position = curPos;

        UpdateAnimator();
    }

    /// <summary>
    /// Called when the player is spawned. Used to setup vars
    /// </summary>
    /// <param name="local">Is the player local controlled?</param>
    /// <param name="id">Player id</param>
    /// <param name="username">player's username</param>
    /// <param name="pos">player's position</param>
    public void Init(bool local, int id, Vector3 pos, string username = "")
    {
        localPlayer = local;
        uid = id;
        name = "" + uid;
        player = new Player(username);
        SetTargetPosition(pos);

        if (localPlayer)
            GetComponent<GuiParty>().SetID(uid);

        if (username != "") displayName = username;
        else displayName = "Unknown";

        loaded = true;
        Debug.Log("Finished loading player. local player: " + localPlayer);

        m_Animator = gameObject.GetComponent<Animator>();
    }

    /// <summary>
    /// Called to begin repeatedly calling SendPlayerMovePacket
    /// </summary>
    private void StartSendMovePacket()
    {
        sending = true;
        InvokeRepeating("SendPlayerMovePacket", movementSendWaitTime, movementSendWaitTime);
    }

    /// <summary>
    /// Sends a move packet if the player has moved or rotated
    /// </summary>
    void SendPlayerMovePacket()
    {
        //Debug.Log("sending move packet");

        if (transform.position != lastPos || transform.rotation.eulerAngles.y != lastRot)
        {
            Vector3 userPos = transform.position;

            float xPos = userPos.x;
            float yPos = userPos.y;
            float zPos = userPos.z;
            float yRot = transform.rotation.eulerAngles.y;
            float vMag = 0.0f;

            cliManager.Send(new PacketMovePlayer(uid, xPos, yPos, zPos, yRot, vMag));

            lastPos = transform.position;
            lastRot = transform.rotation.eulerAngles.y;
        }
    }

    /// <summary>
    /// Called when the player dies on the server
    /// </summary>
    public void Die()
    {
        player.Health = 0;

        //transform.rotation = Quaternion.Euler(90, 0, 0);
        m_Animator.SetBool("Dead", true);
        //m_Animator.Stop();

        //disable all controls
        if (localPlayer)
        {
            TargetHandler th = GetComponent<TargetHandler>();
            th.ClearTarget();
            th.enabled = false;
            GetComponent<OLPlayerController>().enabled = false;
            GetComponent<CharacterController>().enabled = false;
        }
    }

    /// <summary>
    /// Sets the target move to position for updating a non-local player
    /// </summary>
    /// <param name="tPos">target position</param>
    public void SetTargetPosition(Vector3 tPos)
    {
        prevPos = desiredPos;
        desiredPos = tPos;
        movementTimer = 0.0f;
    }

    /// <summary>
    /// Returns the players name
    /// </summary>
    /// <returns>A string containing the player's name</returns>
    public string GetPlayerName()
    {
        return displayName;
    }

    /// <summary>
    /// Is this player controlled locally?
    /// </summary>
    /// <returns>boolean is this player local controlled?</returns>
    public bool IsLocal()
    {
        return localPlayer;
    }

    void UpdateAnimator()
    {
        Vector3 velocity = (desiredPos - prevPos) / movementSendWaitTime;
        float mag = Vector3.ClampMagnitude(velocity, 6.0f).magnitude;
        if (mag < 1.0f) mag = 0.0f;

        //m_IsGrounded = true;

        m_Animator.SetFloat("speed", mag, 3.0f, 0.8f);

        bool attackState = m_Animator.GetCurrentAnimatorStateInfo(0).IsName("attacks");
        if (attackState)
        {
            m_Animator.SetBool("attack", false);
        }
    }

    /// <summary>
    /// Called when the player creates a party
    /// </summary>
    /// <param name="id">party id</param>
    public void CreateParty(int id)
    {
        party = id;
        GetComponent<GuiParty>().SetOwned();
    }

    /// <summary>
    /// Called when the player joins a party
    /// </summary>
    /// <param name="id">party id</param>
    public void JoinParty(int id)
    {
        party = id;
    }

    /// <summary>
    /// Called when a player joins the party
    /// </summary>
    /// <param name="ms">member struct with id and name</param>
    public void AddPartyMember(MemberStruct ms)
    {
        partyMembers.Add(ms.PlayerID, ms.PlayerName);
        chat.AddMessage(ms.PlayerName + " joined the party.");
    }

    /// <summary>
    /// Called when a player leaves the party
    /// </summary>
    /// <param name="id">player id</param>
    public void RemovePartyMember(int id)
    {
        if (party == -1) return;

        if (id == uid)
            LeaveParty();
        else
        {
            string name = partyMembers[id];
            chat.AddMessage(name + " left the party.");
            partyMembers.Remove(id);
        }
    }

    /// <summary>
    /// Called when the player leaves or is kicked from the party
    /// </summary>
    public void LeaveParty()
    {
        party = -1;
        partyMembers.Clear();
        chat.AddMessage("You left the party.");
    }

    /// <summary>
    /// Called when the player enters a zone
    /// </summary>
    /// <param name="id"></param>
    public void ZoneEnter(int id)
    {
        Debug.Log("entered zone " + id);
        zone = id;

        if (id == 0)
            chat.AddMessage("Entering the hub...");
        else
            chat.AddMessage("Entering the dungeon...");
    }

    /// <summary>
    /// Returns the player's id
    /// </summary>
    /// <returns>int of player's id</returns>
    public int GetPlayerID()
    {
        return uid;
    }

    /// <summary>
    /// Called to set whether this player is blocking
    /// </summary>
    /// <param name="blck">0 or 1</param>
    public void SetBlocking(int blck)
    {
        blocking = blck == 1;
    }

    void CheckGroundStatus()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * 0.1f));
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, 0.1f))
        {
            //m_IsGrounded = true;
            m_Animator.applyRootMotion = true;
        }
        else
        {
            //m_IsGrounded = false;
            m_Animator.applyRootMotion = false;
        }
    }

    public void PlayAnimation(int animID)
    {
        switch (animID)
        {
            case 0: //auto attack
                m_Animator.SetBool("attack", true);
                break;
        }
    }
}
