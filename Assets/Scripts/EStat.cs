﻿public enum EStat
{
    HEALTH          = 1,
    STRENGTH        = 2,
    STAMINA         = 3,
    INTELLIGENCE    = 4,
    DEXTERITY       = 5,
    BLOCKING        = 6
}