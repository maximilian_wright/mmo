﻿using Client;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

//[RequireComponent(typeof(NavMeshAgent))]
public class EnemyScript : MonoBehaviour
{
    private ClientManager cliManager;

    private int etype;
    private int eid;
    public int health;
    public int maxHealth;
    private bool blocking = false;
    private bool dead = false;

    private enum EnemyState { WAITING, MOVING, CHASING }
    private EnemyState state = EnemyState.WAITING;
    private NavMeshAgent agent;

    private bool loaded = false;
    private bool isControlled = false;

    private Transform target; //target player
    private Vector3 targetPos;

    private Vector3 spawnPos;

    private Animation anime;

    public GameObject exclaimPrefab;
    private Vector3 adjust = new Vector3(0, 2f, 0);

    public float autoAttackWaitTime = 1.5f;
    public float autoAttackRange = 2.0f;
    private float autoAttackCooldownLeft = 0;

    //wander vars
    public float radius = 8;
    private float wait;
    public float minWait = 4;
    public float maxWait = 11;
    private float waitTimer = 0;

    private float cancelTimer = 0;
    private const float CANCEL_TIMEOUT = 10f;

    //send update vars
    private Vector3 lastPos;
    private float lastRot;

    //sync update vars
    private Vector3 desiredPos = Vector3.zero;
    private Vector3 prevPos = Vector3.zero;
    private float movementTimer = 0.0f;

    public int EnemyID
    {
        get { return eid; }
    }

    public bool IsControlled
    {
        get { return isControlled; }
    }

    void Awake()
    {
        if (SceneManager.GetActiveScene().name != "menu")
            DontDestroyOnLoad(gameObject);

        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        cliManager = ClientManager.Instance;

        spawnPos = transform.position;
        health = maxHealth;

        anime = gameObject.GetComponent<Animation>();
    }
    
    void Update()
    {
        if (!loaded || dead) return;

        if (isControlled)
            ControlUpdate();
        else
            SyncUpdate();
    }

    /// <summary>
    /// Called when this client is controlling this enemy
    /// </summary>
    private void ControlUpdate()
    {
        //if no target, wander
        if (target == null)
        {
            switch (state)
            {
                case EnemyState.WAITING:
                    waitTimer += Time.deltaTime;

                    if (!anime.IsPlaying("Attack")) anime.Play("Idle");

                    if (waitTimer >= wait)
                        GenerateMoveTarget();
                    break;
                case EnemyState.MOVING: //moving to targetPos
                    cancelTimer += Time.deltaTime;

                    if (cancelTimer >= CANCEL_TIMEOUT || agent.remainingDistance <= 0.05f)
                    {
                        state = EnemyState.WAITING;
                        cancelTimer = 0;
                    }
                    if (!anime.IsPlaying("Attack")) anime.Play("Run");
					
                    break;
            }
        }
        else
        {
            agent.SetDestination(target.position);

            if (agent.remainingDistance < 2.0f)
            {
                if (!anime.IsPlaying("Attack")) anime.Play("Idle");
                transform.rotation = Quaternion.LookRotation(target.position - transform.position);
            }
            else if (!anime.IsPlaying("Attack")) anime.Play("Run");

            HandleAutoAttacks();
        }
    }

    private void HandleAutoAttacks()
    {
        if (autoAttackCooldownLeft <= 0.0f)
        {
            if (Vector3.Distance(transform.position, target.transform.position) < autoAttackRange)
            {
                if (Vector3.Angle(transform.forward, (target.transform.position - transform.position)) < 60.0f)
                {
                    cliManager.Send(new PacketEnemyAction(eid, 1, 1, target.GetComponent<PlayerScript>().ID));

                    autoAttackCooldownLeft = autoAttackWaitTime;

                    anime["Attack"].wrapMode = WrapMode.Once;
                    anime.Play("Attack");
                    anime["Attack"].speed = 2.0f;
                }
            }
        }

        if (autoAttackCooldownLeft > 0)
        {
            autoAttackCooldownLeft -= Time.deltaTime;
        }
    }

    /// <summary>
    /// Called to update an enemy's position
    /// </summary>
    private void SyncUpdate()
    {
        movementTimer += Time.deltaTime;
        Vector3 curPos = Vector3.Lerp(prevPos, desiredPos, (movementTimer / 0.1f));
        transform.position = curPos;

        if (curPos != desiredPos)
            anime.Play("Run");
        else
            anime.Play("Idle");
    }

    public void Init(int type, int id)
    {
        etype = type;
        eid = id;

        loaded = true;
    }

    private void GenerateMoveTarget()
    {
        wait = Random.Range(minWait, maxWait);
        targetPos = new Vector3(Random.Range(spawnPos.x - radius, spawnPos.x + radius), 0, Random.Range(spawnPos.z - radius, spawnPos.z + radius));

        //check if out of bounds
        Ray ray = new Ray(transform.position, targetPos - transform.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, radius))
        {
            targetPos = ray.GetPoint(hit.distance - 1);
        }

        SetMoveTarget(targetPos);
        waitTimer = 0;
    }

    /// <summary>
    /// Calls SetControlled(contr, Vector3.zero, Quaternion.identity)
    /// </summary>
    /// <param name="contr"></param>
    public void SetControlled(bool contr)
    {
        SetControlled(false, Vector3.zero, Quaternion.identity);
    }

    /// <summary>
    /// Sets the controlled state of this enemy
    /// </summary>
    /// <param name="contr">whether is locally controlled</param>
    /// <param name="pos">updated position from server</param>
    /// <param name="rot">updated rotation from server</param>
    public void SetControlled(bool contr, Vector3 pos, Quaternion rot)
    {
        isControlled = contr;

        if (isControlled)
        {
            transform.position = pos;
            transform.rotation = rot;
        }

        if (isControlled)
            StartSending();
        else
            StopSending();
    }

    /// <summary>
    /// Called to set the sync update desired position. This is used
    /// when receiving enemy move updates
    /// </summary>
    /// <param name="tar">Position to sync to</param>
    public void SetTargetMovePosition(Vector3 tar)
    {
        prevPos = transform.position;
        desiredPos = tar;
    }

    /// <summary>
    /// Sets this enemy's player target
    /// </summary>
    /// <param name="tar">player transform to target</param>
    public void SetPlayerTarget(Transform tar)
    {
        state = EnemyState.CHASING;
        target = tar;
        agent.stoppingDistance = 2.5f;

        if (exclaimPrefab != null)
        {
            GameObject exclaim = (GameObject)Instantiate(exclaimPrefab, transform.position + adjust, Quaternion.identity);
            exclaim.transform.SetParent(transform);
        }
    }

    /// <summary>
    /// Sets this enemy's position to move to
    /// </summary>
    /// <param name="pos"></param>
    public void SetMoveTarget(Vector3 pos)
    {
        state = EnemyState.MOVING;
        agent.SetDestination(pos);
        agent.stoppingDistance = 0;
    }

    public void ClearTarget()
    {
        target = null;
        agent.Stop();
    }

    public void SetBlocking(int block)
    {
        blocking = block == 1;
    }

    /// <summary>
    /// Called to destroy an enemy
    /// </summary>
    public IEnumerator Die()
    {
        StopSending();

        dead = true;
        FindObjectOfType<TargetHandler>().ClearTarget();

        switch (etype)
        {
            case 1:
                anime.Play("Dead");
                yield return new WaitForSeconds(anime.GetClip("Dead").length);
                break;
            case 2:
                anime.Play("Death");
                yield return new WaitForSeconds(anime.GetClip("Death").length);
                break;
            case 3:
                anime.Play("death");
                yield return new WaitForSeconds(anime.GetClip("death").length);
                break;
        }

        tag = "Untagged";
    }

    private void StartSending()
    {
        InvokeRepeating("SendMovePacket", 0.05f, 0.05f);
    }

    private void SendMovePacket()
    {
        if (transform.position != lastPos || transform.rotation.eulerAngles.y != lastRot)
        {
            Vector3 userPos = transform.position;

            float xPos = userPos.x;
            float yPos = userPos.y;
            float zPos = userPos.z;
            float yRot = transform.rotation.eulerAngles.y;
            float vMag = 0.0f;

            cliManager.Send(new PacketMoveEnemy(eid, xPos, yPos, zPos, yRot, vMag));

            lastPos = transform.position;
            lastRot = transform.rotation.eulerAngles.y;
        }
    }

    private void StopSending()
    {
        CancelInvoke("SendMovePacket");
    }
}
