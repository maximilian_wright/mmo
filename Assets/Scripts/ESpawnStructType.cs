﻿public enum ESpawnStructType
{
    PLAYER, PLAYER_OTHER, ITEM, MONSTER
}