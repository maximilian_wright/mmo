﻿using UnityEngine;

public class DamageText : MonoBehaviour
{
    public float time = 1f;
    public float moveSpeed = 2;

    void Update()
    {
        time -= Time.deltaTime;
        transform.Translate(Vector3.up * moveSpeed * Time.deltaTime / 2.0f);

        if (time <= 0)
            Destroy(gameObject);
    }

    public void Create(DamageStruct ds)
    {
        GetComponent<TextMesh>().text = "" + ds.Damage;
    }
}
