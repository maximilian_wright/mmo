﻿using UnityEngine;

public struct MoveStruct
{
    public EMoveStructType Type;
    public int ID;
    public Vector3 Position;
    public Quaternion Rotation;

    public MoveStruct(EMoveStructType type, int id, Vector3 pos) : this(type, id, pos, Quaternion.identity) { }
    public MoveStruct(EMoveStructType type, int id, Vector3 pos, Quaternion rot)
    {
        Type = type;
        ID = id;
        Position = pos;
        Rotation = rot;
    }
}