﻿public class MemberStruct
{
    public int PlayerID;
    public string PlayerName;
    
    public MemberStruct(int pid, string name)
    {
        PlayerID = pid;
        PlayerName = name;
    }
}