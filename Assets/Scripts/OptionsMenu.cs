﻿using UnityEngine;

public class OptionsMenu : MonoBehaviour
{
    public GameObject mainMenuPanel;
    public GameObject optionsPanel;
	
	public void Back()
    {
        mainMenuPanel.SetActive(true);
        optionsPanel.SetActive(false);
    }
}
