﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TransitionMenu : MonoBehaviour
{
    private enum TransitionState { OUT, LOAD, IN }
    private TransitionState state;

    private Texture2D image;
    private Hotbar hotbar;
    private RawImage imgDungeon;
    private RawImage imgHub;
    private Fade fade;

    private string _scene;
    private AsyncOperation aop;

    private float alpha;
    private const float TRANSITION_SPEED = 0.44f;

    private Color temp;
    private Color current;

    public bool LoadFinished
    {
        get { return state == TransitionState.IN; }
    }

    void Start()
    {
        alpha = 0f;
        hotbar = FindObjectOfType<Hotbar>();
    }

    void OnEnable()
    {
        TargetHandler th = GetComponent<TargetHandler>();
        th.ClearTarget();
        th.enabled = false;
        GetComponent<OLPlayerController>().enabled = false;
        GetComponent<CharacterController>().enabled = false;
        transform.parent.GetChild(1).GetComponent<RotateCamera>().enabled = false;
    }

    void OnDestroy()
    {
        GetComponent<TargetHandler>().enabled = true;
        GetComponent<OLPlayerController>().enabled = true;
        GetComponent<CharacterController>().enabled = true;
    }
    
    void Update()
    {
        switch (state)
        {
            case TransitionState.OUT:
                hotbar.Hide();
                alpha += TRANSITION_SPEED * Time.deltaTime;
                alpha = Mathf.Clamp(alpha, 0, 1);
                current = new Color(1f, 1f, 1f, alpha);

                if (alpha == 1)
                    StartCoroutine("StartLoad");
                break;
            case TransitionState.LOAD:
                if (aop == null) return;

                if (aop.progress >= 0.9f)
                {
                    aop.allowSceneActivation = true;
                    state = TransitionState.IN;
                }
                break;
            case TransitionState.IN:
                alpha -= TRANSITION_SPEED * Time.deltaTime;
                alpha = Mathf.Clamp(alpha, 0, 1);
                current = new Color(1f, 1f, 1f, alpha);

                if (alpha == 0)
                {
                    hotbar.Show();
                    Destroy(this);
                }
                break;
        }
    }

    void OnLevelWasLoaded(int level)
    {
        Debug.Log("level loaded: " + level);
        
        GameObject ps = GameObject.FindGameObjectWithTag("PlayerSpawn");
        Transform player = GameObject.FindGameObjectWithTag("Player").transform;

        Debug.Log("ps: " + ps);
        Debug.Log("ps pos: " + ps.transform.position);
        Debug.Log("player trans: " + player);

        if (ps != null)
        {
            player.position = ps.transform.position;
            player.rotation = ps.transform.rotation;
            Debug.Log("player pos by spawn: " + player.position);
        }
        else
        {
            player.position = Vector3.zero;
            Debug.Log("player pos to zero: " + player.position);
        }
            
        transform.parent.GetChild(1).GetComponent<RotateCamera>().enabled = true;
    }

    void OnGUI()
    {
        temp = GUI.color;
        GUI.color = current;
        if (image != null)
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), image);
        GUI.color = temp;
    }

    public void LoadScene(string scene, Texture2D tex = null)
    {
        _scene = scene;
        image = tex;
    }

    private IEnumerator StartLoad()
    {
        state = TransitionState.LOAD;
        aop = SceneManager.LoadSceneAsync(_scene);
        aop.allowSceneActivation = false;
        yield return aop;
    }
}
