﻿using UnityEngine;

public class ActionStruct
{
    public EIDType EntityType;
    public EActionStructType ActionType;
    public byte ActionID;
    public int SenderID;
    public int TargetID;
    public Vector3 TargetPos = Vector3.zero;

    /// <summary>
    /// This struct is used to denote an action taking place with a specific target
    /// </summary>
    /// <param name="etype">Entity type (player or monster)</param>
    /// <param name="type">Type of action</param>
    /// <param name="sid">Sender ID</param>
    /// <param name="tid">Target ID</param>
    public ActionStruct(EIDType etype, EActionStructType atype, byte aid, int sid, int tid)
    {
        EntityType = etype;
        ActionType = atype;
        ActionID = aid;
        SenderID = sid;
        TargetID = tid;
    }

    /// <summary>
    /// This struct is used to denote an action taking place at a specific position
    /// </summary>
    /// <param name="etype">Entity type (player or monster)</param>
    /// <param name="type">Type of action</param>
    /// <param name="sid">Sender ID</param>
    /// <param name="target">Target position</param>
    public ActionStruct(EIDType etype, EActionStructType type, byte aid, int sid, Vector3 target)
    {
        EntityType = etype;
        ActionType = type;
        ActionID = aid;
        SenderID = sid;
        TargetPos = target;
    }
}