﻿using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    private Transform trans;

    void Start()
    {
        trans = transform;
    }

    void LateUpdate()
    {
        trans.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
    }
}
