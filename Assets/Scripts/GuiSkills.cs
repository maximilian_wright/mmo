﻿using UnityEngine;
using Client;

public class GuiSkills : Gui
{
    public static KeyCode SKILL1 = KeyCode.Alpha1;
    public static KeyCode SKILL2 = KeyCode.Alpha2;
    public static KeyCode SKILL3 = KeyCode.Alpha3;
    public static KeyCode SKILL4 = KeyCode.Alpha4;

    public float cooldown1 = 3;
    public float cooldown2 = 4;
    public float cooldown3 = 7;
    public float cooldown4 = 5;

    public float[] cooldowns;
    public float[] cooldownTimeLeft;

    public GameObject tauntPrefab;

    private Animator animator;
    private ClientManager cliManager;
    private TargetHandler targetHandler;

    void Awake()
    {
        ID = 9;

        cooldowns = new float[] { cooldown1, cooldown2, cooldown3, cooldown4 };
        cooldownTimeLeft = new float[] { 0, 0, 0, 0 };
    }

    public override void Start()
    {
        animator = GetComponent<Animator>();
        cliManager = ClientManager.Instance;
        targetHandler = GetComponent<TargetHandler>();

        area = new Rect(Screen.width / 2 - WIDTH / 2, Screen.height - HEIGHT, WIDTH, HEIGHT);
    }

    void OnGUI()
    {
        area = GUILayout.Window(ID, area, OnWindow, "Skills");
        area.x = Mathf.Clamp(area.x, 0, Screen.width - area.width);
        area.y = Mathf.Clamp(area.y, 0, Screen.height - area.height);
    }

    public override void Update()
    {
        for (int i = 0; i < cooldownTimeLeft.Length; i++)
        {
            if (cooldownTimeLeft[i] > 0)
                cooldownTimeLeft[i] -= Time.deltaTime;
        }

        if (Input.GetKeyUp(SKILL1))
        {
            UseSkill(2);
        }
        if (Input.GetKeyUp(SKILL2))
        {
            UseSkill(3);
        }
        if (Input.GetKeyUp(SKILL3))
        {
            UseSkill(4);
        }
        if (Input.GetKeyUp(SKILL4))
        {
            UseSkill(5);
        }

        IsMouseOverWindow = IsMouseOver || IsMouseOverWindow;
    }

    void OnWindow(int id)
    {
        GUI.DragWindow(new Rect(0, 0, 10000, 20));

        GUI.enabled = (cooldownTimeLeft[0] <= 0);
        GUILayout.BeginHorizontal();

        //empowered strike
        if (GUILayout.Button(IsOnCooldown(2) ? cooldownTimeLeft[0].ToString("0.0") : "1", GUILayout.Width(40), GUILayout.Height(40)))
        {
            UseSkill(2);
        }

        GUI.enabled = (cooldownTimeLeft[1] <= 0);
        GUILayout.FlexibleSpace();

        //cleave
        if (GUILayout.Button(IsOnCooldown(3) ? cooldownTimeLeft[1].ToString("0.0") : "2", GUILayout.Width(40), GUILayout.Height(40)))
        {
            UseSkill(3);
        }

        GUI.enabled = (cooldownTimeLeft[2] <= 0);
        GUILayout.FlexibleSpace();

        //block
        if (GUILayout.Button(IsOnCooldown(4) ? cooldownTimeLeft[2].ToString("0.0") : "3", GUILayout.Width(40), GUILayout.Height(40)))
        {
            UseSkill(4);
        }

        GUI.enabled = (cooldownTimeLeft[3] <= 0);
        GUILayout.FlexibleSpace();

        //taunt
        if (GUILayout.Button(IsOnCooldown(5) ? cooldownTimeLeft[3].ToString("0.0") : "4", GUILayout.Width(40), GUILayout.Height(40)))
        {
            UseSkill(5);
        }
        GUILayout.EndHorizontal();
    }

    private void UseSkill(int skill)
    {
        skill -= 2;

        if ((skill != 0 || targetHandler.GetTargetID() != -1) && cooldownTimeLeft[skill] <= 0)
        {
            Debug.Log("used skill " + skill);

            switch (skill)
            {
                case 0://empowered strike
                    animator.SetBool("strike", true);
                    break;
                case 1://cleave
                    animator.SetBool("cleave", true);
                    break;
                case 2://block
                    break;
                case 3://taunt
                    if (tauntPrefab != null)
                        Instantiate(tauntPrefab, transform.position + Vector3.up, Quaternion.identity);
                    break;
            }

            cooldownTimeLeft[skill] += cooldowns[skill];

            for (int i = 0; i < cooldownTimeLeft.Length; i++)
            {
                if (cooldownTimeLeft[i] < 0.35f)
                    cooldownTimeLeft[i] += 0.35f;
            }

            cliManager.Send(new PacketPlayerAction(1, (byte)(skill + 2), targetHandler.GetTargetID()));
        }
    }

    private bool IsOnCooldown(int skill)
    {
        skill -= 2;

        if (skill >= 0 && skill < cooldownTimeLeft.Length)
        {
            return cooldownTimeLeft[skill] > 0;
        }

        return false;
    }
}
