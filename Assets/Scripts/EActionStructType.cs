﻿public enum EActionStructType
{
    SKILL = 1,
    USE_ITEM = 2,
    DROP_ITEM = 3,
    PICKUP_ITEM = 4
}
