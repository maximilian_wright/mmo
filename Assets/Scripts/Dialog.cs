﻿using UnityEngine;
using System.Collections;

public class Dialog : Gui
{
    public delegate void ClickEvent(int option);
    /// <summary>
    /// Event list to handle response buttons being clicked
    /// </summary>
    public event ClickEvent OnClick;

    public enum DialogType { PARTY, DUNGEON }
    public DialogType dialogType;

    public int width = 300;
    public int height = 100;
    public string title;
    public string message;
    public string[] buttons;

    private static int nextId = 20;

	public override void Start()
    {
        ID = nextId++;

        if (nextId == int.MaxValue)
            nextId = 20;

        area = new Rect(Screen.width / 2 - width / 2, Screen.height / 2 - height / 2, width, height);
	}

    public override void Update()
    {
        BringToFront();
    }

    void OnGUI()
    {
        area = GUILayout.Window(1, area, Window, title);
    }

    void Window(int id)
    {
        GUI.DragWindow(new Rect(0, 0, 10000, 20));
        
        GUILayout.Label(message);

        GUILayout.FlexibleSpace();

        GUILayout.BeginHorizontal();
        for (int i = 0; i < buttons.Length; i++)
            if (GUILayout.Button(buttons[i]))
            {
                OnClick(i);
            }
        GUILayout.EndHorizontal();
    }

    /// <summary>
    /// Helper function to initialize a Dialog to specific values
    /// </summary>
    /// <param name="t">Type of Dialog (Dungeon, Party)</param>
    /// <param name="tit">Title</param>
    /// <param name="msg">Message/Prompt</param>
    /// <param name="buts">List of buttons</param>
    public void Create(DialogType t, string tit, string msg, string[] buts)
    {
        dialogType = t;
        title = tit;
        message = msg;
        buttons = buts;
    }

    /// <summary>
    /// Closes the Dialog by destroying it
    /// </summary>
    public void Close()
    {
        Destroy(this);
    }
}
