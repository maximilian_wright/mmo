﻿using UnityEngine;
using System;
using System.Collections;
using System.Text;
using Client;

public class ClientGUI : MonoBehaviour
{
    private int width = 200;
    private int height = 200;

    private ReliableClient client;

    private Rect rect1;
    private Rect rect2;

    private string ip = "129.21.28.56";
    private string port = "8888";
    private string username = "";

    private string chat = "";
    private string message = "";

	void Start()
    {
        client = new ReliableClient();
        client.Receive += (sender, buffer, len) =>
        {
            Debug.Log("data: " + Encoding.UTF8.GetString(buffer));
            chat += Encoding.UTF8.GetString(buffer) + "\n";
        };
        rect1 = new Rect(Screen.width / 2 - width / 2, Screen.height / 2 - height / 2, width, height);
        rect2 = new Rect(0, 0, Screen.width, Screen.height);
    }

    void OnApplicationQuit()
    {
        client.Disconnect();
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, Screen.height / 2, 200, 25), "" + client.IsConnected);

        if (client.IsConnected)
            GUILayout.BeginArea(rect2);
        else
            GUILayout.BeginArea(rect1);

        GUILayout.BeginVertical();

        if (client.IsConnected)
        {
            GUI.enabled = false;
            chat = GUILayout.TextArea(chat, GUILayout.ExpandHeight(true));
            GUI.enabled = true;

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Disconnect", GUILayout.MaxHeight(50)))
            {
                client.Disconnect();
            }
            GUILayout.EndHorizontal();

            GUILayout.FlexibleSpace();

            GUILayout.BeginHorizontal();
            message = GUILayout.TextField(message);
            if (GUILayout.Button("Send", GUILayout.MaxWidth(100)))
            {
                Process(message);
                message = "";
            }
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("IP:");
            ip = GUILayout.TextField(ip, GUILayout.MaxWidth(100), GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Port:");
            port = GUILayout.TextField(port, GUILayout.MaxWidth(100), GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Name:");
            username = GUILayout.TextField(username, GUILayout.MaxWidth(100), GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();

            GUILayout.FlexibleSpace();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Connect", GUILayout.MaxHeight(50)))
            {
                client.Connect(ip, int.Parse(port));
                client.Send(new PacketLogin(username).ToByteArray());
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.EndVertical();

        GUILayout.EndArea();
    }

    private void Process(string message)
    {
        if (message[0] == '/')
        {
            string[] parts = message.Split(' ');
            string cmd = parts[0].Substring(1);
            Debug.Log("got cmd '" + cmd + "'");
            
            if (cmd == "move")
            {
                //client.Send(new PacketMove(float.Parse(parts[1]), float.Parse(parts[2]), float.Parse(parts[3]), float.Parse(parts[4])));
            }
            else
                chat += "Unknown command '" + cmd + "'\n";
        }
        else
        {
            client.Send(new PacketChatMessage(message).ToByteArray());
        }
    }
}
