﻿using UnityEngine;

public class Gui : MonoBehaviour
{
    public static bool IsMouseOverWindow { get; set; }

    public int ID { get; set; }

    public int WIDTH;
    public int HEIGHT;

    protected Rect area;

    public virtual bool IsMouseOver
    {
        get { return area.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)); }
    }

    public virtual void Start()
    {
        area = new Rect(0, 0, WIDTH, HEIGHT);
    }

    public virtual void Update()
    {

    }

    public virtual void OnEnable()
    {
        BringToFront();
    }

    public void BringToFront()
    {
        GUI.BringWindowToFront(ID);
    }
}