﻿using UnityEngine;

public class TauntScript : MonoBehaviour
{
    public float startScale = 0.1f;
    public float maxScale = 12f;
    public float scaleTime = 1f;
    public float timeFactor = 3;
    public float fadeSpeed = 1.1f;
    public float fadeSpeedDelta = 0.1f;

    private Transform trans;

    private Material mat;
    private Color color;

    private Vector3 startScaleVector;
    private Vector3 maxScaleVector;
    private float time = 0;

    void Start()
    {
        trans = transform;
        mat = GetComponent<MeshRenderer>().material;
        color = mat.color;

        startScaleVector = new Vector3(startScale, startScale, startScale);
        maxScaleVector = new Vector3(maxScale, maxScale, maxScale);
    }

	void Update()
    {
        time += Time.deltaTime;
        
        color.a -= fadeSpeed * Time.deltaTime;
        color.a = Mathf.Max(color.a, 0f);
        mat.color = color;

        trans.localScale = Tween(time / scaleTime, startScaleVector, maxScaleVector - startScaleVector, scaleTime);

        fadeSpeed += fadeSpeedDelta * Time.deltaTime;

        if (color.a == 0)
            Destroy(gameObject);
	}
    
    private Vector3 Tween(float time, Vector3 start, Vector3 delta, float duration)
    {
        time = Mathf.Pow(time, 1 / timeFactor);
        time /= duration;
        return -delta * time * (time - 2) + start;
    }
}
