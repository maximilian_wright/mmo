﻿using UnityEngine;
using System.Collections;

public class OLPlayerController : MonoBehaviour
{
    public bool canJump = true;
    public float jumpHeight = 2.0f;
    public float jumpinterval = 1.5f;
    private float nextjump = 1.2f;

    public float speed = 8.0f;
    private float moveAmount;
    public float smoothSpeed = 2.0f;
    //private float sensitivityX = 6.0f;

    public float gravity = 25;
    public float rotateSpeed = 8.0f;
    public float dampTime = 3.0f;

    private float horizontalSpeed;

    private float nextstep;
    public Transform chest;

    //private bool running = false;

    private Vector3 forward = Vector3.forward;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 right;
    //private bool canrun;
    private bool canjump;
    //private bool isjumping;

    public Transform shield;      //5 1.5 6 6 2 10 8 3
    public Transform weapon;      //target, chest, shield_1, sword, lhandpos, rhandpos, chestholder, chestholdersword, main cam
    public Transform leftHandPos;
    public Transform rightHandPos;
    public Transform chestPosShield;
    public Transform chestPosWeapon;

    public bool fightmodus;
    public bool weaponEquipped;
    public bool canattack;

    public Transform mycamera;

    void Update()
    {
        if (Input.GetKeyDown("f3"))
        {
            foreach(Transform child in transform.parent.transform)
            {
                child.gameObject.SetActive(true);
            }

            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().enabled = false;
            Camera debugCam = GameObject.FindGameObjectWithTag("DebugCamera").GetComponent<Camera>();
            debugCam.enabled = true;
            debugCam.transform.position = gameObject.transform.position;
            debugCam.transform.rotation = gameObject.transform.rotation;

            Cursor.lockState = CursorLockMode.Confined;

            gameObject.SetActive(false);
        }

        forward = Quaternion.Euler(0, mycamera.eulerAngles.y, 0) * Vector3.forward;
        right = new Vector3(forward.z, 0, -forward.x);

        CharacterController controller = GetComponent<CharacterController>();
        Animator animator = GetComponent<Animator>();

        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 targetDirection = (hor * right) + (ver * forward);

        Vector3 velocity = controller.velocity;
        float z = velocity.z;
        float x = velocity.x;
        Vector3 horizontalvelocity = new Vector3(x, 0, z);
        float horizontalspeed = horizontalvelocity.magnitude;
        Vector3 localmagnitude = transform.InverseTransformDirection(horizontalvelocity);

        if (fightmodus)
        {
            animator.SetFloat("hor", (localmagnitude.x), dampTime, 0.8f);
            animator.SetFloat("ver", (localmagnitude.z), dampTime, 0.8f);

            if (targetDirection != Vector3.zero)
            {
                Quaternion lookrotation2 = Quaternion.LookRotation(targetDirection, Vector3.up);
                lookrotation2.x = 0;
                lookrotation2.z = 0;
                transform.rotation = Quaternion.Lerp(transform.rotation, lookrotation2, Time.deltaTime * rotateSpeed);
            }
        }
        else
        {
            //canrun = true;

            if (targetDirection != Vector3.zero)
            {
                Quaternion lookrotation2 = Quaternion.LookRotation(targetDirection, Vector3.up);
                lookrotation2.x = 0;
                lookrotation2.z = 0;
                transform.rotation = Quaternion.Lerp(transform.rotation, lookrotation2, Time.deltaTime * rotateSpeed);
            }
        }

        Vector3 targetVelocity = targetDirection;
        targetVelocity *= speed;

        bool attackState = animator.GetCurrentAnimatorStateInfo(0).IsName("attacks");
        bool cleaveState = animator.GetCurrentAnimatorStateInfo(0).IsName("cleave");
        bool strikeState = animator.GetCurrentAnimatorStateInfo(0).IsName("strike");

        if (attackState) animator.SetBool("attack", false);
        if (cleaveState) animator.SetBool("cleave", false);
        if (strikeState) animator.SetBool("strike", false);

        if (controller.isGrounded)
        {
            animator.SetFloat("speed", horizontalspeed, dampTime, 0.2f);

            if (Input.GetButton("Jump") && Time.time > nextjump)
            {
                nextjump = Time.time + jumpinterval;
                moveDirection.y = jumpHeight;
                animator.SetBool("Jump", true);
                //isjumping = true;
            }
            else
            {
                animator.SetBool("Jump", false);
                //isjumping = false;
            }
        }
        else
        {
            moveDirection.y -= gravity * Time.deltaTime;
            nextjump = Time.time + jumpinterval;
        }

        moveDirection.z = targetVelocity.z;
        moveDirection.x = targetVelocity.x;
        controller.Move(moveDirection * Time.deltaTime);

        if (Input.GetButtonDown("Fire3"))
        {
            //weaponselect();
        }
        
        animator.SetBool("grounded", controller.isGrounded);
    }

    public void equip()
    {
        weapon.parent = rightHandPos;
        weapon.position = rightHandPos.position;
        weapon.rotation = rightHandPos.rotation;
        shield.parent = leftHandPos;
        shield.position = leftHandPos.position;
        shield.rotation = leftHandPos.rotation;
        fightmodus = true;
    }

    public void holster()
    {
        shield.parent = chestPosShield;
        shield.position = chestPosShield.position;
        shield.rotation = chestPosShield.rotation;
        fightmodus = false;
        weapon.parent = chestPosWeapon;
        weapon.position = chestPosWeapon.position;
        weapon.rotation = chestPosWeapon.rotation;
    }

    public void weaponselect()
    {
        Animator animator = GetComponent<Animator>();

        if (weaponEquipped)
        {
            animator.CrossFade("Holster", 0.15f, 0, 0);
            canattack = false;
            weaponEquipped = false;
        }
        else
        {
            animator.CrossFade("Equip", 0.15f, 0, 0);
            canattack = true;
            weaponEquipped = true;
        }
    }
}
