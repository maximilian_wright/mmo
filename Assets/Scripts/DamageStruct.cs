﻿using UnityEngine;

public class DamageStruct
{
    public EIDType Type;
    public int ID;
    public int Damage;
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="type">Damage for a player or monster</param>
    /// <param name="id">Entity's ID</param>
    /// <param name="dmg">Amount of damage taken</param>
    public DamageStruct(EIDType type, int id, int dmg)
    {
        Type = type;
        ID = id;
        Damage = dmg;
    }
}