﻿using UnityEngine;
using System.Collections;
using System;
using Client;

public class TargetHandler : MonoBehaviour
{
    public int currentHP;   //Amount of hp the entity /curently/ has
    public int fullHP;      //Amount of hp the entity /can/ have
    private bool inCombat;

    private GameObject targetObject;
    private int hadTarget = 0;
    [SerializeField]
    private float distanceToBreakTarget = 25;
    [SerializeField]
    private GameObject spinnerPrefab;
    private GameObject targetSpinner;

    private Hotbar hotbar;
    private CombatManager CombatMngr;

    private Animator animator;
    private OLPlayerController playerController;
    
	void Start()
    {
        targetObject = null;
        targetSpinner = Instantiate(spinnerPrefab);
        DontDestroyOnLoad(targetSpinner);

        hotbar = GetComponent<Hotbar>();
        CombatMngr = gameObject.GetComponent<CombatManager>();
        animator = gameObject.GetComponent<Animator>();
        playerController = gameObject.GetComponent<OLPlayerController>();
	}
	
	void Update()
    {
        if (!targetObject)
        {
            targetSpinner.SetActive(false);

            if (hadTarget == 1)
            {
                animator.CrossFade("Holster", 0.15f, 0, 0);
                playerController.canattack = false;
                playerController.weaponEquipped = false;
            }

            hadTarget = 0;
        }
        else
        {
            if (targetObject.tag == "Enemy")
                hadTarget = 1;
            else if (targetObject.tag == "NetworkPlayer")
                hadTarget = 2;
        }

        if (Input.GetMouseButtonDown(0) && !Gui.IsMouseOverWindow)
        {
            RaycastHit hit = new RaycastHit();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if(Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag != "Untagged")
                    Debug.Log(hit.transform.tag);

                if (hit.transform.tag == "NetworkPlayer")
                {
                    targetObject = hit.transform.gameObject;
                    targetSpinner.SetActive(true);
                }
                else if (hit.transform.tag == "Enemy")
                {
                    targetObject = hit.transform.gameObject;
                    targetSpinner.SetActive(true);

                    CombatMngr.SetCombatTarget(targetObject);

                    //update animator with draw weapons
                    if (!playerController.weaponEquipped)
                    {
                        animator.CrossFade("Equip", 0.15f, 0, 0);
                        playerController.canattack = false;
                        playerController.weaponEquipped = true;
                    }
                }
                else if (hit.transform.tag == "DungeonDoor")
                {
                    targetObject = hit.transform.gameObject;
                    targetSpinner.SetActive(true);

                    //if player is close enough to the door
                    if ((hit.transform.position - transform.position).magnitude < 4)
                    {
                        Dialog d = gameObject.AddComponent<Dialog>();
                        hotbar.AddDialog(d);

                        //player wasn't in a party
                        if (GetComponent<PlayerScript>().Party == -1)
                        {
                            d.Create(Dialog.DialogType.DUNGEON, "Dungeon",
                                "You must be in a party to enter a dungeon.",
                                new string[] { "OK" });

                            d.OnClick += (option) =>
                            {
                                switch (option)
                                {
                                    default:
                                        d.Close();
                                        break;
                                }
                            };
                        }

                        //player was in a party
                        else
                        {
                            d.Create(Dialog.DialogType.DUNGEON, "Dungeon",
                                "Would you like to enter the dungeon?",
                                new string[] { "Yes", "No" });

                            d.OnClick += (option) =>
                            {
                                switch (option)
                                {
                                    case 0:
                                        ClientManager.Instance.Send(new PacketZoneEnter(1));
                                        d.Close();
                                        break;
                                    default:
                                        d.Close();
                                        break;
                                }
                            };
                        }
                    }
                }
                else if (hit.transform.tag == "DungeonExit")
                {
                    targetObject = hit.transform.gameObject;
                    targetSpinner.SetActive(true);

                    //if player is close enough to the door
                    if ((hit.transform.position - transform.position).magnitude < 4)
                    {
                        Dialog d = gameObject.AddComponent<Dialog>();
                        hotbar.AddDialog(d);

                        d.Create(Dialog.DialogType.DUNGEON, "Dungeon",
                                "Would you like to leave the dungeon?",
                                new string[] { "Yes", "No" });

                        d.OnClick += (option) =>
                        {
                            switch (option)
                            {
                                case 0:
                                    ClientManager.Instance.Send(new PacketZoneEnter(0));
                                    d.Close();
                                    break;
                                default:
                                    d.Close();
                                    break;
                            }
                        };
                    }
                }
                else
                {
                    ClearTarget();
                    CombatMngr.SetCombatTarget(null);

                    if (playerController.weaponEquipped)
                    {

                        animator.CrossFade("Holster", 0.15f, 0, 0);
                        playerController.canattack = true;
                        playerController.weaponEquipped = false;
                    }
                }
            }
            else
            {
                targetObject = null;
                CombatMngr.SetCombatTarget(null);
                targetSpinner.SetActive(false);

                if (playerController.weaponEquipped)
                {
                    animator.CrossFade("Holster", 0.15f, 0, 0);
                    playerController.canattack = true;
                    playerController.weaponEquipped = false;
                }
            }
        }

        if (targetObject)
        {
            if(Vector3.Distance(transform.position, targetObject.transform.position) > distanceToBreakTarget)
            {
                ClearTarget();
            }
            else
            {
                targetSpinner.transform.position = new Vector3(
                    targetObject.transform.position.x,
                    targetObject.transform.position.y + 3.0f,
                    targetObject.transform.position.z
                    );
            }
        }
	}

    void OnGUI()
    {
        if (targetObject)
            GUI.TextArea(new Rect(Screen.width / 2 - 100, 0, 200, 25), "Target: " + targetObject.name);
    }

    public void ClearTarget()
    {
        targetObject = null;
        targetSpinner.SetActive(false);
    }

    public void Logout()
    {
        Destroy(targetSpinner);
    }

    public void ModifyHealth(int dmg)
    {
        currentHP += dmg;
    }

    public int GetTargetID()
    {
        if (targetObject)
        {
            if (targetObject.tag == "NetworkPlayer")
                return targetObject.GetComponent<PlayerScript>().GetPlayerID();
            else if (targetObject.tag == "Enemy")
                return targetObject.GetComponent<EnemyScript>().EnemyID;
        }

        return -1;
    }

    public int GetTargetPlayerID()
    {
        if (targetObject && targetObject.tag == "NetworkPlayer")
            return targetObject.GetComponent<PlayerScript>().GetPlayerID();

        return -1;
    }
}
