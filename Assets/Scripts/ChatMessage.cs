﻿public struct ChatMessage
{
    public enum EChatType { PLAYER, INFO }
    public EChatType Type;
    public int PlayerID;
    public string Name;
    public string Message;

    public ChatMessage(EChatType type, int pid, string name, string msg)
    {
        Type = type;
        PlayerID = pid;
        Name = name;
        Message = msg;
    }
}
