﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using Client;
using System.Text;

public class ClientManager
{
    private static ClientManager instance;

    private ReliableClient client;
    private bool loggedIn = false;

    //outgoing
    private Thread outThread;
    private SafeList<IPacket> outPackets;

    //incoming
    private SafeList<SpawnStruct> spawnList;
    private SafeList<MoveStruct> moveList;
    private SafeList<UpdateStruct> updateList;
    private SafeList<ActionStruct> actionList;
    private SafeList<DamageStruct> damageList;
    private SafeList<SimulateStruct> startSimList;
    private SafeList<int> stopSimList;
    private SafeList<int> deathList;
    private SafeList<DestroyStruct> destroyList;
    private SafeList<ChatMessage> chatList;
    private SafeList<InviteStruct> invList;
    private SafeList<MemberStruct> addPartyMembers;
    private SafeList<int> rmPartyMembers;
    public int CreateParty { get; set; }

    public int ChangeZone { get; set; }
    public bool DungeonClear { get; set; }
    public bool DungeonWipe { get; set; }

    private bool disconnected;

    private ClientManager()
    {
        client = new ReliableClient();
        client.Receive += ParseIncomingPacket;

        outPackets = new SafeList<IPacket>();
        spawnList = new SafeList<SpawnStruct>();
        moveList = new SafeList<MoveStruct>();
        updateList = new SafeList<UpdateStruct>();
        actionList = new SafeList<ActionStruct>();
        damageList = new SafeList<DamageStruct>();
        startSimList = new SafeList<SimulateStruct>();
        stopSimList = new SafeList<int>();
        deathList = new SafeList<int>();
        destroyList = new SafeList<DestroyStruct>();
        chatList = new SafeList<ChatMessage>();
        invList = new SafeList<InviteStruct>();
        addPartyMembers = new SafeList<MemberStruct>();
        rmPartyMembers = new SafeList<int>();
        CreateParty = -1;
        ChangeZone = -1;
        DungeonClear = false;
        DungeonWipe = false;
    }

    public static ClientManager Instance
    {
        get
        {
            if (instance == null)
                instance = new ClientManager();

            return instance;
        }
    }

    public static void DestroyInstance()
    {
        if (instance != null && instance.IsConnected)
            instance.Disconnect();
        
        instance = null;
    }

    public bool IsConnected
    {
        get { return client.IsConnected; }
    }

    public bool DisconnectedFromServer
    {
        get { return disconnected; }
    }

    public bool IsLoggedIn
    {
        get { return loggedIn; }
    }

    public SafeList<SpawnStruct> SpawnList
    {
        get { return spawnList; }
    }

    public SafeList<MoveStruct> MoveList
    {
        get { return moveList; }
    }

    public SafeList<UpdateStruct> UpdateList
    {
        get { return updateList; }
    }

    public SafeList<ActionStruct> ActionList
    {
        get { return actionList; }
    }

    public SafeList<DamageStruct> DamageList
    {
        get { return damageList; }
    }

    public SafeList<SimulateStruct> StartSimList
    {
        get { return startSimList; }
    }

    public SafeList<int> StopSimList
    {
        get { return stopSimList; }
    }

    public SafeList<int> DeathList
    {
        get { return deathList; }
    }

    public SafeList<DestroyStruct> DestroyList
    {
        get { return destroyList; }
    }

    public SafeList<ChatMessage> ChatList
    {
        get { return chatList; }
    }

    public SafeList<InviteStruct> InviteList
    {
        get { return invList; }
    }

    public SafeList<MemberStruct> AddPartyMembers
    {
        get { return addPartyMembers; }
    }

    public SafeList<int> RemovePartyMembers
    {
        get { return rmPartyMembers; }
    }
    
    /// <summary>
    /// Sets reliable client's ip and port and starts the outgoing packet thread
    /// </summary>
    /// <param name="ip">ip address</param>
    /// <param name="port">port number</param>
    public void Connect(string ip, int port)
    {
        if (!client.IsConnected)
        {
            Debug.Log("connecting to " + ip + ":" + port);
            client.Connect(ip, port);
            disconnected = false;
            outThread = new Thread(new ThreadStart(HandleOutgoing));
            outThread.Start();
        }
    }

    /// <summary>
    /// Stops the outgoing packet thread and disconnects reliable client
    /// </summary>
    public void Disconnect()
    {
        if (client.IsConnected)
        {
            outThread.Abort();
            client.Disconnect();
            disconnected = true;
            loggedIn = false;
            Debug.Log("ClientManager Disconnect finished. client connect: " + client.IsConnected);
        }
    }

    /// <summary>
    /// Called to remove all packets from outgoing buffer
    /// </summary>
    public void CancelSending()
    {
        client.CancelSending();
        Disconnect();
    }

    /// <summary>
    /// Adds packet to list of outgoing packets
    /// </summary>
    /// <param name="packet">packet to send</param>
    public void Send(IPacket packet)
    {
        outPackets.Add(packet);
    }

    /// <summary>
    /// Thread function to send outgoing packets
    /// </summary>
    private void HandleOutgoing()
    {
        while (client.IsConnected)
        {
            if (outPackets.Count > 0)
            {
                IPacket ip = outPackets[0];
                outPackets.RemoveAt(0);

                try
                {
                    client.Send(ip);
                }
                catch (SocketException se)
                {
                    Debug.Log(se.Message);
                    if (se.InnerException != null)
                        Debug.Log(se.InnerException);
                    Debug.Log(se.StackTrace);
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);
                    if (e.InnerException != null)
                        Debug.Log(e.InnerException);
                    Debug.Log(e.StackTrace);
                }
            }
        }

        Debug.Log("Outgoing thread finished");
    }

    #region Packet Parsing

    /// <summary>
    /// Called by ReliableClient to handling any incoming packets
    /// </summary>
    /// <param name="sender">Object calling this function (ReliableClient)</param>
    /// <param name="buffer">the packet as a byte[]</param>
    /// <param name="len">length of the packet</param>
    void ParseIncomingPacket(object sender, byte[] buffer, int len)
    {
        //Debug.Log("Recieved Packet of ID: " + buffer[0]);

        if (buffer[0] != 20 && buffer[0] != 9)
            Debug.Log("Recieved Packet: #" + buffer[0]);

        switch (buffer[0])
        {
            case Packet.LOGIN_SUCCESS:
                Debug.Log("login successful");
                OnPlayerLogin(buffer);
                break;

            case Packet.LOGIN_FAIL:
                Debug.Log("Login failed");
                break;

            case Packet.TRACK_PLAYER:
                OnTrackPlayer(buffer);
                break;

            case Packet.UNTRACK_PLAYER:
                OnUntrackPlayer(buffer);
                break;

            case Packet.TRACK_ENEMY:
                OnTrackEnemy(buffer);
                break;

            case Packet.CHAT_MSG:
                OnChat(buffer);
                break;

            case Packet.UPDATE_PLAYER:
                OnUpdatePlayer(buffer);
                break;

            case Packet.UPDATE_ENEMY:
                OnUpdateEnemy(buffer);
                break;

            case Packet.MOVE_PLAYER:
                OnPlayerMove(buffer);
                break;

            case Packet.MOVE_ENEMY:
                OnEnemyMove(buffer);
                break;

            case Packet.CREATE_PARTY:
                OnCreateParty(buffer);
                break;

            case Packet.PARTY_INVITE:
                OnPartyInvite(buffer);
                break;

            case Packet.PARTY_JOIN:
                OnPartyJoin(buffer);
                break;

            case Packet.PARTY_LEAVE:
                OnPartyLeave(buffer);
                break;

            case Packet.ZONE_ENTER:
                OnZoneEnter(buffer);
                break;

            case Packet.ACTION_PLAYER:
                OnActionPlayer(buffer);
                break;

            case Packet.ACTION_ENEMY:
                OnActionEnemy(buffer);
                break;

            case Packet.DAMAGE_PLAYER:
                OnDamagePlayer(buffer);
                break;

            case Packet.DAMAGE_ENEMY:
                OnDamageEnemy(buffer);
                break;

            case Packet.START_SIM_ENEMY:
                OnStartSimulateEnemy(buffer);
                break;

            case Packet.STOP_SIM_ENEMY:
                OnStopSimulateEnemy(buffer);
                break;

            case Packet.KILL_ENEMY:
                OnKillEnemy(buffer);
                break;

            case Packet.KILL_PLAYER:
                OnKillPlayer(buffer);
                break;

            case Packet.DUNGEON_CLEAR:
                OnDungeonClear();
                break;

            case Packet.DUNGEON_WIPE:
                OnDungeonWipe();
                break;

            case Packet.DISCONNECT:
                OnDisconnect(buffer);
                break;

            default:
                Debug.Log("Got unknown packet id: " + buffer[0]);
                break;
        }
    }

    /// <summary>
    /// Called when a player logs into the server and receives a login success
    /// </summary>
    /// <param name="buffer">The login success packet data</param>
    private void OnPlayerLogin(byte[] buffer)
    {
        int uid = BitConverter.ToInt32(buffer, 4);

        loggedIn = true;
        SpawnStruct ss = new SpawnStruct(ESpawnStructType.PLAYER, uid, Vector3.up);
        spawnList.Add(ss);
    }

    /// <summary>
    /// Called when another player joins this player's zone
    /// </summary>
    /// <param name="buffer">Other player's packet data</param>
    private void OnTrackPlayer(byte[] buffer)
    {
        short len = BitConverter.ToInt16(buffer, 2);
        int playerID = BitConverter.ToInt32(buffer, 4);
        //int partyID = BitConverter.ToInt32(buffer, 8);

        int nullIndex = buffer.Length;

        for (int i = 12; i < buffer.Length; i++)
            if (buffer[i] == '\0')
            {
                nullIndex = i;
                break;
            }

        int nameLength = nullIndex - 12;
        Debug.Log("name length: " + nameLength);
        string name = Encoding.UTF8.GetString(buffer, 12, nameLength);
        Debug.Log("name: " + name);
        int index = nullIndex + 1;
        
        float xNet = BitConverter.ToSingle(buffer, index);
        index += 4;
        float yNet = BitConverter.ToSingle(buffer, index);
        index += 4;
        float zNet = BitConverter.ToSingle(buffer, index);
        index += 4;
        float rNet = BitConverter.ToSingle(buffer, index);
        index += 4;
        //float vNet = BitConverter.ToSingle(buffer, index);
        index += 4;

        /*
            calc number of stats in this update packet.
            (numBytes - player uid size) / 5 bytes per stat
        */
        int numStats = (len - sizeof(float) * 4 - nameLength - sizeof(int) * 2 - 4) / 5;

        SortedList<EStat, int> values = new SortedList<EStat, int>();

        for (int i = 0; i < numStats; i++)
        {
            values.Add((EStat)buffer[index], BitConverter.ToInt32(buffer, index + 1));
            index += 5;
        }

        //create structs
        SpawnStruct newPlayer = new PlayerStruct(ESpawnStructType.PLAYER_OTHER, playerID,
            new Vector3(xNet, yNet, zNet), Quaternion.Euler(0, rNet, 0), name);
        spawnList.Add(newPlayer);
        UpdateStruct us = new UpdateStruct(EUpdateStructType.PLAYER, playerID, values);
        updateList.Add(us);
    }

    /// <summary>
    /// Called when another player leaves this player's zone
    /// </summary>
    /// <param name="buffer">The leaving player's packet data</param>
    private void OnUntrackPlayer(byte[] buffer)
    {
        int uID = BitConverter.ToInt32(buffer, 4);

        Debug.Log("destroy player id: " + uID);

        DestroyStruct ds = new DestroyStruct(EDestroyStructType.PLAYER, uID);
        destroyList.Add(ds);
    }

    /// <summary>
    /// Called when an enemy spawns in the same zone
    /// </summary>
    /// <param name="buffer">enemy data packet</param>
    private void OnTrackEnemy(byte[] buffer)
    {
        int eid = BitConverter.ToInt32(buffer, 4);
        byte type = buffer[8];
        int index = 9;

        float x = BitConverter.ToSingle(buffer, index);
        index += 4;
        float y = BitConverter.ToSingle(buffer, index);
        index += 4;
        float z = BitConverter.ToSingle(buffer, index);
        index += 4;
        //float r = BitConverter.ToSingle(buffer, index);
        index += 4;
        //float v = BitConverter.ToSingle(buffer, index);
        index += 4;

        SortedList<EStat, int> values = new SortedList<EStat, int>();

        index++;
        int hp = BitConverter.ToInt32(buffer, index);
        index += 4;
        values.Add(EStat.HEALTH, hp);
        index++;
        int blocking = buffer[index];
        values.Add(EStat.BLOCKING, blocking);

        spawnList.Add(new EnemyStruct(ESpawnStructType.MONSTER, eid, new Vector3(x, y, z), type));
        UpdateStruct us = new UpdateStruct(EUpdateStructType.MONSTER, eid, values);
        updateList.Add(us);
    }

    /// <summary>
    /// Called when server sends stat updates for a player
    /// </summary>
    /// <param name="buffer">player stat update packet</param>
    private void OnUpdatePlayer(byte[] buffer)
    {
        short len = BitConverter.ToInt16(buffer, 2);
        int uid = BitConverter.ToInt32(buffer, 4);

        /*
            calc number of stats in this update packet.
            (numBytes - player uid size) / 5 bytes per stat
        */
        int numStats = (len - 4) / 5;
        int index = 8;

        SortedList<EStat, int> values = new SortedList<EStat, int>();

        for (int i = 0; i < numStats; i++)
        {
            values.Add((EStat)buffer[index], BitConverter.ToInt32(buffer, index + 1));
            index += 5;
        }

        UpdateStruct us = new UpdateStruct(EUpdateStructType.PLAYER, uid, values);
        updateList.Add(us);
    }

    /// <summary>
    /// Called to update the health of an enemy
    /// </summary>
    /// <param name="buffer">enemy stat packet</param>
    private void OnUpdateEnemy(byte[] buffer)
    {
        short len = BitConverter.ToInt16(buffer, 2);
        int uid = BitConverter.ToInt32(buffer, 4);

        /*
            calc number of stats in this update packet.
            (numBytes - player uid size) / 5 bytes per stat
        */
        int numStats = (len - 4) / 5;
        int index = 8;

        SortedList<EStat, int> values = new SortedList<EStat, int>();

        for (int i = 0; i < numStats; i++)
        {
            values.Add((EStat)buffer[index], BitConverter.ToInt32(buffer, index + 1));
            index += 5;
        }

        UpdateStruct us = new UpdateStruct(EUpdateStructType.MONSTER, uid, values);
        updateList.Add(us);
    }

    /// <summary>
    /// Called when a different player moves
    /// </summary>
    /// <param name="buffer">Player's new position data</param>
    private void OnPlayerMove(byte[] buffer)
    {
        int uID = BitConverter.ToInt32(buffer, 4);

        float x = BitConverter.ToSingle(buffer, 8);
        float y = BitConverter.ToSingle(buffer, 12);
        float z = BitConverter.ToSingle(buffer, 16);
        float r = BitConverter.ToSingle(buffer, 20);
        //float v = BitConverter.ToSingle(buffer, 24);

        MoveStruct ms = new MoveStruct(EMoveStructType.PLAYER, uID, new Vector3(x, y, z), Quaternion.Euler(0, r, 0));
        moveList.Add(ms);
    }

    /// <summary>
    /// Called when an enemy moves
    /// </summary>
    /// <param name="buffer">Enemy move data</param>
    private void OnEnemyMove(byte[] buffer)
    {
        int enemyID = BitConverter.ToInt32(buffer, 4);

        float x = BitConverter.ToSingle(buffer, 8);
        float y = BitConverter.ToSingle(buffer, 12);
        float z = BitConverter.ToSingle(buffer, 16);
        float r = BitConverter.ToSingle(buffer, 20);
        //float v = BitConverter.ToSingle(buffer, 24);

        MoveStruct ms = new MoveStruct(EMoveStructType.MONSTER, enemyID, new Vector3(x, y, z), Quaternion.Euler(0, r, 0));
        moveList.Add(ms);
    }

    /// <summary>
    /// Called when a chat message is received
    /// </summary>
    /// <param name="buffer">chat message packet</param>
    private void OnChat(byte[] buffer)
    {
        int pid = BitConverter.ToInt32(buffer, 4);

        Debug.Log("got chat message from player: " + pid);

        int nullIndex = buffer.Length;

        for (int i = 8; i < buffer.Length; i++)
            if (buffer[i] == '\0')
            {
                nullIndex = i;
                break;
            }

        string pname = Encoding.UTF8.GetString(buffer, 8, nullIndex - 8);
        int index = nullIndex + 1;

        for (int i = index; i < buffer.Length; i++)
            if (buffer[i] == '\0')
            {
                nullIndex = i;
                break;
            }

        string msg = Encoding.UTF8.GetString(buffer, index, nullIndex - index);

        chatList.Add(new ChatMessage(ChatMessage.EChatType.PLAYER, pid, pname, msg));
    }

    /// <summary>
    /// Called when player successfully creates a party
    /// </summary>
    /// <param name="buffer">party data packet</param>
    private void OnCreateParty(byte[] buffer)
    {
        int partyID = BitConverter.ToInt32(buffer, 4);
        Debug.Log("creating party id " + partyID);

        CreateParty = partyID;
    }
    
    /// <summary>
    /// Called when receiving another player's party invite
    /// </summary>
    /// <param name="buffer">invite packet data</param>
    private void OnPartyInvite(byte[] buffer)
    {
        int partyID = BitConverter.ToInt32(buffer, 4);
        int senderID = BitConverter.ToInt32(buffer, 8);

        int nullIndex = buffer.Length;

        for (int i = 12; i < buffer.Length; i++)
            if (buffer[i] == '\0')
            {
                nullIndex = i;
                break;
            }

        string name = Encoding.UTF8.GetString(buffer, 12, nullIndex - 12);
        Debug.Log("got party invite from " + senderID + ":" + name + " for party id " + partyID);

        invList.Add(new InviteStruct(partyID, senderID, name));
    }

    /// <summary>
    /// Called when the player successfully joins a party
    /// </summary>
    /// <param name="buffer">joined party info</param>
    private void OnPartyJoin(byte[] buffer)
    {
        int playerid = BitConverter.ToInt32(buffer, 4);

        int nullIndex = buffer.Length;

        for (int i = 8; i < buffer.Length; i++)
            if (buffer[i] == '\0')
            {
                nullIndex = i;
                break;
            }

        string name = Encoding.UTF8.GetString(buffer, 8, nullIndex - 8);

        addPartyMembers.Add(new MemberStruct(playerid, name));
    }

    /// <summary>
    /// Called when a player leaves (and/or is kicked) from the same party
    /// </summary>
    /// <param name="buffer">The leaving player data</param>
    private void OnPartyLeave(byte[] buffer)
    {
        Debug.Log("Left party...");

        int leaveID = BitConverter.ToInt32(buffer, 4);
        rmPartyMembers.Add(leaveID);
    }

    /// <summary>
    /// Called when the server tells this player to change zones
    /// </summary>
    /// <param name="buffer">New zone packet data</param>
    private void OnZoneEnter(byte[] buffer)
    {
        int newZone = BitConverter.ToInt32(buffer, 4);
        Debug.Log("Entering zone " + newZone);

        ChangeZone = newZone;
    }

    /// <summary>
    /// Called when the server broadcasts an action a player did
    /// </summary>
    /// <param name="buffer">action data packet</param>
    private void OnActionPlayer(byte[] buffer)
    {
        int pid = BitConverter.ToInt32(buffer, 4);
        byte actID = buffer[8];
        int index = 9;
        ActionStruct actStruct = null;

        Debug.Log(pid + " , " + actID);

        switch (actID)
        {
            case 1: //skill
                byte skillid = buffer[index++];
                int targetid = BitConverter.ToInt32(buffer, index);
                actStruct = new ActionStruct(EIDType.PLAYER, (EActionStructType)actID, skillid, pid, targetid);
                break;
            case 2: //use item
                break;
            case 3: //drop item
                break;
            case 4: //pickup item
                break;
            default:
                Debug.Log("Unknown action id " + actID);
                break;
        }

        actionList.Add(actStruct);
    }

    /// <summary>
    /// Called when the server broadcasts an action an enemy did
    /// </summary>
    /// <param name="buffer">enemy action data packet</param>
    private void OnActionEnemy(byte[] buffer)
    {
        Debug.Log("Recieved Enemy Action Packet");

        int enemyID = BitConverter.ToInt32(buffer, 4);
        byte actID = buffer[8];
        int index = 9;
        ActionStruct actStruct = null;

        switch (actID)
        {
            case 1: //auto attack
                byte skillID = buffer[index++];
                int targetid = BitConverter.ToInt32(buffer, index);
                actStruct = new ActionStruct(EIDType.MONSTER, (EActionStructType)actID, skillID, enemyID, targetid);
                break;
            default:
                Debug.Log("Unknown action id " + actID);
                break;
        }

        actionList.Add(actStruct);
    }

    /// <summary>
    /// Called when a player takes damage
    /// </summary>
    /// <param name="buffer">player damage packet</param>
    private void OnDamagePlayer(byte[] buffer)
    {
        int id = BitConverter.ToInt32(buffer, 4);
        int damage = BitConverter.ToInt32(buffer, 8);

        damageList.Add(new DamageStruct(EIDType.PLAYER, id, damage));
    }

    /// <summary>
    /// Called when an enemy takes damage
    /// </summary>
    /// <param name="buffer">enemy damage packet</param>
    private void OnDamageEnemy(byte[] buffer)
    {
        int id = BitConverter.ToInt32(buffer, 4);
        int damage = BitConverter.ToInt32(buffer, 8);

        damageList.Add(new DamageStruct(EIDType.MONSTER, id, damage));
    }

    /// <summary>
    /// Called when the server designates this client to control an enemy
    /// </summary>
    /// <param name="buffer">enemy to control</param>
    private void OnStartSimulateEnemy(byte[] buffer)
    {
        int eid = BitConverter.ToInt32(buffer, 4);
        float x = BitConverter.ToSingle(buffer, 8);
        float y = BitConverter.ToSingle(buffer, 12);
        float z = BitConverter.ToSingle(buffer, 16);
        float r = BitConverter.ToSingle(buffer, 20);
        float v = BitConverter.ToSingle(buffer, 24);
        int tid = BitConverter.ToInt32(buffer, 28);

        SimulateStruct ss = new SimulateStruct(eid, new Vector3(x, y, z), Quaternion.Euler(0, r, 0), v, tid);
        startSimList.Add(ss);
    }

    /// <summary>
    /// Called when the server designates this client to stop controlling an enemy
    /// </summary>
    /// <param name="buffer">enemy to stop controlling</param>
    private void OnStopSimulateEnemy(byte[] buffer)
    {
        int eid = BitConverter.ToInt32(buffer, 4);
        stopSimList.Add(eid);
    }

    /// <summary>
    /// Called when an enemy is killed on the server
    /// </summary>
    /// <param name="buffer">enemy that died</param>
    private void OnKillEnemy(byte[] buffer)
    {
        int eid = BitConverter.ToInt32(buffer, 4);
        destroyList.Add(new DestroyStruct(EDestroyStructType.MONSTER, eid));
    }

    private void OnKillPlayer(byte[] buffer)
    {
        int pid = BitConverter.ToInt32(buffer, 4);
        deathList.Add(pid);
    }

    private void OnDungeonClear()
    {
        DungeonClear = true;
    }

    private void OnDungeonWipe()
    {
        DungeonWipe = true;
    }

    /// <summary>
    /// Called when the server shuts down
    /// </summary>
    /// <param name="buffer"></param>
    private void OnDisconnect(byte[] buffer)
    {
        Debug.Log("Disconnected from server...");
        disconnected = true;
        loggedIn = false;
    }

    #endregion
}
