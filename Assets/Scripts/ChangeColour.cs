﻿using UnityEngine;
using System.Collections;

public class ChangeColour : MonoBehaviour {

    public Color ObjectColor;

    private Material materialColored;

	// Use this for initialization
	void Start () {
	    materialColored = new Material(Shader.Find("Diffuse"));
        materialColored.color = ObjectColor;
        this.GetComponent<Renderer>().material = materialColored;
	}
}
