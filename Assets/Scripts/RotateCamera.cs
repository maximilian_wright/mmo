﻿using UnityEngine;
using System.Collections;

public class RotateCamera : MonoBehaviour
{
    public Transform target;
    public float distFromTarget;
    public float heightMod;

    public float rotateSpeed;
    public float pitchSpeed;

    public float yMinLimit, yMaxLimit;

    public float zoomSpeed;

    private float x, y = 0.0f;
    
    void Start()
    {
        if (transform.parent != null)
        {
            GameObject me = transform.parent.GetChild(0).gameObject;
            transform.position = me.transform.position + (me.transform.forward * -5);

            transform.forward = Vector3.ClampMagnitude((target.position - transform.position), 1);
        }

        Rigidbody objRB = GetComponent<Rigidbody>();

        if (objRB) { objRB.freezeRotation = true; }
    }
    
    void LateUpdate()
    {
        if (target)
        {
            if (Input.GetMouseButton(1))
            {
                x += (Input.GetAxis("Mouse X") * rotateSpeed * 0.02f);
                y -= (Input.GetAxis("Mouse Y") * pitchSpeed * 0.02f);
            }

            distFromTarget += -(Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime) * zoomSpeed * Mathf.Abs((float)distFromTarget);

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            Quaternion rotation = Quaternion.Euler(y, x, 0.0f);
            Vector3 position = rotation * new Vector3(0.0f, heightMod, -distFromTarget) + target.position;

            transform.rotation = rotation;
            transform.position = position;
        }
    }

    float ClampAngle(float y, float yMin, float yMax)
    {
        if (y < -360) y += 360;
        if (y > 360) y -= 360;
        return Mathf.Clamp(y, yMin, yMax);
    }
}
