﻿using UnityEngine;
using System.Collections;
using Client;

public class CombatManager : MonoBehaviour
{
    private ClientManager cliManager;

    private GameObject targetObject;
    private float autoAttackCooldownLeft = 0;

    [SerializeField]
    private float autoAttackWaitTime = 1.5f;
    [SerializeField]
    private float autoAttackRange = 1.0f;

    private Animator animator;
    
    void Start()
    {
        cliManager = ClientManager.Instance;
        animator = gameObject.GetComponent<Animator>();
    }
    
    void Update()
    {
        HandleAutoAttacks();
    }

    public void SetCombatTarget(GameObject tar)
    {
        targetObject = tar;
    }

    void HandleAutoAttacks()
    {
        if (targetObject)
        {
            if (autoAttackCooldownLeft <= 0.0f)
            {
                if (Vector3.Distance(transform.position, targetObject.transform.position) < autoAttackRange)
                {
                    if (Vector3.Angle(transform.forward, (targetObject.transform.position - transform.position)) < 60.0f)
                    {
                        cliManager.Send(new PacketPlayerAction(1, 1, targetObject.GetComponent<EnemyScript>().EnemyID));
                        Debug.Log("Sent AA message to Server");

                        autoAttackCooldownLeft = autoAttackWaitTime;

                        animator.SetBool("attack", true);
                    }
                }
            }
        }

        if (autoAttackCooldownLeft > 0)
        {
            autoAttackCooldownLeft -= Time.deltaTime;
        }
    }
}
