title Fix Unity

cd %APPDATA%\Roaming\Unity\Packages
mkdir node_modules
cd node_modules
mkdir unityeditor-cloud-hub
mkdir unity-editor-home

copy "C:\Program Files\Unity\Editor\Data\Resources\Packages\unityeditor-cloud-hub-0.0.1.tgz" "."
copy "C:\Program Files\Unity\Editor\Data\Resources\Packages\unity-editor-home-0.0.7.tgz" "."

7z x unityeditor-cloud-hub-0.0.1.tgz
7z x unityeditor-cloud-hub-0.0.1.tar -ounityeditor
copy "unityeditor\package\*" "unityeditor-cloud-hub\"

7z x unityeditor-cloud-home-0.0.7.tgz
7z x unityeditor-cloud-home-0.0.7.tar -ounityeditorhome
copy "unityeditorhome\package\*" "unity-editor-home\"