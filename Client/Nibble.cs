﻿using System;

namespace Client
{
    public class Nibble
    {
        public byte ID;
        public byte PID;

        public Nibble(byte id, byte pid)
        {
            ID = id;
            PID = pid;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Nibble))
                return false;

            return ((Nibble)obj) == this;
        }

        public override string ToString()
        {
            return "Nibble[ID=" + ID + ", PID=" + PID + "]";
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + ID.GetHashCode();
                hash = hash * 23 + PID.GetHashCode();
                return hash;
            }
        }

        public static bool operator==(Nibble n1, Nibble n2) => n1.ID == n2.ID && n1.PID == n2.PID;
        public static bool operator!=(Nibble n1, Nibble n2) => n1.ID != n2.ID || n1.PID != n2.PID;
    }
}
