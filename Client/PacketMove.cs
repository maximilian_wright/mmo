﻿using System;

namespace Client
{
    public class PacketMovePlayer : Packet
    {
        public int UID { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }
        public float Rot { get; private set; }
        public float VelocityMagnitude { get; private set; }

        public PacketMovePlayer(int uid, float x, float y, float z, float r, float vMag) : base(MOVE_PLAYER)
        {
            UID = uid;
            X = x;
            Y = y;
            Z = z;
            Rot = r;
            VelocityMagnitude = vMag;
        }

        public override byte[] ToByteArray()
        {
            byte[] data = new byte[4 + 4 + sizeof(float) * 5];

            data[0] = ID;
            data[1] = PID;

            byte[] len = BitConverter.GetBytes(sizeof(float) * 5);
            byte[] uid = BitConverter.GetBytes(UID);
            byte[] x = BitConverter.GetBytes(X);
            byte[] y = BitConverter.GetBytes(Y);
            byte[] z = BitConverter.GetBytes(Z);
            byte[] r = BitConverter.GetBytes(Rot);
            byte[] vMag = BitConverter.GetBytes(VelocityMagnitude);

            Array.Copy(len, 0, data, 2, len.Length);
            Array.Copy(uid, 0, data, 4, uid.Length);
            Array.Copy(x, 0, data, 8, x.Length);
            Array.Copy(y, 0, data, 8 + sizeof(float), y.Length);
            Array.Copy(z, 0, data, 8 + sizeof(float) * 2, z.Length);
            Array.Copy(r, 0, data, 8 + sizeof(float) * 3, r.Length);
            Array.Copy(vMag, 0, data, 8 + sizeof(float) * 4, vMag.Length);

            return data;
        }
    }

    public class PacketMoveEnemy : Packet
    {
        public int UID { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }
        public float Rot { get; private set; }
        public float VelocityMagnitude { get; private set; }

        public PacketMoveEnemy(int uid, float x, float y, float z, float r, float vMag) : base(MOVE_ENEMY)
        {
            UID = uid;
            X = x;
            Y = y;
            Z = z;
            Rot = r;
            VelocityMagnitude = vMag;
        }

        public override byte[] ToByteArray()
        {
            byte[] data = new byte[4 + 4 + sizeof(float) * 5];

            data[0] = ID;
            data[1] = PID;

            byte[] len = BitConverter.GetBytes(sizeof(float) * 5);
            byte[] uid = BitConverter.GetBytes(UID);
            byte[] x = BitConverter.GetBytes(X);
            byte[] y = BitConverter.GetBytes(Y);
            byte[] z = BitConverter.GetBytes(Z);
            byte[] r = BitConverter.GetBytes(Rot);
            byte[] vMag = BitConverter.GetBytes(VelocityMagnitude);

            Array.Copy(len, 0, data, 2, len.Length);
            Array.Copy(uid, 0, data, 4, uid.Length);
            Array.Copy(x, 0, data, 8, x.Length);
            Array.Copy(y, 0, data, 8 + sizeof(float), y.Length);
            Array.Copy(z, 0, data, 8 + sizeof(float) * 2, z.Length);
            Array.Copy(r, 0, data, 8 + sizeof(float) * 3, r.Length);
            Array.Copy(vMag, 0, data, 8 + sizeof(float) * 4, vMag.Length);

            return data;
        }
    }
}
