﻿using System;
using System.Text;

namespace Client
{
    public class PacketGenericAck : Packet
    {
        public byte ACK_CID { get; private set; }
        public byte ACK_PID { get; private set; }

        public PacketGenericAck(byte ack_cid, byte ack_pid) : base(GENERIC_ACK)
        {
            ACK_CID = ack_cid;
            ACK_PID = ack_pid;
        }

        public override byte[] ToByteArray()
        {
            byte[] bytes = new byte[4];
            bytes[0] = ID;
            bytes[1] = ACK_PID;
            bytes[2] = ACK_CID;
            bytes[3] = 0x0;

            return bytes;
        }
    }
}
