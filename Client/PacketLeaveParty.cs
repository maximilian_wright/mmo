﻿using System;

namespace Client
{
    public class PacketLeaveParty : Packet
    {
        public int PlayerID;

        public PacketLeaveParty(int pid) : base(PARTY_LEAVE, true)
        {
            PlayerID = pid;
        }

        public override byte[] ToByteArray()
        {
            byte[] bytes = new byte[4 + sizeof(int)];
            bytes[0] = ID;
            bytes[1] = PID;

            byte[] len = BitConverter.GetBytes(4);
            Array.Copy(len, 0, bytes, 2, len.Length);

            byte[] player = BitConverter.GetBytes(PlayerID);
            Array.Copy(player, 0, bytes, 4, player.Length);

            return bytes;
        }
    }
}
