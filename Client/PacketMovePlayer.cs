﻿using System;
using System.Text;

namespace Client
{
    class PacketMovePlayer : Packet
    {
        private float posX;
        private float posY;
        private float posZ;
        private float rotY;
        private float vMag;

        public PacketMovePlayer(float x, float y, float z, float r, float vm) : base(MOVE_PLAYER)
        {
            posX = x;
            posY = y;
            posZ = z;
            rotY = r;
            vMag = vm;
        }

        public override byte[] ToByteArray()
        {
            byte[] bytes = new byte[1 + 2 + 160];
            bytes[0] = ID;

            byte[] lenBytes = BitConverter.GetBytes((short)160);
            Array.Copy(lenBytes, 0, bytes, 1, lenBytes.Length);


            byte[] posXBytes = BitConverter.GetBytes(posX);
            Array.Copy(posXBytes, 0, bytes, 3 + (32 * 0), posXBytes.Length);

            byte[] posYBytes = BitConverter.GetBytes(posY);
            Array.Copy(posYBytes, 0, bytes, 3 + (32 * 1), posYBytes.Length);

            byte[] posZBytes = BitConverter.GetBytes(posZ);
            Array.Copy(posZBytes, 0, bytes, 3 + (32 * 2), posZBytes.Length);

            byte[] rotYBytes = BitConverter.GetBytes(rotY);
            Array.Copy(rotYBytes, 0, bytes, 3 + (32 * 3), rotYBytes.Length);

            byte[] vMagBytes = BitConverter.GetBytes(vMag);
            Array.Copy(vMagBytes, 0, bytes, 3 + (32 * 4), vMagBytes.Length);


            return bytes;
        }

    }
}
