﻿using System;
using System.Text;

namespace Client
{
    public class PacketLogin : Packet
    {
        private string name;

        public PacketLogin(string newName) : base(LOGIN, true)
        {
            name = newName;
        }

        public override byte[] ToByteArray()
        {
            byte[] bytes = new byte[2 + 2 + name.Length];
            bytes[0] = ID;
            bytes[1] = PID;

            byte[] lenBytes = BitConverter.GetBytes((short)name.Length);
            Array.Copy(lenBytes, 0, bytes, 2, lenBytes.Length);

            byte[] nameBytes = Encoding.UTF8.GetBytes(name);
            Array.Copy(nameBytes, 0, bytes, 4, nameBytes.Length);

            return bytes;
        }
    }
}
