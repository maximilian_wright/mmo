﻿using System;

namespace Client
{
    public class PacketPlayerAction : Packet
    {
        private byte ActID;
        private byte SkillID;
        private int TarID; //enemy id, item id
        private float X;
        private float Y;
        private float Z;

        public PacketPlayerAction(byte actionID, byte skillID, int targetID) : base(ACTION_PLAYER, true)
        {
            ActID = actionID;
            SkillID = skillID;
            TarID = targetID;
        }

        public PacketPlayerAction(byte actionID, float x, float y, float z) : base(ACTION_PLAYER, true)
        {
            ActID = actionID;
            X = x;
            Y = y;
            Z = z;
        }

        public override byte[] ToByteArray()
        {

            switch (ActID)
            {
                case 1: return CreateSkill();
                case 2: return CreateUse();
                case 3: return CreateDrop();
                case 4: return CreatePickup();
            }

            return null;
        }

        private byte[] CreateSkill()
        {
            byte[] bytes = new byte[HEADER_SIZE + sizeof(byte) * 2 + sizeof(int)];

            bytes[0] = ID;
            bytes[1] = PID;
            
            byte[] lenBytes = BitConverter.GetBytes(bytes.Length - HEADER_SIZE);
            Array.Copy(lenBytes, 0, bytes, 2, lenBytes.Length);

            bytes[4] = ActID;
            bytes[5] = SkillID;

            byte[] targetID = BitConverter.GetBytes(TarID);
            Array.Copy(targetID, 0, bytes, 6, targetID.Length);

            return bytes;
        }

        private byte[] CreateUse()
        {
            return null;
        }

        private byte[] CreateDrop()
        {
            return null;
        }

        private byte[] CreatePickup()
        {
            return null;
        }
    }

    public class PacketEnemyAction : Packet
    {
        private int EnemyID;
        private byte ActID;
        private byte SkillID;
        private int TarID; //player id
        private float X;
        private float Y;
        private float Z;

        public PacketEnemyAction(int enemyID, byte actionID, byte skillID, int targetID) : base(ACTION_ENEMY, true)
        {
            EnemyID = enemyID;
            ActID = actionID;
            SkillID = skillID;
            TarID = targetID;
        }

        public PacketEnemyAction(int enemyID, byte actionID, float x, float y, float z) : base(ACTION_ENEMY, true)
        {
            EnemyID = enemyID;
            ActID = actionID;
            X = x;
            Y = y;
            Z = z;
        }

        public override byte[] ToByteArray()
        {
            switch (ActID)
            {
                case 1: return CreateSkill();
            }

            return null;
        }

        private byte[] CreateSkill()
        {
            byte[] bytes = new byte[HEADER_SIZE + sizeof(byte) * 2 + sizeof(int) * 2];

            bytes[0] = ID;
            bytes[1] = PID;

            byte[] lenBytes = BitConverter.GetBytes(bytes.Length - HEADER_SIZE);
            Array.Copy(lenBytes, 0, bytes, 2, lenBytes.Length);

            byte[] eid = BitConverter.GetBytes(EnemyID);
            Array.Copy(eid, 0, bytes, 4, eid.Length);

            int index = 8;
            bytes[index++] = ActID;
            bytes[index++] = SkillID;

            byte[] targetID = BitConverter.GetBytes(TarID);
            Array.Copy(targetID, 0, bytes, index, targetID.Length);

            return bytes;
        }
    }
}
