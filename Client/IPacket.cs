﻿using System;

namespace Client
{
    public interface IPacket
    {
        byte ID { get; }
        bool IsReliable { get; }
        byte PID { get; set; }
        Nibble Nibble { get; }

        byte[] ToByteArray();
        string ToString();
    }
}
