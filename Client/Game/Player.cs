﻿namespace Client.Game
{
    public class Player
    {
        public string Name { get; private set; }

        public int Health { get; set; }
        public int MaxHealth
        {
            get
            {
                return Stamina * 10;
            }
        }

        public int Strength { get; set; }
        public int Stamina { get; set; }
        public int Intelligence { get; set; }
        public int Dexterity { get; set; }

        public Player(string name)
        {
            Name = name;
            Strength = Stamina = Intelligence = Dexterity = 10;
            Health = MaxHealth;
        }

        public bool IsDead
        {
            get { return Health <= 0; }
        }
    }
}
