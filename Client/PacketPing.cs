﻿namespace Client
{
    public class PacketPing : Packet
    {
        public PacketPing() : base(PING) { }

        public override byte[] ToByteArray()
        {
            byte[] bytes = new byte[4];
            bytes[0] = ID;
            bytes[1] = PID;
            bytes[2] = bytes[3] = 0;

            return bytes;
        }
    }
}
