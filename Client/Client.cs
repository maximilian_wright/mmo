﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Client
{
    public class Client
    {
        public delegate void ReceiveEvent(object sender, byte[] data, int numBytes);
        public event ReceiveEvent Receive;

        protected Socket socket;
        private bool created = false;
        private EndPoint endPoint;
        private Thread receiveThread;

        public Client()
        {
            CreateSocket();
        }

        public bool IsConnected
        {
            get { return created; }
        }

        private void CreateSocket()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.Blocking = false;

            socket.Bind(new IPEndPoint(IPAddress.Any, 0));
        }

        public void Connect(string host, int port)
        {
            if (!IsConnected)
            {
                try
                {
                    CreateSocket();
                    endPoint = new IPEndPoint(IPAddress.Parse(host), port);

                    created = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }

                receiveThread = new Thread(new ThreadStart(ReceiveThread));
                receiveThread.Start();
            }
        }

        public virtual void Disconnect()
        {
            if (IsConnected)
            {
                try
                {
                    receiveThread.Abort();
                    socket.SendTo(new Packet(Packet.DISCONNECT).ToByteArray(), endPoint);
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();

                    Console.WriteLine("Client.Disconnect");
                    created = false;
                }
                catch (SocketException e)
                {
                    Console.WriteLine(e.Message);
                    if (e.InnerException != null)
                        Console.WriteLine(e.InnerException);
                    Console.WriteLine(e.StackTrace);
                }
            }
        }

        public void Send(string text)
        {
            Send(Encoding.UTF8.GetBytes(text));
        }

        public virtual void Send(IPacket packet)
        {
            Send(packet.ToByteArray());
        }

        public void Send(byte[] bytes)
        {
            if (IsConnected)
            {
                try
                {
                    socket.SendTo(bytes, endPoint);
                }
                catch (SocketException e)
                {
                    Console.WriteLine(e.Message);
                    if (e.InnerException != null)
                        Console.WriteLine(e.InnerException);
                    Console.WriteLine(e.StackTrace);
                }
            }
        }

        protected virtual bool OnPreReceive(byte[] data)
        {
            return false;
        }

        protected void OnReceive(object sender, byte[] packet, int len)
        {
            if (Receive != null && len > 0)
                Receive(this, packet, len);
        }

        protected virtual void ReceiveThread()
        {
            while (!IsConnected) { }

            byte[] buffer = new byte[1300];

            EndPoint ep = new IPEndPoint(IPAddress.Any, 0);

            while (IsConnected)
            {
                try
                {
                    int numBytes = socket.ReceiveFrom(buffer, ref ep);

                    int firstPacketLen = BitConverter.ToInt16(buffer, buffer[0] == Packet.GENERIC_ACK ? 6 : 2) + 4;

                    Console.WriteLine(buffer[0] + ": " + firstPacketLen + "," + numBytes);

                    //multipacket
                    if (firstPacketLen < numBytes)
                    {
                        List<int> lengths = new List<int>();
                        List<byte[]> packets = Split(buffer, numBytes, ref lengths);

                        Console.WriteLine("got " + packets.Count + " packets");
                        for (int i = 0; i < packets.Count; i++)
                        {
                            byte[] packet = packets[i];
                            int len = lengths[i];

                            if (!OnPreReceive(packet))
                                OnReceive(this, packet, len);
                        }
                    }
                    else
                    {
                        if (!OnPreReceive(buffer))
                            if (Receive != null && numBytes > 0)
                                Receive(this, buffer, numBytes);
                    }
                }
                catch (SocketException)
                {
                    if (socket.Available == 0)
                        continue;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    //Environment.Exit(1);
                }
            }
        }

        protected List<byte[]> Split(byte[] buffer, int numBytes, ref List<int> lengths)
        {
            List<byte[]> packets = new List<byte[]>();
            byte[] data;
            int index = 2;

            while (index < numBytes)
            {
                int len = buffer[index - 2] == Packet.GENERIC_ACK ? 4 : BitConverter.ToInt16(buffer, index) + 4;

                Console.WriteLine("index: " + index + " | len: " + len);

                lengths.Add(len);
                data = new byte[len];

                Array.Copy(buffer, index - 2, data, 0, data.Length);
                packets.Add(data);
                index += len;
            }

            return packets;
        }
    }
}
