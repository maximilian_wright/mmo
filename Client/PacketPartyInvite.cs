﻿using System;
using System.Text;

namespace Client
{
    public class PacketPartyInvite : Packet
    {
        public int TargetID { get; private set; }

        public PacketPartyInvite(int tid) : base(PARTY_INVITE, true)
        {
            TargetID = tid;
        }

        public override byte[] ToByteArray()
        {
            byte[] bytes = new byte[4 + 4];
            bytes[0] = ID;
            bytes[1] = PID;

            byte[] len = BitConverter.GetBytes(4);
            byte[] target = BitConverter.GetBytes(TargetID);

            Array.Copy(len, 0, bytes, 2, len.Length);
            Array.Copy(target, 0, bytes, 4, target.Length);

            return bytes;
        }
    }
}
