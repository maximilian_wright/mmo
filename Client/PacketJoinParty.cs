﻿using System;

namespace Client
{
    public class PacketJoinParty : Packet
    {
        public int PartyID { get; private set; }

        public PacketJoinParty(int partyid) : base(PARTY_JOIN, true)
        {
            PartyID = partyid;
        }

        public override byte[] ToByteArray()
        {
            byte[] bytes = new byte[4 + 4];
            bytes[0] = ID;
            bytes[1] = PID;

            byte[] len = BitConverter.GetBytes(4);
            byte[] party = BitConverter.GetBytes(PartyID);

            Array.Copy(len, 0, bytes, 2, len.Length);
            Array.Copy(party, 0, bytes, 4, party.Length);

            return bytes;
        }
    }
}
