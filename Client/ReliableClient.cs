﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Timers;

namespace Client
{
    public class ReliableClient : Client
    {
        private Dictionary<Nibble, IPacket> packets;
        private Dictionary<Nibble, Timer> timers;
        private short uid;

        private byte packetid;
        
        private byte lastid = 0;

        public ReliableClient()
        {
            packets = new Dictionary<Nibble, IPacket>();
            timers = new Dictionary<Nibble, Timer>();
            packetid = 1;
        }

        public override void Send(IPacket packet)
        {
            if (packet.IsReliable)
            {
                packet.PID = GetNextPacketID();
                packets.Add(packet.Nibble, packet);
                AddTimer(packet.Nibble);
            }

            base.Send(packet);
        }

        public void CancelSending()
        {
            packets.Clear();

            foreach (KeyValuePair<Nibble, Timer> pair in timers)
                pair.Value.Stop();

            timers.Clear();
        }

        private byte GetNextPacketID()
        {
            packetid = (byte)((packetid + 1) % 256);
            return packetid;
        }

        private void AddTimer(Nibble n)
        {
            timers.Add(n, new Timer(200));
            timers[n].AutoReset = true;
            timers[n].Elapsed += (sender, e) =>
            {
                base.Send(packets[n]);
            };
            timers[n].Start();
        }

        protected override bool OnPreReceive(byte[] data)
        {
            byte id = data[0];
            byte pid = data[1]; 

            Nibble n;
            
            //Console.WriteLine("reliable got " + id);

            switch (id)
            {
                case Packet.GENERIC_ACK:
                    n = new Nibble(data[2], pid);
                    //Console.WriteLine(n);

                    if (packets.ContainsKey(n))
                        RemovePacket(n);
                    //else
                        //Console.WriteLine("Received GENERIC_ACK for packet we didn't send: " + n);

                    return true;
                case Packet.LOGIN_SUCCESS:
                    uid = BitConverter.ToInt16(data, 3);
                    SendAck(id, pid);
                    break;
                case Packet.DISCONNECT:
                case Packet.CHAT_MSG:
                case Packet.TRACK_PLAYER:
                case Packet.UNTRACK_PLAYER:
                case Packet.ZONE_ENTER:
                case Packet.CREATE_PARTY:
                case Packet.PARTY_INVITE:
                case Packet.PARTY_JOIN:
                case Packet.PARTY_LEAVE:
                case Packet.UPDATE_PLAYER:
                case Packet.UPDATE_ENEMY:
                case Packet.TRACK_ENEMY:
                case Packet.ACTION_PLAYER:
                case Packet.ACTION_ENEMY:
                case Packet.START_SIM_ENEMY:
                case Packet.STOP_SIM_ENEMY:
                case Packet.KILL_ENEMY:
                case Packet.KILL_PLAYER:
                case Packet.DUNGEON_CLEAR:
                case Packet.DUNGEON_WIPE:
                    SendAck(id, pid);
                    break;
                default:
                    break;
            }

            return false;
        }

        protected override void ReceiveThread()
        {
            while (!IsConnected) { }

            byte[] buffer = new byte[1300];

            EndPoint ep = new IPEndPoint(IPAddress.Any, 0);

            while (IsConnected)
            {
                try
                {
                    int numBytes = socket.ReceiveFrom(buffer, ref ep);

                    int firstPacketLen = BitConverter.ToInt16(buffer, buffer[0] == Packet.GENERIC_ACK ? 6 : 2) + 4;

                    //Console.WriteLine(buffer[0] + ": " + firstPacketLen + "," + numBytes);

                    //multipacket
                    if (firstPacketLen < numBytes)
                    {
                        List<int> lengths = new List<int>();
                        List<byte[]> packets = Split(buffer, numBytes, ref lengths);

                        //Console.WriteLine("got " + packets.Count + " packets");
                        for (int i = 0; i < packets.Count; i++)
                        {
                            byte[] packet = packets[i];
                            int len = lengths[i];

                            //if this is not the next packet we're expecting, ignore it
                            if (IsGuaranteed(packet[0]))
                            {
                                if (packet[1] != (lastid + 1) % 256)
                                {
                                    //Console.WriteLine("skipping packet id=" + packet[0] + ", pid=" + packet[1] + " | expected: " + (lastid + 1));
                                    continue;
                                }

                                lastid = packet[1];
                                //Console.WriteLine("new lastid: " + lastid + " from cid = " + packet[0]);
                            }

                            if (!OnPreReceive(packet))
                                OnReceive(this, packet, len);
                        }
                    }
                    else
                    {
                        if (IsGuaranteed(buffer[0]))
                        {
                            if (buffer[1] != (lastid + 1) % 256)
                            {
                                //Console.WriteLine("skipping packet id=" + buffer[0] + ", pid=" + buffer[1] + " | expected: " + (lastid + 1));
                                continue;
                            }

                            lastid = buffer[1];
                            //Console.WriteLine("new lastid: " + lastid + " from cid = " + buffer[0]);
                        }

                        if (!OnPreReceive(buffer))
                            OnReceive(this, buffer, numBytes);
                    }
                }
                catch (SocketException)
                {
                    if (socket.Available == 0)
                        continue;
                }
                catch (Exception)
                {
                    //Console.WriteLine(e.Message);
                    //Console.WriteLine(e.StackTrace);
                    //Environment.Exit(1);
                }
            }
        }

        private bool IsGuaranteed(byte cid)
        {
            switch (cid)
            {
                case Packet.GENERIC_ACK:
                case Packet.MOVE_PLAYER:
                case Packet.MOVE_ENEMY:
                case Packet.DAMAGE_PLAYER:
                case Packet.DAMAGE_ENEMY:
                    return false;
                default:
                    return true;
            }
        }

        public override void Disconnect()
        {
            base.Disconnect();

            packets.Clear();

            foreach (KeyValuePair<Nibble, Timer> pair in timers)
                pair.Value.Stop();

            timers.Clear();
        }

        private void SendAck(byte cid, byte pid)
        {
            Send(new PacketGenericAck(cid, pid));
        }

        private void RemovePacket(Nibble n)
        {
            packets.Remove(n);

            if (timers.ContainsKey(n))
            {
                timers[n].Stop();
                timers.Remove(n);
            }
        }
    }
}
