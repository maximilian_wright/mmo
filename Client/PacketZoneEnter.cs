﻿using System;
using System.Text;

namespace Client
{
    public class PacketZoneEnter : Packet
    {
        public int ZoneID { get; private set; }

        public PacketZoneEnter(int zoneid) : base(ZONE_ENTER, true)
        {
            ZoneID = zoneid;
        }

        public override byte[] ToByteArray()
        {
            byte[] bytes = new byte[4 + 4];
            bytes[0] = ID;
            bytes[1] = PID;

            byte[] len = BitConverter.GetBytes(4);
            byte[] zone = BitConverter.GetBytes(ZoneID);

            Array.Copy(len, 0, bytes, 2, len.Length);
            Array.Copy(zone, 0, bytes, 4, zone.Length);

            return bytes;
        }
    }
}
