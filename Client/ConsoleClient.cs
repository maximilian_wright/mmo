﻿using System;

namespace Client
{
    class ConsoleClient
    {
        private ReliableClient client;

        public ConsoleClient()
        {
            client = new ReliableClient();
            client.Receive += (sender, data, numBytes) =>
            {
                switch (data[0])
                {
                    default:
                        Console.WriteLine("len: " + numBytes);
                        break;
                }
            };

            string input;

            Console.Write("> ");
            while ((input = Console.ReadLine()) != "exit")
            {
                if (input.StartsWith("connect"))
                {
                    string username = input.Split(' ')[1];

                    client.Connect("129.21.28.42", 8888);
                    client.Send(new PacketLogin(username).ToByteArray());
                }
                else
                {
                    Console.WriteLine("sending '" + input + "'");
                    client.Send(input);
                }

                Console.Write("> ");
            }
            
            client.Disconnect();
        }

        public static void Main(string[] args)
        {
            new ConsoleClient();
        }
    }
}
