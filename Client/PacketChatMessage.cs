﻿using System;
using System.Text;

namespace Client
{
    public class PacketChatMessage : Packet
    {
        private string message;

        public PacketChatMessage(string msg) : base(CHAT_MSG, true)
        {
            message = msg;
        }

        public override byte[] ToByteArray()
        {
            byte[] bytes = new byte[2 + 2 + message.Length];
            bytes[0] = ID;
            bytes[1] = PID;

            byte[] lenBytes = BitConverter.GetBytes((short)message.Length);
            Array.Copy(lenBytes, 0, bytes, 2, lenBytes.Length);

            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            Array.Copy(messageBytes, 0, bytes, 4, messageBytes.Length);

            return bytes;
        }
    }
}
