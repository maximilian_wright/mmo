﻿namespace Client
{
    public class Packet : IPacket
    {
        protected const int HEADER_SIZE = 4;

        private byte id;
        private byte pid;

        private bool reliable;

        public Packet(byte id) : this(id, false) { }
        public Packet(byte id, bool reliable)
        {
            ID = id;
            PID = 0;
            IsReliable = reliable;
        }

        public byte ID
        {
            get { return id; }
            set { id = value; }
        }

        public bool IsReliable
        {
            get { return reliable; }
            set { reliable = value; }
        }

        public byte PID
        {
            get { return pid; }
            set { pid = value; }
        }

        public Nibble Nibble { get { return new Nibble(ID, PID); } }

        public virtual byte[] ToByteArray()
        {
            return new byte[4] { ID, PID, 0, 0 };
        }

        public override string ToString()
        {
            return "Packet[CID=" + ID + ",PID=" + PID + "]";
        }

        #region Command IDs

        public const byte LOGIN             = 1;
        public const byte LOGIN_SUCCESS     = 2;
        public const byte LOGIN_FAIL        = 3;

        public const byte GENERIC_ACK       = 4;
        public const byte DISCONNECT        = 5;
        
        public const byte TRACK_PLAYER      = 6;
        public const byte UNTRACK_PLAYER    = 7;

        public const byte CHAT_MSG          = 8;
        public const byte MOVE_PLAYER       = 9;

        public const byte PARTY_JOIN        = 10;
        public const byte PARTY_LEAVE       = 11;

        public const byte PING              = 12;
        public const byte ZONE_ENTER        = 13;

        public const byte PARTY_INVITE      = 14;
        public const byte CREATE_PARTY      = 15;

        public const byte UPDATE_PLAYER     = 16;
        public const byte UPDATE_ENEMY      = 17;

        public const byte ACTION_PLAYER     = 18;
        public const byte DAMAGE_PLAYER     = 19;

        public const byte MOVE_ENEMY        = 20;
        public const byte TRACK_ENEMY       = 21;

        public const byte DAMAGE_ENEMY      = 22;
        public const byte ACTION_ENEMY      = 23;

        public const byte START_SIM_ENEMY   = 24;
        public const byte STOP_SIM_ENEMY    = 25;

        public const byte KILL_ENEMY        = 26;
        public const byte KILL_PLAYER       = 27;

        public const byte DUNGEON_CLEAR     = 28;
        public const byte DUNGEON_WIPE      = 29;

        #endregion
    }

    #region Packet Mini Classes

    public class PacketCreateParty : Packet
    {
        public PacketCreateParty() : base(CREATE_PARTY, true) { }
    }

    #endregion
}
