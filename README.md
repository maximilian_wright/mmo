# Eternal Knights Online

Unity v5.3.x
Visual Studio 2015

## Building and Running

1. Build the Client project. This builds the dll that Unity uses.

2. Build and run the Server project.

3. Open Unity.

4. Open and run the menu scene.

